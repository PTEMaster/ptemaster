//
//  HpExtension.swift
//  Washitto
//
//  Created by kavyaMacMini4 on 08/06/18.
//  Copyright © 2018 Himanshupal.kavya. All rights reserved.
//

import UIKit

extension UIView {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.3
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
    func flash() {
        
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.5
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        
        layer.add(flash, forKey: nil)
    }
    
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
}

extension UIImage {
   
    class func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension Date {
    func daysBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
        return components.day ?? 0
    }
    func hourBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.hour], from: self, to: toDate)
        
        return components.hour ?? 0
    }
    
}

extension  UIView{
    func addPaddingView(_ img: UIImage) -> UIView{
        self.frame = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        let imgE = UIImageView(frame: CGRect(x: 13, y: 18, width: 13, height: 6))
        imgE.image = img
        self.addSubview(imgE)
        return self
    }
    /*tfPassword.rightView = UIView().addPaddingView(UIImage(named:"checkwhite")!)
     tfPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))*/
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    // call hp   let lblColor = UIColor(hex: "156CA9")
}
extension Date {
    
    func getLocalDate() -> Date {
        
        let timeZoneLocal = NSTimeZone.local as TimeZone!
        let newDate = Date(timeInterval: TimeInterval((timeZoneLocal?.secondsFromGMT(for: self))!), since: self)
        return newDate
    }
}

extension UIView {
    // OUTPUT 1
    func dropShadow() {
        self.layer.cornerRadius = 4
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 1
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 4.0
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, cornerRadius : CGFloat ,scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.cornerRadius = cornerRadius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func dropShadowAllSide() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 2.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds =  false
        
    }
    
    
    func padding (_ tf:UITextField,_ pad:Int) {
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: pad, height: Int(tf.frame.size.height)))
        // if kAppDelegate.language == "en" {
        tf.leftView = paddingView;
        tf.leftViewMode = UITextField.ViewMode.always
        //        } else {
        //            tf.rightView = paddingView;
        //            tf.rightViewMode = UITextFieldViewMode.always
        //        }
        
        /*TfGroupName.padding(TfGroupName, 5)*/
    }
    
}

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}


extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    /*tfState.text = dict.state_name.capitalizingFirstLetter()*/
}
extension String {
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    var uppercaseFirst: String {
        return first.uppercased() + String(characters.dropFirst())
    }
    //self.tfState.text!.uppercaseFirst
}


extension String {
    
    func getDoubleValue() -> Double {
        
        if self.characters.count == 0 {
            return 0.00
        } else {
            return Double(self)!
        }
        
    }
    
    
}


extension String {
    func chopPrefix(_ count: Int = 1) -> String {
        return substring(from: index(startIndex, offsetBy: count))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        return substring(to: index(endIndex, offsetBy: -count))
    }
}


/*"hello".chopPrefix()    // "ello"
 "hello".chopPrefix(3)   // "lo"
 
 "hello".chopSuffix()    // "hell"
 "hello".chopSuffix(3)   // "he"*/

extension UIView {
    func addDashedBorder() {
       // let color = headerColor.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
      //  shapeLayer.strokeColor = color
        shapeLayer.lineWidth  = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [8,2]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        self.layer.addSublayer(shapeLayer)
        
    }
    //viewProfile.addDashedBorder()
}
extension String {
    var isPhoneNumberNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.characters.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0// && res.range.length == self.characters.count //&& self.characters.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}


// MARK: extension UITextField

extension UITextField {
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    @IBInspectable var doneAccessory: Bool {
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        // doneToolbar.barStyle = UIBarStyle.blackOpaque
        
        doneToolbar.barStyle = UIBarStyle.blackOpaque
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor.white
    //    doneToolbar.tintColor = appColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        let close: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [close ,flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        
        self.resignFirstResponder()
    }
}

extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}


extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    //UIApplication.shared.statusBarView?.backgroundColor = statusBarColor in aap delegate
}

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    //print(UIDevice.current.modelName)
}

extension UINavigationController {
    
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    //self.navigationController?.backToViewController(vc: EditProfileVC.self)
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    //today.toString(dateFormat: "dd-MM")
}

extension NSMutableArray {
    func convertToString () -> String {
        do {
            let jsonData: NSData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
            
            let str = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            
            return str.replacingOccurrences(of: "\n", with: "")
        } catch {
            
        }
        
        return "[]"
    }
}

extension NSMutableDictionary {
    func convertToString () -> String {
        do {
            let jsonData: NSData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
            
            let str = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            
            return str.replacingOccurrences(of: "\n", with: "")
        } catch {
            
        }
        
        return "[]"
    }
}


extension String {
    
    func getDecimalValue(_ rupee: String) -> String {
        let fullNameArr = rupee.components(separatedBy: ".")
        if fullNameArr.count  > 1{
            let name = fullNameArr[0]
            var surname = fullNameArr[1]
            if surname.characters.count == 1 {
                surname.append("0")
                let xx = name + "." + surname
                return xx
            }else {
                return rupee
            }
        }
        return rupee + ".00"
    }
}


extension Double {
    var dollarString:String {
        return String(format: "$%.2f", self)
    }
    //Double(string(washtemperature, "value"))?.dollarString
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    // cell.viewCorner.roundCorners([.topLeft, .bottomLeft], radius: 30)
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    //let x = Double(0.123456789).rounded(toPlaces: 4)   x becomes 0.1235
}

extension String {
    func mycountryCode() -> String {
        let cc = Locale.current.regionCode!
        if (((Locale.current as NSLocale).object(forKey: .countryCode) as? String) != nil) {
            let fileURL : String = Bundle.main.path(forResource: "DiallingCodes", ofType: "plist")!
            let diallingCodesDictionary = NSDictionary(contentsOfFile: fileURL)
           // print(diallingCodesDictionary!)
            let aa = diallingCodesDictionary?.object(forKey: cc.lowercased())as! String
          //  print(aa)
        return aa
    }
    return ""
    }
}

// Increase height of scrollview when  textview height is increase then scroll up automatic.
extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        guard contentSize.height > bounds.size.height else { return }

        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: true)
    }
    
   // scrlView.scrollToBottom(animated: true)
   // view.layoutIfNeeded()
}

public func string (_ dict:NSDictionary, _ key:String) -> String {
    if let title = dict[key] as? String {
        return "\(title)"
    } else if let title = dict[key] as? NSNumber {
        return "\(title)"
    } else {
        return ""
    }
}

public func number (_ dict:NSDictionary, _ key:String) -> NSNumber {
    if let title = dict[key] as? NSNumber {
        return title
    } else if let title = dict[key] as? String {
        
        if let title1 = Int(title) as Int? {
            return NSNumber(value: title1)
        } else if let title1 = Float(title) as Float? {
            return NSNumber(value: title1)
        } else if let title1 = Double(title) as Double? {
            return NSNumber(value: title1)
        } else if let title1 = Bool(title) as Bool? {
            return NSNumber(value: title1)
        }
        
        return 0
    } else {
        return 0
    }
}

func getCurrent(element:AnyObject) -> IndexPath {
    let accessibilityString :String = (element.accessibilityLabel as? String)!
    let accessibilityArray : Array<String> = accessibilityString.components(separatedBy: ",")
    return (accessibilityArray.count == 2) ? IndexPath(row: Int(accessibilityArray.first!)!, section: Int(accessibilityArray.last!)!) : IndexPath(row: Int(-1), section: Int(-1))
    
    /*cell.btnCheck.accessibilityLabel = "\(indexPath),\(indexPath.section)"
    cell.btnCheck.addTarget(self, action: #selector(btnFeedback(_:)), for: .touchUpInside)
     let index : IndexPath = getCurrent(element: sender)
     */
}
extension Date {
    static var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    //let currentTimeTS = Date.currentTimeStamp
}

extension UIFont {

    static func availableFonts() {

        // Get all fonts families
        for family in UIFont.familyNames {
            NSLog("\(family)")

            // Show all fonts for any given family
            for name in UIFont.fontNames(forFamilyName: family) {
                NSLog("   \(name)")
            }
        }
    }
    //UIFont.availableFonts()
}





