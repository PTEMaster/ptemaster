//
//  RestAPI.swift
//  PTEMaster
//
//  Created by mac on 04/07/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit
import Alamofire
typealias ServiceComplitionClouser = ([String: Any]?, UInt16?, Error?) -> Void

class IPServiceHelper: NSObject {
    
  var completionClouser :ServiceComplitionClouser?
  static let shared: IPServiceHelper = IPServiceHelper()
  

    
    func callAnswerAPIWithParameters(_ param: [String: Any] , ansApi : String,  clouser:@escaping ServiceComplitionClouser)-> Void {
         
        if appDelegate.isNetworkAvailable {
          appDelegate.showHUD()
          
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: ansApi , method: .post, headers: nil) { (result) in
                print("Api------>\(ansApi)")
                print("Param-\(param)")
             
               
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                         appDelegate.hideHUD()
                        do
                        {
                            //let discresiotns = String(data: response.data!, encoding: String.Encoding.utf8)
                          //  print(discresiotns!)
                            let dict  = response.result.value as? [String:Any]
                            if dict != nil {
                                 print("Responce-\(dict! as Any)-")
                              clouser(dict!, 200, nil)
                            }else{
                                Util.showAlertWithMessage(AppConstant.errorMsg, title: "")
                            }
                            
                        }
                    }
                case .failure(let error):
                appDelegate.hideHUD()
                    clouser(nil, 400, nil)
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
           // appDelegate.hideHUD()
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
}
