//
//  SpeechRecogniserManager.swift
//
//  Copyright © 2020 Test. All rights reserved.
//

import Foundation
import Speech

protocol SpeechRecogniserDelegate:class {
    func getNewWordFromSpeechRecogniser(word:String)
    //func didFailWith(error:Error)
}


@available(iOS 10.0, *)
class SpeechRecogniserManager:NSObject, SFSpeechRecognizerDelegate, SFSpeechRecognitionTaskDelegate {
    
    static let sharedSpeechRecogniser = SpeechRecogniserManager()
    
    weak var delegate : SpeechRecogniserDelegate?
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))  //1
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    var inputNode:AVAudioInputNode? = nil
    
    func initlization(clouser:@escaping (Bool) -> Void ){
        speechRecognizer?.delegate = self  //3
        
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in  //4
            var isPermissionAllow = false
            
            switch authStatus {  //5
            case .authorized:
                isPermissionAllow = true
                
            case .denied:
                isPermissionAllow = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isPermissionAllow = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isPermissionAllow = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                clouser(isPermissionAllow)
            }
        }
        
    }
    func checkForSpeechRecognizer() {
          switch SFSpeechRecognizer.authorizationStatus() {
          case .authorized:
              break
          case .denied:
              print("User denied access to speech recognition")
          case .restricted:
              print("Speech recognition restricted on this device")
          case .notDetermined:
              askForSpeechRecognizer()
              print("Speech recognition not yet authorized")
          }
      }
      
      private func askForSpeechRecognizer() {
          SFSpeechRecognizer.requestAuthorization { (authStatus) in
              self.checkForSpeechRecognizer()
          }
      }
    
    func checkMicrophoneStatus() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        } else {
            startRecording()
        }
    }
    
    @available(iOS 10.0, *)
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
    }
    
    @available(iOS 10.0, *)
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishRecognition recognitionResult: SFSpeechRecognitionResult){
    }
    
    // Called when the task first detects speech in the source audio
    @available(iOS 10.0, *)
    func speechRecognitionDidDetectSpeech(_ task: SFSpeechRecognitionTask){
        print("speechRecognitionDidDetectSpeech")
    }
    
    // Called for all recognitions, including non-final hypothesis
    @available(iOS 10.0, *)
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didHypothesizeTranscription transcription: SFTranscription){
        print("didHypothesizeTranscription --- ", transcription.formattedString)
        self.delegate?.getNewWordFromSpeechRecogniser(word:transcription.formattedString)
    }
    
    // Called when the task is no longer accepting new audio but may be finishing final processing
    @available(iOS 10.0, *)
    func speechRecognitionTaskFinishedReadingAudio(_ task: SFSpeechRecognitionTask){
        print("speechRecognitionTaskFinishedReadingAudio")
    }
    
    // Called when the task has been cancelled, either by client app, the user, or the system
    @available(iOS 10.0, *)
    func speechRecognitionTaskWasCancelled(_ task: SFSpeechRecognitionTask){
        print("speechRecognitionTaskWasCancelled")
       
    }
    
    // Called when recognition of all requested utterances is finished.
    // If successfully is false, the error property of the task will contain error information
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishSuccessfully successfully: Bool)
    {
        print("didFinishSuccessfully")
        startRecording()
    }
    
    func startRecording() {
        if recognitionTask != nil {
            self.recognitionRequest?.endAudio()
            recognitionTask?.cancel()
            recognitionTask = nil
            recognitionRequest = nil
            inputNode?.reset()
            inputNode?.removeTap(onBus: 0)
            inputNode?.reset()
            stopRecording()
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .default)
            try audioSession.setPreferredSampleRate(44100)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        inputNode = audioEngine.inputNode
        
        //  Initializer for conditional binding must have Optional type, not 'AVAudioInputNode'
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        recognitionTask = self.speechRecognizer?.recognitionTask(with: recognitionRequest, delegate: self)
        
        let recordingFormat = inputNode?.outputFormat(forBus: 0)
        inputNode?.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    /// to stop the audio session
    func stopRecording() {
        DispatchQueue.main.async {
            if ((self.audioEngine.isRunning)){
                self.recognitionRequest?.endAudio()
                self.recognitionTask?.cancel()
                self.recognitionTask = nil
                self.recognitionRequest = nil
                self.inputNode?.reset()
                self.inputNode?.removeTap(onBus: 0)
                self.inputNode?.reset()
                self.audioEngine.inputNode.reset()
            }
        }
    }
}

