//
//  TDImageDownload.swift
//  Traydi
//
//  Created by mac on 01/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    public func imageFromURL(urlString: String) {
        
       // let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
      //  let urls = NSURL(string: (escapedString?.description)!  )! as URL
        
        
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            image = cachedImage
            return
        }
        var activityIndicator : UIActivityIndicatorView?
        if #available(iOS 13.0, *) {
             activityIndicator = UIActivityIndicatorView(style: .gray)
        } else {
                activityIndicator = UIActivityIndicatorView()
            activityIndicator?.style = .gray
        }
        activityIndicator!.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator!.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator!)
        }

        
        URLSession.shared.dataTask(with: NSURL(string: urlString )! as URL, completionHandler: { (data, response, error) -> Void in

            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                let demoimage = UIImage(named: "test_icon")
                
                imageCache.setObject(image ?? demoimage! , forKey: urlString as AnyObject)
                activityIndicator!.removeFromSuperview()
                self.image = image
            })

        }).resume()
    }
    
    
}
