//
//  PracticeModel.swift
//  PTEMaster
//
//  Created by mac on 15/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation

struct PracticeCategoryModel {
    
    var id: Int!
    var category: String?
    var parent: String?
    var subCategory: [PracticeSubCategoryModel]?
    
    init(_ dict: [String: JSON]) {
        id = dict["id"]?.intValue
        category = dict["category"]?.stringValue
        parent = dict["parent"]?.stringValue
        subCategory = []
        if let arr = dict["subcategory"]?.array {
            for obj in arr {
                subCategory?.append(PracticeSubCategoryModel(json: obj))
            }
        }
    }

    init(json: JSON) {
        id = json["id"].intValue
        category = json["category"].stringValue
        parent = json["parent"].stringValue
        subCategory = []
        if let arr = json["subcategory"].array {
            for obj in arr {
                subCategory?.append(PracticeSubCategoryModel(json: obj))
            }
        }
    }
}

struct PracticeSubCategoryModel {

    var id: Int!
    var category: String?
    var parent: String?

    init(_ dict: [String: JSON]) {
        id = dict["id"]?.intValue
        category = dict["category_id"]?.stringValue
        parent = dict["test_id"]?.stringValue
    }
    
    init(json: JSON) {
        id = json["id"].intValue
        category = json["category"].stringValue
        parent = json["parent"].stringValue
        
    }
}
