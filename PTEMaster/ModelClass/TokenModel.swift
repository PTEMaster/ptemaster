//
//  TokenModel.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation


class TokenModal: NSObject {
    
    var tk_pkg_from_date:String?
    var deals:String?
    var inhouse:String?
    var tk_pkg_credits:String?
    var tk_pkg_name:String?
    var tk_pkg_description:String?
    var consume:String?
    var tk_pkg_id:String?
    var tk_pkg_active_time:String?
    var tk_pkg_to_date:String?
    var tk_pkg_type:String?
    var tk_pkg_price:String?
    var tk_pkg_discount:String?
    
    init(parameters:JSON) {
        tk_pkg_from_date = parameters["tk_pkg_from_date"].stringValue
        deals = parameters["deals"].stringValue
        inhouse  = parameters["inhouse"].stringValue
        tk_pkg_credits = parameters["tk_pkg_credits"].stringValue
        tk_pkg_name = parameters["tk_pkg_name"].stringValue
        tk_pkg_description = parameters["tk_pkg_description"].stringValue
        consume = parameters["consume"].stringValue
        tk_pkg_id = parameters["tk_pkg_id"].stringValue
        tk_pkg_active_time = parameters["tk_pkg_active_time"].stringValue
        tk_pkg_to_date = parameters["tk_pkg_to_date"].stringValue
        tk_pkg_type = parameters["tk_pkg_type"].stringValue
        tk_pkg_price = parameters["tk_pkg_price"].stringValue
        tk_pkg_discount = parameters["tk_pkg_discount"].stringValue
    }
}


class MyToken : NSObject{
    var endtime:String?
    var affiliated_by:String?
    var promo:String?
    var timecount:String?
    var starttime:String?
    var id:String?
    var purchase_token:String?
    var finalprice:String?
    var tokenpackid:String?
    var buy_date:String?
    var tokentype:String?
    var activestatus:String?
    var tokencount:String?
    var purchase_status:String?
    var tokenpackname:String?
    var userid:String?
    
    init(parameters:JSON) {
        endtime = parameters["endtime"].stringValue
        affiliated_by = parameters["affiliated_by"].stringValue
        promo  = parameters["promo"].stringValue
        timecount = parameters["timecount"].stringValue
        starttime = parameters["starttime"].stringValue
        id = parameters["id"].stringValue
        purchase_token = parameters["purchase_token"].stringValue
        finalprice = parameters["finalprice"].stringValue
        tokenpackid = parameters["tokenpackid"].stringValue
        buy_date = parameters["buy_date"].stringValue
        tokentype = parameters["tokentype"].stringValue
        activestatus = parameters["activestatus"].stringValue
        tokencount = parameters["tokencount"].stringValue
         purchase_status = parameters["purchase_status"].stringValue
         tokenpackname = parameters["tokenpackname"].stringValue
         userid = parameters["userid"].stringValue
        
    }
}


