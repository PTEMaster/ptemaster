//
//  TestDetailModel.swift
//  PTEMaster
//
//  Created by mac on 22/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation


struct TestDetailModel {
    
    var id : String?
    var category : String?
    var parent : String?
    var totalQuestion : String?
    
    var testId: String?
    var totalQuestionNumber: String?
    var time: String?
    var price: String?
    var desciption: String?


    init(_ dict: [String: JSON]) {
        id = dict["id"]?.stringValue
        category = dict["category"]?.stringValue
        parent = dict["parent"]?.stringValue
        totalQuestion = dict["total_que"]?.stringValue
        testId = dict["id"]?.stringValue
        time = dict["duration"]?.stringValue
        price = dict["price"]?.stringValue
        totalQuestionNumber = dict["total_que"]?.stringValue
        desciption = dict["name"]?.stringValue
    }
    
    init(json: JSON) {
        id = json["id"].stringValue
        category = json["category"].stringValue
        id = json["parent"].stringValue
        totalQuestion = json["total_que"].stringValue
        
        testId = json["id"].stringValue
        time = json["duration"].stringValue
        price = json["price"].stringValue
        totalQuestionNumber = json["total_que"].stringValue
        desciption = json["name"].stringValue
    }


}

struct CoupleInfoModel {
    
    var id: String?
    var title: String?
    var code: String?
    var discount: String?
    var fromDate: String?
    var toDate: String?
    var tests: String?
    var limitPerUser: String?
    var totalLimit: String?
    
    init(_ dict: [String: JSON]) {
        id = dict["id"]?.stringValue
        title = dict["title"]?.stringValue
        code = dict["code"]?.stringValue
        discount = dict["discount"]?.stringValue
        fromDate = dict["from_date"]?.stringValue
        toDate = dict["to_date"]?.stringValue
        tests = dict["tests"]?.stringValue
        limitPerUser = dict["limit_per_user"]?.stringValue
        totalLimit = dict["total_limit"]?.stringValue
    }
    
    init(json: JSON) {
        id = json["id"].stringValue
        title = json["title"].stringValue
        code = json["code"].stringValue
        discount = json["discount"].stringValue
        fromDate = json["from_date"].stringValue
        totalLimit = json["to_date"].stringValue
        tests = json["tests"].stringValue
        limitPerUser = json["limit_per_user"].stringValue
        totalLimit = json["tests"].stringValue
    }
}
