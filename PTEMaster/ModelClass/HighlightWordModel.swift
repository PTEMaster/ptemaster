//
//  HighlightWordModel.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 13/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation

struct HighlightWordModel {
    var word: String {
        didSet {
            self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        }
    }
    var isCorrect: Bool
    var displayCorrect: Bool
    var isSelected: Bool
    var wordWidth: CGFloat = 0.0
}

extension HighlightWordModel {
    
    init(word: String, isCorrect: Bool, displayCorrect: Bool, isSelected: Bool) {
        self.word = word
        self.isCorrect = isCorrect
        self.displayCorrect = displayCorrect
        self.isSelected = isSelected
        self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
    }
    
}
