//
//  PracticeTestModel.swift
//  PTEMaster
//
//  Created by mac on 18/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation

struct PracticeTestModel {
    
    var row: Int!
    var questionNumber: Int!
    var allcount: Int!
    var mockqestion  = [PracticeQestionModel]()
    var selectMockqestion = [PracticeQestionModel]()
    var difftokentime: Int!
    var toalCreditTime: Int!
    var toalCredits: Int!
    
    
    
    init(json: JSON) {
        row = json["row"].intValue
        questionNumber = json["question_number"].intValue
        allcount = json["allcount"].intValue
        
        if let practiceTestDetail = json["Allmockqestion"].array {
            // self.mockqestion = PracticeQestionModel.init(practiceTestDetail)
            for practiceData in practiceTestDetail {
                let model = PracticeQestionModel.init(json: practiceData)
                mockqestion.append(model)
            }
        }
        
        if let practiceTestDetail = json["mockqestion"].array {
            // self.mockqestion = PracticeQestionModel.init(practiceTestDetail)
            for practiceData in practiceTestDetail {
                let model = PracticeQestionModel.init(json: practiceData)
                selectMockqestion.append(model)
            }
        }
        
        difftokentime = json["difftokentime"].int
        
        
        // FIXME: 29/10/21 by himanshu pal iOS
        toalCreditTime = json["toalCreditTime"].int
        toalCredits = json["toalCredits"].int

    }
}


struct PracticeQestionModel {
    
    var id: String?
    var categoryId: String?
    var testId: String?
    var shortTitle: String?
    var description: String?
    var audio: String?
    var image: String?
    var question: String?
    var categoryName: String?
    var subCategoryName: String?
    var questionNumber: Int!
    var questionId: String?
    var totalTimeRemaining: TimeInterval!
    var totalQuestion: Int!
    var waitTime: TimeInterval!
    var responseTime: TimeInterval!
    var categoryType: CategoryType?
    var answer: String?
    var answerType: String?
    var addons: AddonsModal?
    var frequency:String?
    var arrOptions = [OptionsModel]()
    
    
    init(json: JSON) {
        id = json["id"].string
        categoryId = json["category_id"].string
        testId = json["test_id"].string
        shortTitle = json["short_title"].string
        description = json["description"].string
        audio = json["audio"].string
        image = json["image"].string
        question = json["question"].string
        categoryName = json["category_name"].string
        subCategoryName = json["sub_category_name"].string
        questionNumber = json["question_number"].intValue
        questionId = json["question_id"].string
        totalTimeRemaining = json["total_time_remaining"].doubleValue
        totalQuestion = json["total_question"].intValue
        waitTime = json["wait_time"].doubleValue
        responseTime = json["response_time"].doubleValue
        categoryType = CategoryType(rawValue: categoryId ?? "") ?? CategoryType.summarizeWrittenText
        answer = json["answer"].string
        answerType = json["answer_type"].string
        frequency = json["frequency"].stringValue
        let add = json["addons"]
        addons = AddonsModal(parameters: JSON(add))
        
        if let options = json["options"].array {
            for item in options {
                let dict = OptionsModel(json: item)
                self.arrOptions.append(dict)
            }
        }
    } 
}

struct AddonsModal {
    var notes:String?
    var label:String?
    var my_attampt:String?
    var id:String?
    var question_cat:String?
    var userid:String?
    var exam_count:String?
    var question_id:String?
    
    init(parameters:JSON) {
        notes = parameters["note"].stringValue
        label = parameters["label"].stringValue
        my_attampt = parameters["my_attampt"].stringValue
        id = parameters["id"].stringValue
        question_cat = parameters["question_cat"].stringValue
        userid = parameters["userid"].stringValue
        exam_count = parameters["exam_count"].stringValue
        question_id = parameters["question_id"].stringValue
    }
}


