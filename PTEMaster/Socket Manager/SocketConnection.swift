//
//  SocketConnection.swift
//  IOUHOU
//
//  Created by mac on 21/10/19.
//  Copyright © 2019 mac. All rights reserved.
// https://ptemaster.herokuapp.com

import Foundation
import SocketIO

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
       static let manager=SocketManager(socketURL:URL(string:"https://ptemaster.herokuapp.com/")!)
       var defaultSocket = manager.defaultSocket
    
     
    //  let socket = SocketManager(socketURL: URL(string: "https://api.quickkitty.com:5000/")!, config: [.log(true), .compress])
    
    
    override init() {
        super.init()
       defaultSocket.manager?.socket(forNamespace: "/swift")
      
    }
   
    

  func establishConnection() {
        defaultSocket.connect()
        print(defaultSocket.status)
        switch defaultSocket.status {
        case .connected:
            print("Socket Conneted")
            if UserDefaults.standard.value(forKey: randomString) == nil{
                self.emitData()
            }
        case .connecting:
            print("connecting...")
        case .disconnected:
            print("disconnected")
        default:
            print("Socket Has Stopped")
        }
        
    }
    
    
    func closeConnection() {
           print("Socket DisConneted")
        
           defaultSocket.disconnect()
        // Util.showAlertWithCallback(kAppName, message: "Socket DisConneted", isWithCancel: false)
      //  show(message: "Socket DisConneted")
       }
    
    func connectSocket(){
        defaultSocket.once(clientEvent: .connect) { (data , ask) in
        print("===================================================== connect socket")
            if UserDefaults.standard.value(forKey: randomString) == nil{
                print("================================emitData===================== emitData")
                self.emitData()
            }
        }
    }
    func emitData() {
       var params = [String:Any]()
        let randomStr = NSUUID().uuidString
       params["userId"] = String(AppDataManager.shared.loginData.id)
       params["uniqueCode"] = randomStr
        UserDefaults.standard.setValue(randomStr, forKey: randomString)
       SocketIOManager.sharedInstance.defaultSocket.emit("sendUniqueCode" , with: [params])
       
      //  PTE_Master.show(message: "emit")
   }
    
   /* func receiveSocketData(){
        let aa = AppDataManager.shared.loginData.id
        SocketIOManager.sharedInstance.defaultSocket.on("receiveUniqueCode\(aa!)") { (receiveData, ask) in
            print("receiveData-\(receiveData)-")
            PTE_Master.show(message: "receiveSocketData")
            UserDefaults.standard.removeObject(forKey: loginDetail)
            self.navigationController!.setViewControllers([LoginViewController.instance()], animated: true)
         
        }}*/
    

}



extension UIViewController {
    func topMostViewController() -> UIViewController {

        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }

        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }

        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
    }

        return self
    }
}


extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}

func show(message: String) {
   let controller =    UIApplication.shared.topMostViewController()
   let toastContainer = UIView(frame: CGRect())
   toastContainer.backgroundColor = UIColor(red: 45/255, green: 45/255, blue: 45/255, alpha: 1)
   toastContainer.alpha = 0.0
   toastContainer.layer.cornerRadius = 8;
   toastContainer.clipsToBounds  =  true

   let toastLabel = UILabel(frame: CGRect())
   toastLabel.textColor = UIColor.white
   toastLabel.textAlignment = .center;
   toastLabel.font = UIFont.regularFont(size: 15)
   toastLabel.text = message
   toastLabel.clipsToBounds  =  true
   toastLabel.numberOfLines = 0

   toastContainer.addSubview(toastLabel)
   controller?.view.addSubview(toastContainer)

   toastLabel.translatesAutoresizingMaskIntoConstraints = false
   toastContainer.translatesAutoresizingMaskIntoConstraints = false

   let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 15)
   let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -15)
   let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
   let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
   toastContainer.addConstraints([a1, a2, a3, a4])
   
   
   

   let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller?.view, attribute: .leading, multiplier: 1, constant: 65)
   let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller?.view, attribute: .trailing, multiplier: 1, constant: -65)
   let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller?.view, attribute: .bottom, multiplier: 1, constant: -110)
   controller?.view.addConstraints([c1, c2, c3])

   UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
       toastContainer.alpha = 1.0
   }, completion: { _ in
       UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
           toastContainer.alpha = 0.0
       }, completion: {_ in
           toastContainer.removeFromSuperview()
       })
   })
}
//-----------------------------------
/*class SocketIOManagerr: NSObject {

    open class SocketConnection {

        public static let default_ = SocketConnection()
        let manager: SocketManager
        private init() {
            let param:[String:Any] = [:]
            let route = "YOUR_ROUTE"
            let socketURL: URL = Utility.URLforRoute(route: route, params: param)! as URL
            manager = SocketManager(socketURL: socketURL, config: [.log(true), .compress])
            manager.config = SocketIOClientConfiguration(arrayLiteral: .connectParams(param), .secure(true))
        }
    }
    private func connectSocket(){
        let socket = SocketConnection.default_.manager.defaultSocket
        if socket.status != .connected{
            socket.connect()
        }
        socket.on(clientEvent: .connect) {data, ack in

            print(data)
            print(ack)
            print("socket connected")
            self.getFinishAcknowledgement()
        }
        socket.on(clientEvent: .disconnect) {data, ack in

        }
        socket.on("unauthorized") { (data, ack) in
            print(data)
            print(ack)
            print("unauthorized user")
        }
    }
    private func disconnectSocket(){
        let socket = SocketConnection.default_.manager.defaultSocket
        socket.disconnect()
    }
    private func emitLatLng(){
        let socket = SocketConnection.default_.manager.defaultSocket
        if socket.status != .connected{return}
        let params:[String:Any] = ["lat":"lat","lng":"lng","rideId":"rideId"] as Dictionary
        print(params)
        socket.emitWithAck("Acknowledgement", params).timingOut(after: 5) {data in
            print(data)
        }
    }
    private func emitEndRide(){
        let socket = SocketConnection.default_.manager.defaultSocket
        let param:[String:Any] = ["rideId":"rideId"] as Dictionary
        socket.emitWithAck("Acknowledgement", param).timingOut(after: 5) {data in
            print(data)
        }
    }
    private func getFinishAcknowledgement(){
        let socket = SocketConnection.default_.manager.defaultSocket
        socket.on("Acknowledgement") {data, ack in
            print(data)
            socket.disconnect()
        }
    }
}
*/
