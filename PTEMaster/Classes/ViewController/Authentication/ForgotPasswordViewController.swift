//
//  ForgotPasswordViewController.swift
//  PTEMaster
//
//  Created by mac on 12/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ForgotPasswordViewController: UIViewController
{

    // MARK: - Properties
    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Check Validation
    func isValidLogin() -> Bool {
        if !Util.isValidString(txtEmail.text!) {
            Util.showAlertWithMessage("Please enter email", title: Key_Alert)
            return false
        } else if !Util.isValidEmail(txtEmail.text!) {
            Util.showAlertWithMessage("Please enter valid email", title: Key_Alert)
            return false
        }
        return true
    }
    
    
    // MARK: - Action Method
    @IBAction func doclickonSend(_ sender: Any) {
        if isValidLogin() {
            self.view.endEditing(true)
            forgetPassowrdApi()
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Forget passowrd Api
    func forgetPassowrdApi() {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: Any] = [ "email": "\(txtEmail.text!)"]
               appDelegate.showHUD()
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.forgetPasswordApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                   
                                    
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false){
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                   
                                    
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false){
                                        
                                    }
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
}


