//
//  RegisterViewController.swift
//  PTEMaster
//
//  Created by mac on 12/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txfFullName: JVFloatLabeledTextField!
    @IBOutlet weak var txfEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txfPassword: JVFloatLabeledTextField!
    @IBOutlet weak var txfConfirmPassword: JVFloatLabeledTextField!
    @IBOutlet weak var txfContactNumber: JVFloatLabeledTextField!
 
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        btnLogin.transform = CGAffineTransform(rotationAngle: -89.55)
        btnRegister.transform = CGAffineTransform(rotationAngle: -89.55)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- Check Vaildation
    func isValidRegister() -> Bool {
        if !Util.isValidString(txfFullName.text!) {
            Util.showAlertWithMessage("Please enter full name", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfEmail.text!) {
            Util.showAlertWithMessage("Please enter e-mail", title: Key_Alert)
            return false
        } else if !Util.isValidEmail(txfEmail.text!) {
            Util.showAlertWithMessage("Please enter Valid e-mail", title: Key_Alert)
            return false
        }  else if !Util.isValidString(txfPassword.text!) {
            Util.showAlertWithMessage("Please enter Password", title: Key_Alert)
            return false
        }
        else if txfPassword.text?.count ?? 0 < 6 {
            Util.showAlertWithMessage("Password must have 6 characters", title: Key_Alert)
            return false
        }
        else if !Util.isValidString(txfConfirmPassword.text!) {
            Util.showAlertWithMessage("Please enter confirm password", title: Key_Alert)
            return false
        } else if txfConfirmPassword.text! != txfPassword.text! {
            Util.showAlertWithMessage("Password Doesn't Match", title: Key_Alert)
            return false
        } else if !Util.isValidString(txfContactNumber.text!) {
            Util.showAlertWithMessage("Please enter a valid mobile number.", title: Key_Alert)
            return false
        }
        return true
    }
    
    
    // MARK:- Button Action
    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonRegister(_ sender: Any) {
        if isValidRegister() {
           SignUpApi()
        }
    }
    
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
          guard let url = URL(string: "https://www.masterpte.com.au/privacy") else { return }
          UIApplication.shared.open(url)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
           return true
        }
    
    
    //MARK: - Sign Up Api
    func SignUpApi() {
        if appDelegate.isNetworkAvailable {
            
          appDelegate.showHUD()
            let parameter : [String: Any] = ["email" : "\(txfEmail.text!)" , "name": "\(txfFullName.text!)", "password": "\(txfConfirmPassword.text!)", "mobile": "\(txfContactNumber.text!)"]
            print(parameter)
                        
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.SignupApi.rawValue, method: .post, headers: nil) { (result) in
                
               // Loader.hideLoader()
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print(response.response!) // URL response
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value != nil {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let loginDict = swiftyJsonResponseDict["data"].dictionary {
                                        
                                        print(">>>> \(loginDict)")

                                        self.navigationController?.popViewController(animated: true)
                                       // Util.showAlertWithMessage("Please verify your account", title: Key_Alert)
                                        
                                    }else {
                                        Util.showAlertWithMessage("Please try again", title: Key_Alert)
                                    }
                                    
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }else {
                                
                                Util.showAlertWithMessage("Please try again", title: Key_Alert)
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
            
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
}
