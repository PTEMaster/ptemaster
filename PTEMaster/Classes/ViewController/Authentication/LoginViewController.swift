//
//  LoginViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 11/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices



class LoginViewController: UIViewController , UITextFieldDelegate {
   // @IBOutlet weak var btnAppleLogin: ASAuthorizationAppleIDButton!
  //  @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txfEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txfPassword: JVFloatLabeledTextField!
    @IBOutlet weak var btngoog: GIDSignInButton!
   // @IBOutlet weak var stkViewLogin: UIStackView!
    @IBOutlet weak var btnAppleSigin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         GIDSignIn.sharedInstance().delegate = self
        setInitialView()
        btnLogin.transform = CGAffineTransform(rotationAngle: -89.55)
        btnRegister.transform = CGAffineTransform(rotationAngle: -89.55)
        
        if #available(iOS 13.0, *) {
            self.btnAppleSigin.isHidden = false
        }
     
   
         // "deva@mailinator.com"

//     txfEmail.text = "ankit@mailinator.com"
  //     txfEmail.text = "anshul12@mailinator.com"
//  txfEmail.text = "ctnidhi10@gmail.com"
 // txfPassword.text = "123456"
        
        // Let GIDSignIn know that this view controller is presenter of the sign-in sheet
        GIDSignIn.sharedInstance()?.presentingViewController = self
  
        
        // appleLoginButton()
       // appleCustomLoginButton()
    }
    
    
    @available(iOS 13.0, *)
    @IBAction func handleAppleIdRequest(_ sender: ASAuthorizationAppleIDButton) {
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]
    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
    authorizationController.delegate = self
    authorizationController.performRequests()
    }
    

    
    private func fetchUserData() {
        let fbLoginManager : LoginManager = LoginManager()

        fbLoginManager.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in

              if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                  if fbloginresult.grantedPermissions != nil {
                      if(fbloginresult.grantedPermissions.contains("email")) {
                        if((AccessToken.current) != nil){
                            
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                  if (error == nil){
                                     let dict: NSDictionary = result as! NSDictionary
                                    print("dict-\(dict)-")
                                    if let token = AccessToken.current?.tokenString {
                                          print("tocken: \(token)")
                                          let userDefult = UserDefaults.standard
                                          userDefult.setValue(token, forKey: "access_tocken")
                                          userDefult.synchronize()
                                      }
                                    let user  = dict.object(forKey:"name") as? String ?? ""
                                          print("user: \(user)")
                                     
                                      if let id  = dict.object(forKey:"id") as? String {
                                          print("id: \(id)")
                                      }
                                     let email  = (result! as AnyObject).value(forKey: "email") as? String ?? ""
                                          print("email: \(email)")
                                    
                                    let imageURL = ((dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String ?? ""
                                                   print(imageURL)
                                              
                                    
                                    self.socialLoginApi(user  , email as String , image: imageURL as String )
                                  }
                              })
                          }
                      }
                  }
              }
          }
    }

    
    func setInitialView() {
        
        if let loginDict = UserDefaults.standard.dictionary(forKey: loginDetail)  {
            AppDataManager.shared.loginData = LoginModel(loginDetail: loginDict)
            let DashboardVC = CustomTabBarController.instance() as! CustomTabBarController
            self.navigationController?.setViewControllers([DashboardVC], animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Method
    func isValidLogin() -> Bool {
        if !Util.isValidString(txfEmail.text!) {
            Util.showAlertWithMessage("Please enter email", title: Key_Alert)
            return false
        } else if !Util.isValidEmail(txfEmail.text!) {
            Util.showAlertWithMessage("Please enter valid email", title: Key_Alert)
            return false
        }  else if !Util.isValidString(txfPassword.text!) {
            Util.showAlertWithMessage("Please enter Password", title: Key_Alert)
            return false
        } else if txfPassword.text?.count ?? 0 < 6 {
            Util.showAlertWithMessage("Password must have 6 characters", title: Key_Alert)
            return false
        }
        
        return true
    }

    //MARK: IBAction Method
    @IBAction func doclickonForgotPassword(_ sender: Any) {
        let vc = ForgotPasswordViewController.instance() as! ForgotPasswordViewController
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func doclickonLogin(_ sender: Any) {
        if isValidLogin() {
            LoginApi()
        }
    }
    
    @IBAction func doclickonRegister(_ sender: Any) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @IBAction func actionGoolgeLogin(_ sender: Any) {
     GIDSignIn.sharedInstance()?.signIn()
    }
    @IBAction func actionFacebokLogin(_ sender: Any) {
        fetchUserData()
    }
    //MARK: GoogleSignIn UI Delegate
    
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
           return true
        }
    
    // MARK:- Login Api
    func LoginApi() {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: AnyObject] =
                [ "email" : txfEmail.text as AnyObject,
                    "password" : txfPassword.text as AnyObject]
            
            appDelegate.showHUD()
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.LoginApi.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                  //  print(" swifty json print \(swiftyJsonResponseDict)")
                
                                    if let loginDict = swiftyJsonResponseDict["data"].dictionary {
                                    //    print(">>>> \(loginDict)")
                                        
                                        let loginDataModel = LoginModel(loginDict)
                                        AppDataManager.shared.loginData = loginDataModel
                                        UserDefaults.standard.set(loginDataModel.getDictionary(), forKey: loginDetail)
                                      //  self.emitData()
                                        let DashboardVC = CustomTabBarController.instance()  as! CustomTabBarController
                                        self.navigationController?.pushViewController(DashboardVC, animated: true)
                                      }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    func emitData() {
       var params = [String:Any]()
        let randomStr = NSUUID().uuidString
       params["userId"] = AppDataManager.shared.loginData.id
       params["uniqueCode"] = randomStr
        UserDefaults.standard.setValue(randomStr, forKey: randomString)
       SocketIOManager.sharedInstance.defaultSocket.emit("sendUniqueCode" , with: [params])
       
      //  PTE_Master.show(message: "emit")
   }
   
    
    func socialLoginApi(_ name :String?  = "", _ email : String?  = "" , image: String? = "") {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: AnyObject] =
                [ "fname" : name as AnyObject,
                    "femail" : email as AnyObject , "fimage" : image as AnyObject]
            
            appDelegate.showHUD()
            print("API-\(BaseUrl3 + RequestPath.socialLogin.rawValue)-")
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl3 + RequestPath.socialLogin.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                print(swiftyJsonResponseDict)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                
                                    if let loginDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(loginDict)")
                                        
                                        let loginDataModel = LoginModel(loginDict)
                                        AppDataManager.shared.loginData = loginDataModel
                                        UserDefaults.standard.set(loginDataModel.getDictionary(), forKey: loginDetail)
                                        let DashboardVC = CustomTabBarController.instance()
                                        self.navigationController?.pushViewController(DashboardVC, animated: true)
                                      }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    
    
    // 'Sign in with Apple' button using ASAuthorizationAppleIDButton class
   /*  func appleLoginButton() {
         if #available(iOS 13.0, *) {
             let appleLoginBtn = ASAuthorizationAppleIDButton(type: .signIn, style: .whiteOutline)
             appleLoginBtn.addTarget(self, action: #selector(actionHandleAppleSignin), for: .touchUpInside)
             self.view.addSubview(appleLoginBtn)
             // Setup Layout Constraints to be in the center of the screen
             appleLoginBtn.translatesAutoresizingMaskIntoConstraints = false
             NSLayoutConstraint.activate([
                 appleLoginBtn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                 appleLoginBtn.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
                 appleLoginBtn.widthAnchor.constraint(equalToConstant: 200),
                 appleLoginBtn.heightAnchor.constraint(equalToConstant: 40)
                 ])
         }
     }*/

     // Custom 'Sign in with Apple' button
    /* func appleCustomLoginButton() {
         if #available(iOS 13.0, *) {
             let customAppleLoginBtn = UIButton()
             customAppleLoginBtn.backgroundColor = UIColor.red
             customAppleLoginBtn.setImage(UIImage(named: "ic_apple"), for: .normal)
             customAppleLoginBtn.addTarget(self, action: #selector(actionHandleAppleSignin), for: .touchUpInside)

         }
     }*/
     
     @available(iOS 13.0, *)
     func getCredentialState() {
         let appleIDProvider = ASAuthorizationAppleIDProvider()
         appleIDProvider.getCredentialState(forUserID: "USER_ID") { (credentialState, error) in
             switch credentialState {
             case .authorized:
                 // Credential is valid
                 // Continiue to show 'User's Profile' Screen
                 break
             case .revoked:
                 // Credential is revoked.
                 // Show 'Sign In' Screen
                 break
             case .notFound:
                 // Credential not found.
                 // Show 'Sign In' Screen
                 break
             default:
                 break
             }
         }
     }

    @IBAction func actionHandleAppleSignin(_ sender : Any) {
         if #available(iOS 13.0, *) {
             let appleIDProvider = ASAuthorizationAppleIDProvider()
             let request = appleIDProvider.createRequest()
             request.requestedScopes = [.fullName, .email]
             let authorizationController = ASAuthorizationController(authorizationRequests: [request])
             authorizationController.delegate = self
             authorizationController.presentationContextProvider = self
             authorizationController.performRequests()
         }
     }
}






@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate {

    // Authorization Failed
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }


    // Authorization Succeeded
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Get user data with Apple ID credentitial
            let appleId = appleIDCredential.user
            let userId = appleId
            let appleUserFirstName = appleIDCredential.fullName?.givenName
            let appleUserLastName = appleIDCredential.fullName?.familyName
            let appleUserEmail = appleIDCredential.email
            print(userId)
            print(appleUserFirstName)
            print(appleUserLastName)
            print(appleUserEmail)
          
            socialLoginApi(appleUserFirstName, appleUserEmail)
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Get user data using an existing iCloud Keychain credential
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            // Write your code
            print(appleUsername)
            print(applePassword)
        }
    }

}


@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
    
    // For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
}

extension LoginViewController: GIDSignInDelegate{
    
    
    // Retrieve user profile details from Google
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                withError error: Error!) {
        if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            print("\(error.localizedDescription)")
          }
          return
        }
        // Perform any operations on signed in user here.
       // let userId = user.userID                  // For client-side use only!
       // let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
      //  let givenName = user.profile.givenName
      //  let familyName = user.profile.familyName
        let email = user.profile.email
        let img = user.profile.imageURL(withDimension: 100).absoluteString
        print("img-\(img)-")
    // let strImg = try? String(contentsOf: img!)
        socialLoginApi(fullName ?? "", email ?? "", image: img)
        /*  print("userId-\(userId)-")
           print("idToken-\(idToken)-")
           print("fullName-\(fullName)-")
           print("givenName-\(givenName)-")
           print("familyName-\(familyName)-")
           print("emaill-\(email)-")
           print("img-\(img)-")*/
        // ...
      }
      
      func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
                withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        if let error = error {
                 if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                   print("The user has not signed in before or")
                 } else {
                   print("\(error.localizedDescription)")
                 }
      }
    
    }
}

