//
//  NotificationViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 13/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController
{
      var categoryName: [String] = ["ATTEMPT TEST", "COMPLETED", "TOTAL TEST"]

    @IBOutlet weak var tableView_noti: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: IBAction  Method
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
   
}
extension NotificationViewController : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView_noti.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationTableViewCell
        
        cell.lblTitle.text = categoryName[indexPath.row]
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 110
    }
}

