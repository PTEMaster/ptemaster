//
//  ChangePasswordViewController.swift
//  PTEMaster
//
//  Created by mac on 12/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    // MARK:- Check Vaildation
    func isValid() -> Bool {
       if !Util.isValidString(tfNewPassword.text!) {
            Util.showAlertWithMessage("Please enter Password", title: Key_Alert)
            return false
        }
        else if tfNewPassword.text?.count ?? 0 < 6 {
            Util.showAlertWithMessage("Password must have 6 characters", title: Key_Alert)
            return false
        }
        else if !Util.isValidString(tfConfirmPassword.text!) {
            Util.showAlertWithMessage("Please enter confirm password", title: Key_Alert)
            return false
        } else if tfConfirmPassword.text! != tfNewPassword.text! {
            Util.showAlertWithMessage("Password Doesn't Match", title: Key_Alert)
            return false
        }
        return true
    }
    
    
    
    //MARK: - All action
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonUpdate(_ sender: Any) {
        if isValid() {
          ChangePasswordApi()
        }
    }
    
    
    //MARK: - Sign Up Api
    func ChangePasswordApi() {
        if appDelegate.isNetworkAvailable {
          appDelegate.showHUD()
          let postParams: [String: AnyObject] =
                           [ "password" : tfNewPassword.text as AnyObject,
                             "confpassword" : tfConfirmPassword.text as AnyObject,
                             "user_id": AppDataManager.shared.loginData.id as AnyObject]
            print(BaseUrl + RequestPath.resetPassword.rawValue)
            print(postParams)
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.resetPassword.rawValue, method: .post, headers: nil) { (result) in
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print(response.response!) // URL response
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value != nil {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                           Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["message"].string, isWithCancel: false){
                               self.navigationController?.popViewController(animated: true)
                           }
                                    
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }else {
                                
                                Util.showAlertWithMessage("Please try again", title: Key_Alert)
                            }
                        
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
            
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    
    
   

}
