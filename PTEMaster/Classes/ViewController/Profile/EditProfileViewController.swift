//
//  EditProfileViewController.swift
//  PTEMaster
//
//  Created by mac on 12/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var tfName: UITextField!
    var imageData = UIImage().jpegData(compressionQuality: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
setData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func doclickonChangePass(_ sender: Any)
    {
        let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
     
    @IBAction func actionEditProfile(_ sender: Any) {
         Util.openImagePicker()
    }
    @IBAction func doclickonDone(_ sender: Any)
    {
        if   isValid(){
        updateProfile()
        }
    }
    // MARK:- Check Vaildation
    func isValid() -> Bool {
       if !Util.isValidString(tfName.text!) {
            Util.showAlertWithMessage("Please enter name", title: Key_Alert)
            return false
        }
        
        else if !Util.isValidString(tfMobileNumber.text!) {
            Util.showAlertWithMessage("Please enter mobile number", title: Key_Alert)
            return false
        }
        return true
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setData() {
        
        self.tfName.text = AppDataManager.shared.loginData.name.validate == "" ? "N/A" : AppDataManager.shared.loginData.name.validate
        
        self.tfMobileNumber.text = AppDataManager.shared.loginData.mobile.validate == "" ? "N/A" : AppDataManager.shared.loginData.mobile.validate
    
        
        imgProfile.image = #imageLiteral(resourceName: "app_icon")
    
        if (AppDataManager.shared.loginData.image! == "")
        {
            imgProfile.image = #imageLiteral(resourceName: "profile-img")
        }
        else
        {
            if let url = URL(string: BaseUrlProfileImg+AppDataManager.shared.loginData.image!) {
                imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "test_icon"))
            }
        }
        
         self.imageData = imgProfile.image!.jpegData(compressionQuality: 1)
    }
    
    
    
    func updateProfile()
     {
         if appDelegate.isNetworkAvailable {
             let postParams: [String: AnyObject] =
                 [ "name" : tfName.text as AnyObject,
                   "mobile" : tfMobileNumber.text as AnyObject,
                   "user_id": AppDataManager.shared.loginData.id as AnyObject]
             
             appDelegate.showHUD()
             print("Post Parameter is:\(postParams)")
             
             Alamofire.upload(multipartFormData: { (multipartFormData) in
                 for (key, value) in postParams {
                     multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                 }
                 
                 if ((self.imageData!.count) > 0)
                 {
                     multipartFormData.append(self.imageData!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                 }
                 else
                 {
                     
                 }
                 
             }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.updateProfileImage.rawValue, method: .post, headers: nil) { (result) in
                 
                 switch result
                 {
                 case .success(let upload, _, _):
                     upload.responseJSON { response in
                         do
                         {
                             appDelegate.hideHUD()
                             if response.result.value == nil {
                                 Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                             }else {
                                 
                                 let swiftyJsonResponseDict = JSON(response.result.value!)
                                 if swiftyJsonResponseDict ["status"].intValue == 1 {
                                     
                                     print(" swifty json print \(swiftyJsonResponseDict)")
                                     
                                     if let loginDict = swiftyJsonResponseDict["data"].dictionary {
                                         print(">>>> \(loginDict)")
                                         
                                         let loginDataModel = LoginModel(loginDict)
                                         AppDataManager.shared.loginData = loginDataModel
                                         UserDefaults.standard.set(loginDataModel.getDictionary(), forKey: loginDetail)
                                         
                                         self.setData()
                                        Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false){
                                                                      self.navigationController?.popViewController(animated: true)
                                                                  }

                                     }
                                 }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                     Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                 }
                             }
                         }
                     }
                 case .failure(let error):
                     appDelegate.hideHUD()
                     print("Error in upload: \(error.localizedDescription)")
                 }
             }
         } else {
             Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
         }

     }
 
}


//MARK:- image picker View Delegate
extension EditProfileViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
     let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imgProfile.contentMode = .scaleAspectFill
            imgProfile.image = pickedImage
        }
         self.imageData = imgProfile.image!.jpegData(compressionQuality: 1)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("")
        self.dismiss(animated: true, completion: nil)
    }
    
}
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
