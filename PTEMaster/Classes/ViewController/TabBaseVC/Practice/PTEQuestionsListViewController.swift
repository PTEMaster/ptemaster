//
//  PTEQuestionsListViewController.swift
//  PTEMaster
//
//  Created by mac on 21/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTEQuestionsListViewController: UIViewController {
 
    var categoryId:String = ""
    var currentRow: Int = 0
    var questionType:Int = 0
    var page:Int = 0
    var isDownloading = false
    @IBOutlet weak var tableQuestions: UITableView!
    @IBOutlet weak var navigationbars: UINavigationBar!
    @IBOutlet weak var titleNavigationBar: UINavigationItem!
    var pointContentOffset = CGPoint.zero
    var questionList = [PracticeQestionModel]()
    var titles:String?
    var dictMaxfreq = [String:Any]()
   

    override func viewDidLoad() {
        super.viewDidLoad()
     
            self.titleNavigationBar.title = self.titles
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        questionList = []
         self.tableQuestions.reloadDataInMain()
         self.getquestionsListApi()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func actions(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension PTEQuestionsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = questionList[indexPath.row]
        let maxfreq = self.dictMaxfreq["maxfreq"] as! String
        let intMaxfreq = Double(maxfreq)
        let frequency = Double(questionList[indexPath.row].frequency!)
        let result = (frequency! / intMaxfreq!) * 100
        let progress = Float(result)
        
        if dict.categoryType == CategoryType.describeImg {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PTEQuestionImageTableViewCell", for: indexPath) as! PTEQuestionImageTableViewCell
            cell.setData(questions: questionList[indexPath.row], index: indexPath.row)
            
            cell.progressView.progress = progress / 100
            if indexPath.row == (questionList.count - 1) && (tableQuestions.contentOffset.y > pointContentOffset.y) {
                                      if !isDownloading {
                                          isDownloading = true
                                          if questionList.count % 10 == 0 {
                                              page += 10
                                              self.getquestionsListApi()
                                          }
                                      }
                                  }
            return cell
        } else if dict.categoryType == CategoryType.reOrderParagraph {
             let cell = tableView.dequeueReusableCell(withIdentifier: "PTEQuestionReOrderTableViewController", for: indexPath) as! PTEQuestionReOrderTableViewController
            cell.setData(questions: questionList[indexPath.row], index: indexPath.row)
           
            cell.progressView.progress = progress / 100
            
           if indexPath.row == (questionList.count - 1) && (tableQuestions.contentOffset.y > pointContentOffset.y) {
                                      if !isDownloading {
                                          isDownloading = true
                                          if questionList.count % 10 == 0 {
                                              page += 10
                                              self.getquestionsListApi()
                                          }
                                      }
                                  }
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PTEQuestionTableViewCell", for: indexPath) as! PTEQuestionTableViewCell
            cell.setData(questions: questionList[indexPath.row], index: indexPath.row)
            cell.progressView.progress = progress / 100
            
                    if indexPath.row == (questionList.count - 1) && (tableQuestions.contentOffset.y > pointContentOffset.y) {
                           if !isDownloading {
                               isDownloading = true
                               if questionList.count % 10 == 0 {
                                   page += 10
                                   self.getquestionsListApi()
                               }
                           }
                       }
        
        /*let lastRowIndex = tableView.numberOfRows(inSection: tableView.numberOfSections-1)
            if (indexPath.row == lastRowIndex - 1) {
                print("last row selected")
                page += 10    ///
                self.getquestionsListApi()
            }*/
            return cell
        }
        
       // return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let practiceDetailVC = PracticeDetailVC.instance() as! PracticeDetailVC
        practiceDetailVC.currentRow = indexPath.row
        practiceDetailVC.categoryId = "\(categoryId)"
        practiceDetailVC.questionType = questionType
        practiceDetailVC.practiceQestionSelect =  questionList[indexPath.row]
        practiceDetailVC.titles = self.titles
        self.navigationController?.pushViewController(practiceDetailVC, animated: true)
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let animation = AnimationFactory.makeFadeAnimation(duration: 0.2, delayFactor: 0.02)
//           let animator = Animator(animation: animation)
//           animator.animate(cell: cell, at: indexPath, in: tableView)
//       }
}




extension PTEQuestionsListViewController {
    
    func getquestionsListApi() {
         appDelegate.showHUD()
        if appDelegate.isNetworkAvailable {
         pointContentOffset = tableQuestions.contentOffset
            var postParams = [String: Any]()
          //  postParams["user_id"] = AppDataManager.shared.loginData.id
                  //  postParams["categoryId"] = categoryId
            postParams["userid"] = AppDataManager.shared.loginData.id
            postParams["cat_id"] = categoryId
            postParams["qtype"] = "0"
            postParams["qquality"] = "0"
            postParams["myprac"] = "0"
            postParams["labelselect"] = "0"
            postParams["frecuency"] = questionType
            postParams["startfrom"] = page
           //  DispatchQueue.main.async {
               
           // }
        //    https://api.masterpte.com.au/api/webservices/practice_question
           print("Api:-\(BaseUrl + RequestPath.practice_question.rawValue)")
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.practice_question.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                      //  appDelegate.hideHUD()
                        do
                        {
                            DispatchQueue.main.async {
                               // DispatchQueue.main.async { [unowned self] in
                            // self.setupScreen()
                            //  Loader.hideLoader()
                            if response.result.value == nil {
                                self.tableQuestions.displayBackgroundImageWithText(text: AppConstant.couldNotConnect , fontStyle: "SansaGLACIALINDIFFERENCE-BOLD", fontSize: 15, imgName: "cloud")
                                    self.tableQuestions.reloadData()
                               // Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            } else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                
                         print("swiftyJsonResponseDict >>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary, let mockqestion = dashboradDataDict["mockqestion"]?.array , let maxfreq = dashboradDataDict["maxfreq"]?.array {
                                    
                                    // print(">>> mockqestion >>> \(mockqestion)")
                                    self.dictMaxfreq = maxfreq[0].dictionaryObject!
                                    
                                    for item in mockqestion {
                                        let mockquestions = PracticeQestionModel(json: item)
                                        self.questionList.append(mockquestions)
                                    }
                                    self.isDownloading = false
                                    self.tableQuestions.reloadDataInMain()
                                    if self.questionList.count > 0{
                                        // self.tableView_MockList.displayBackgroundText(text: "", fontStyle: "helvetica", fontSize: 15)
                                        self.tableQuestions.displayBackgroundImageWithText(text:  "" , fontStyle: "SansaGLACIALINDIFFERENCE-BOLD", fontSize: 15, imgName: "")
                                        self.tableQuestions.reloadData()
                                    }
                                    appDelegate.hideHUD()
                                }
                                    
                                // fail case
                                else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                     self.isDownloading = true
                                    appDelegate.hideHUD()
                                    
                                    if self.questionList.count == 0{
                                                // self.tableView_MockList.displayBackgroundText(text: "", fontStyle: "helvetica", fontSize: 15)
                                        self.tableQuestions.displayBackgroundImageWithText(text: swiftyJsonResponseDict ["msg"].string ?? "" , fontStyle: "SansaGLACIALINDIFFERENCE-BOLD", fontSize: 15, imgName: "cloud")
                                                  self.tableQuestions.reloadData()
                                              }
                                 //   Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                            
                    }
                    }
                case .failure(let error):
                     self.isDownloading = true
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }

}



