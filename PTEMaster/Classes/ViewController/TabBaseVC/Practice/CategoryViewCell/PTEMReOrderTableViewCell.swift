//
//  PTEMReOrderTableViewCell.swift
//  PTEMaster
//
//  Created by mac on 09/06/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation

class PTEMReOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblOptions: UILabel!
    @IBOutlet weak var viewLbl: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblOptions.numberOfLines = 0
        viewLbl.layer.borderColor = UIColor.blue.cgColor
        viewLbl.layer.borderWidth = 1.0
        viewLbl.layer.cornerRadius = 5
        lblOptions.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblOptions.sizeToFit()
         // cell.clipsToBounds = true
        //self.contentView.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
   }
