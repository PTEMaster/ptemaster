//
//  PTEQuestionImageTableViewCell.swift
//  PTEMaster
//
//  Created by mac on 22/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit
import SDWebImage

class PTEQuestionImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblQuestionsCounts: UILabel!
    @IBOutlet weak var imgQuestions: UIImageView!
    @IBOutlet weak var lblQuestionsNumber: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(questions:PracticeQestionModel , index:Int) {
        // lblQuestions.text = questions.description
        // lblQuestionsNumber.text = "#\(questions.addons?.question_id ?? "0")"
        let imagesUrl = URL(string: ImageBaseUrl.questionImageUrl + questions.image!)
        imgQuestions.sd_setImage(with: imagesUrl, placeholderImage: UIImage(named: "test_icon"))
       // imgQuestions.kf.setImage(with: imagesUrl)
        lblQuestionsCounts.text = "\(index + 1)"
        lblQuestionsCounts.clipsToBounds = true
        lblQuestionsCounts.layer.cornerRadius = lblQuestionsCounts.frame.size.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
