//
//  PTEQuestionReOrderTableViewController.swift
//  PTEMaster
//
//  Created by mac on 09/06/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//


import UIKit

class PTEQuestionReOrderTableViewController: UITableViewCell , UITableViewDelegate, UITableViewDataSource{
   
    
    
    @IBOutlet weak var lblQuestionsCounts: UILabel!
    @IBOutlet weak var lblQuestionsNumber: UILabel!
    
   @IBOutlet weak var progressView: UIProgressView!
   
    @IBOutlet weak var tableViewOptions: UITableView!
    @IBOutlet weak var constraintCollectionviewHeight: NSLayoutConstraint!
    
    var arrOption = [OptionsModel]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setData(questions:PracticeQestionModel , index:Int) {
    constraintCollectionviewHeight.constant = 0
        var heights : CGFloat = 0.0
        for item in questions.arrOptions {
            let height = item.option?.heightWithConstrainedWidth(width: tableViewOptions.frame.size.width , font: UIFont.systemFont(ofSize: 16))
            heights = heights + height! + 26
        }
        constraintCollectionviewHeight.constant = CGFloat(heights)
   
        arrOption = questions.arrOptions
        tableViewOptions.delegate = self
        tableViewOptions.dataSource = self
        tableViewOptions.tag  = index
        lblQuestionsNumber.text = "#\(questions.id ?? "0")"
        lblQuestionsCounts.text = "\(index + 1)"
        lblQuestionsCounts.clipsToBounds = true
        lblQuestionsCounts.layer.cornerRadius = lblQuestionsCounts.frame.size.height / 2
        
        self.tableViewOptions.reloadDataInMain()
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return arrOption.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PTEMReOrderTableViewCell", for: indexPath) as! PTEMReOrderTableViewCell
          let dict = arrOption[indexPath.row]
          cell.lblOptions.text = dict.option
          return cell
      }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
   /*
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PTEMReOrderTableViewCell", for: indexPath) as! PTEMReOrderTableViewCell
        let dict = arrOption[indexPath.row]
        cell.lblOptions.text = dict.option
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }

    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
   
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = arrOption[indexPath.row].option?.heightWithConstrainedWidth(width: tableViewOptions.frame.size.width , font: UIFont.systemFont(ofSize: 16))
        return CGSize(width: tableViewOptions.frame.width  , height: height!)
    }*/
}








extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
}
