//
//  PTESortViewController.swift
//  PTEMaster
//
//  Created by mac on 21/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTESortViewController: UIViewController {
    @IBOutlet weak var navigationCustoms: UINavigationBar!
    @IBOutlet weak var btnMP: UIButton!
    @IBOutlet weak var btnDN: UIButton!
    @IBOutlet weak var btnNQF: UIButton!
    @IBOutlet weak var btnHRQ: UIButton!
    @IBOutlet weak var btnWP: UIButton!
    var currentRow : Int = 0
    var categoryId = ""
    var checkStatus:Bool = true
    var titles:String?
    var questionType:Int = 0
    var isMoveToPlan  = false
    
    @IBOutlet weak var navigationItems: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItems.title = titles
        btnMP?.isSelected = true
        btnDN?.isSelected = false
        btnNQF?.isSelected = false
        btnHRQ?.isSelected = false
        // Do any additional setup after loading the view.
        lastminutestudyNew()
    }
    
    @IBAction func actionBarButtonItems(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 205:
            if checkStatus {
                if questionType == 4 && isMoveToPlan{
                    Util.showAlertWithMessage("You have to purchase VIP token", title: AppConstant.appName)
                }else{
                    let vc = PTEQuestionsListViewController.instance() as! PTEQuestionsListViewController
                    vc.categoryId = categoryId
                    vc.currentRow = currentRow
                    vc.titles = navigationItems.title
                    vc.questionType = questionType
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            } else {
                Util.showAlertWithMessage("Please select options", title: AppConstant.appName)
            }
        case 206:
            self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
    
    
    @IBAction func actionSorts(_ sender: UIButton) {
        checkStatus = true
        switch sender.tag {
        case 201:
            questionType = 0
            btnMP?.isSelected = true
            btnDN?.isSelected = false
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = false
            btnWP?.isSelected = false
        case 202:
            questionType = 1
            btnMP?.isSelected = false
            btnDN?.isSelected = true
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = false
            btnWP?.isSelected = false
        case 203:
            questionType = 2
            btnMP?.isSelected = false
            btnDN?.isSelected = false
            btnNQF?.isSelected = true
            btnHRQ?.isSelected = false
            btnWP?.isSelected = false
        case 204:
            questionType = 3
            btnMP?.isSelected = false
            btnDN?.isSelected = false
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = true
            btnWP?.isSelected = false
        case 205:
            questionType = 4
            btnMP?.isSelected = false
            btnDN?.isSelected = false
            btnNQF?.isSelected = false
            btnHRQ?.isSelected = false
            btnWP?.isSelected = true
        default:
            break
        }
    }
    
    func lastminutestudyNew(){
        if appDelegate.isNetworkAvailable {
            var postParams = [String: Any]()
            postParams["user_id"] = AppDataManager.shared.loginData.id
            appDelegate.showHUD()
              print("Api:-http://3.25.96.167/api/lastminutestudyNew")
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: "http://3.25.96.167/api/lastminutestudyNew", method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        appDelegate.hideHUD()
                        do
                        {
                            DispatchQueue.main.async {
                                if response.result.value == nil {
                                    Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                                }else {
                                    let swiftyJsonResponseDict = JSON(response.result.value!)
                                    if swiftyJsonResponseDict ["success"].string == "1" {
                                        let toalCreditTime = swiftyJsonResponseDict ["toalCreditTime"].intValue
                                        
                                        if let dashboradDataDict = swiftyJsonResponseDict["user"].dictionary {
                                            let is_student = dashboradDataDict ["is_student"]?.string
                                            let full_access = dashboradDataDict ["full_access"]?.string
                                            
                                            if(is_student == "1" || toalCreditTime != 0 || full_access == "1"){
                                                //Working
                                                self.isMoveToPlan = false
                                            }else{
                                                // Move to plan
                                                self.isMoveToPlan = true
                                            }
                                        }
                                    }
                                    print(swiftyJsonResponseDict)
                                    appDelegate.hideHUD()
                                }
                            }
                        }
                    }
                    appDelegate.hideHUD()
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
       
    }
    
    
    
}
