//
//  PTEAlertViewController.swift
//  PTEMaster
//
//  Created by mac on 22/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

protocol AlertDelegate:NSObject {
    func dismissViewController(_ viewController:PTEAlertViewController)
}

class PTEAlertViewController: UIViewController {

    weak var delegate:AlertDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func actionYes(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.dismissViewController(self)
    }
    
    @IBAction func actionNo(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
