//
//  PracticeSectionCell.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 15/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class PracticeSectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewIndicator: UIView!
    
}
