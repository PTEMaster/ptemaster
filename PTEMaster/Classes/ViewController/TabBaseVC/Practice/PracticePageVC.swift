//
//  PracticePageVC.swift
//  PTEMaster
//
//  Created by mac on 15/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

protocol PracticePageVCProtocol: NSObjectProtocol {
    func updateSelectedCell(pageIndex: Int)
}


class PracticePageVC: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var pages = [UIViewController]()
    var currentPageIndex: Int?
    private var pendingIndex: Int?
    
    weak var delegateProperty: PracticePageVCProtocol?
    
    var speakingViewController: SpeakingViewController?
    var writingViewController: WritingViewController?
    var readingViewController: ReadingViewController?
    var listeningViewController: ListeningViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setScreenLayout()

        praticeApi()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    

    func setScreenLayout() {
        currentPageIndex = 0
        
        speakingViewController = SpeakingViewController.instance() as? SpeakingViewController
        pages.append(speakingViewController!)
        
        writingViewController = WritingViewController.instance() as? WritingViewController
        pages.append(writingViewController!)
        
        readingViewController = ReadingViewController.instance() as? ReadingViewController
        pages.append(readingViewController!)
        
        listeningViewController = ListeningViewController.instance() as? ListeningViewController
        pages.append(listeningViewController!)
        
        dataSource = self
        delegate = self
        setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
    }
    
    // MARK:- Practice Category Api
    func praticeApi () {
        if appDelegate.isNetworkAvailable {
            appDelegate.showHUD()
            Alamofire.upload(multipartFormData: { (multipartFormData) in }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.practiceCategoryApi.rawValue, method: .post, headers: nil) { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do {
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            } else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    if let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(dashboradDataDict)")
                                        self.updateDataInScreens(dashboradDataDict)
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    
    func updateDataInScreens(_ response: [String: JSON]) {
        if let arrCategory = response["category"]?.array {
            for dict in arrCategory {
                let practiceInfo = PracticeCategoryModel(json: dict)
                if let category = practiceInfo.category {
                    if category == "Speaking Section" {
                        speakingViewController?.setCategoryDataAndUpdate(practiceInfo)
                    } else if category == "Writing Section" {
                        writingViewController?.setCategoryDataAndUpdate(practiceInfo)
                    } else if category == "Reading Section" {
                        readingViewController?.setCategoryDataAndUpdate(practiceInfo)
                    } else if category == "Listening Section" {
                        listeningViewController?.setCategoryDataAndUpdate(practiceInfo)
                    }
                }
            }
        }
    }
    
    //call  view controller according to the page number
    func setCurrentIndex(index: Int) {
        if index < pages.count {
            if index < currentPageIndex! {
                setViewControllers([pages[index]], direction: .reverse, animated: true, completion: nil)
            } else {
                setViewControllers([pages[index]], direction: .forward, animated: true, completion: nil)
            }
            currentPageIndex = index
        }
    }
    
    
    // MARK:- UIPageViewControllerDelegate, UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == pages.count-1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == 0 {
            return nil
        }
        let previousIndex = abs((currentIndex - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.index(of: pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentPageIndex = pendingIndex
            if let index = currentPageIndex {
               delegateProperty?.updateSelectedCell(pageIndex: index)
            }
        }
    }
    

    
}
