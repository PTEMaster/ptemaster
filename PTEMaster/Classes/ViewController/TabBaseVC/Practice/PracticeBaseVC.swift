//
//  PracticeBaseVC.swift
//  PTEMaster
//
//  Created by mac on 15/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class PracticeBaseVC: UIViewController {

    @IBOutlet weak var collectionVeiw: UICollectionView!
    
    var propertyDetailPageVC: PracticePageVC?
    var sections = [PracticSectionModel]()
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setScreenLayout()
        updateFreeToken()
        DispatchQueue.global().async {
            do {
                let update = try self.isUpdateAvailable()
                DispatchQueue.main.async {
                    print(update.0)
                    print(update.1)
                    
                    if (UserDefaults.standard.value(forKey: "version") as? String) != nil{
                   let version = UserDefaults.standard.value(forKey: "version")as? String
                        if version  == update.1{
                            return
                        }
                        
                    }
                    if update.0{
                        DispatchQueue.main.async(execute: {
                            if (UserDefaults.standard.value(forKey: "version") as? String) != nil{
                           let version = UserDefaults.standard.value(forKey: "version")as? String
                                if version  == update.1{
                                    return
                                }
                            }
                            Util.showAlertWithCancelCallback("New Version Available", message: "A new version of PTE Master Application is available, Please update the app to new version.", isWithCancel: true) { (btnTitle) in
                                if btnTitle == "Ok" {
                                    //URL(string:"itms-apps://apple.com/app/id1464871486")
                                    if let url = URL(string:"https://apps.apple.com/us/app/pte-master/id1464871486") {
                                        UIApplication.shared.open(url)
                                    }
                                } else {
                                    UserDefaults.standard.set(update.1, forKey: "version")
                                    print("Cancel Button")
                                }
                            }
                        })
                    }
                }
            } catch {
                print(error)
            }
        }
    }
    
    func isUpdateAvailable() throws -> (Bool , String) {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
          //  print(result)
           // print( result["version"] as? String)
            
           let bb = version != currentVersion
            return (bb ,result["version"] as! String )
        }
        throw VersionError.invalidResponse
    }
    
    func setScreenLayout() {
        sections.append(PracticSectionModel(title: "Speaking", selectedImage: "speaking_icon", unSelectedImage: "speaking_big_icon"))
        sections.append(PracticSectionModel(title: "Writing", selectedImage: "writing_icon", unSelectedImage: "writing_big_icon"))
        sections.append(PracticSectionModel(title: "Reading", selectedImage: "reading_icon", unSelectedImage: "reading_big_icon"))
        sections.append(PracticSectionModel(title: "Listening", selectedImage: "listening_icon", unSelectedImage: "listening_big_icon"))
        collectionVeiw.reloadData()
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PracticePageVC {
            propertyDetailPageVC = viewController
            viewController.delegateProperty = self
        }
    }



    
    
    func updateFreeToken(){
        if appDelegate.isNetworkAvailable {
            var postParams = [String: Any]()
            postParams["user_id"] = AppDataManager.shared.loginData.id
            appDelegate.showHUD()
              print("Api:-http://3.25.96.167/api/updateFreeToken")
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: "http://3.25.96.167/api/updateFreeToken", method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        appDelegate.hideHUD()
                        do
                        {
                            DispatchQueue.main.async {
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                             print("done")
                                appDelegate.hideHUD()
                            }
                            }
                        }
                    }
                    appDelegate.hideHUD()
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
       
    }
}

extension PracticeBaseVC: PracticePageVCProtocol {
    
    func updateSelectedCell(pageIndex: Int) {
        selectedIndex = pageIndex
        collectionVeiw.reloadData()
        collectionVeiw.scrollToItem(at: IndexPath(row: pageIndex, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
    
}



extension PracticeBaseVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PracticeSectionCell", for: indexPath) as! PracticeSectionCell
        cell.viewIndicator.isHidden = selectedIndex != indexPath.item
        cell.lblTitle.text = sections[indexPath.row].title
        cell.lblTitle.textColor = selectedIndex == indexPath.item ? cell.viewIndicator.backgroundColor : UIColor.darkGray
        let imageName = selectedIndex == indexPath.item ? sections[indexPath.row].selectedImage : sections[indexPath.row].unSelectedImage
        cell.imgView.image = UIImage(named: imageName)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: collectionView.frame.size.height)
        if sections.indices.contains(indexPath.row) {
            size.width = sections[indexPath.row].titleWidth + 12 + 12 + 5 + 18
            // left padding + right padding + distance between image and label + image width
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionVeiw.reloadData()
       self.propertyDetailPageVC?.setCurrentIndex(index: indexPath.item)
        collectionVeiw.scrollToItem(at: IndexPath(row: indexPath.item, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
}



struct PracticSectionModel {
    var title: String {
        didSet {
            self.titleWidth = title.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        }
    }
    var selectedImage: String
    var unSelectedImage: String
    var titleWidth: CGFloat
}


extension PracticSectionModel {
    
    init(title: String, selectedImage: String, unSelectedImage: String) {
        self.title = title
        self.selectedImage = selectedImage
        self.unSelectedImage = unSelectedImage
        self.titleWidth = title.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
    }
    
}

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}
