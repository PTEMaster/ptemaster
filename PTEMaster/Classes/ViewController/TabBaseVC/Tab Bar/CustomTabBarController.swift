//
//  CustomTabBarController.swift
//  PTEMaster
//
//  Created by CTIMac on 19/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class CustomTabBarController: UIViewController
{
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblSubscription: UILabel!
    @IBOutlet weak var lblPractice: UILabel!
    @IBOutlet weak var lblDashboard: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var img_history: UIImageView!
    @IBOutlet weak var img_sub: UIImageView!
    @IBOutlet weak var img_practice: UIImageView!
    @IBOutlet weak var img_dash: UIImageView!
    @IBOutlet weak var view_history: UIView!
    @IBOutlet weak var view_subscription: UIView!
    @IBOutlet weak var view_dashboard: UIView!
    @IBOutlet weak var view_practice: UIView!
    @IBOutlet weak var view_token: UIView!
    
    @IBOutlet weak var constBottomViewLeading: NSLayoutConstraint!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lblToken: UILabel!
    @IBOutlet weak var imgToken: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        img_dash.image = #imageLiteral(resourceName: "dashboard_deactive")
        img_practice.image = #imageLiteral(resourceName: "practice_active")
        img_sub.image = #imageLiteral(resourceName: "subscription_deactive")
        imgToken.image = #imageLiteral(resourceName: "subscription_deactive")
        img_history.image = #imageLiteral(resourceName: "history_deactive")
        lblPractice.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
        lblDashboard.textColor = UIColor.white
        lblSubscription.textColor = UIColor.white
        lblHistory.textColor = UIColor.white
        lblToken.textColor = UIColor.white
        moveBottomArrowImage(withViewPoint: view_practice)
//            let vc = PracticeViewController.instance()
        let vc = PracticeBaseVC.instance()
        updateContentViewChildController(vc)
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.UserProfilePick(_:)))
        img_profile.addGestureRecognizer(tap)
        img_profile.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if (AppDataManager.shared.loginData.image! == "")
        {
            img_profile.image = #imageLiteral(resourceName: "profile-img")
        }
        else
        {
            if let url = URL(string: BaseUrlProfileImg+AppDataManager.shared.loginData.image!) {
                
                 img_profile.sd_setImage(with: url, placeholderImage: UIImage(named: "test_icon"))
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func UserProfilePick(_ sender: UITapGestureRecognizer)
    {
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(profileVC, animated: true)
        //alertOpenCamraOrGalary()
        //type = "1"
    }
    
    // add child class in container view
    func updateContentViewChildController(_ vc: UIViewController) {
        self.addChild(vc)
        vc.view.frame = self.containerView.bounds;
        self.containerView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    // move blue image on bottom in tabbar
    func moveBottomArrowImage(withViewPoint: UIView){
        let constPoint = withViewPoint.frame.origin.x
        UIView.animate(withDuration: 0.3, delay: 0.1 , usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options:.curveEaseOut , animations: {
            self.constBottomViewLeading.constant = constPoint
            self.view.layoutIfNeeded()
        }, completion: {(Bool)  in
        })
    }
    
    
    //MARK: IBAction
   /* @IBAction func doclickonNotification(_ sender: Any)
    {
       // let vc = NotificationViewController.instance()
       // self.navigationController?.pushViewController(vc, animated: true)
    }*/
    
    @IBAction func btnTabBarSelection_Action(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1:
            print("Fifth")
            img_dash.image = #imageLiteral(resourceName: "dashboard_active")
            img_practice.image = #imageLiteral(resourceName: "practice_deactive")
            img_sub.image = #imageLiteral(resourceName: "subscription_deactive")
            img_history.image = #imageLiteral(resourceName: "history_deactive")
            lblDashboard.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
            lblPractice.textColor = UIColor.white
            lblSubscription.textColor = UIColor.white
            lblHistory.textColor = UIColor.white
            lblToken.textColor = UIColor.white
            moveBottomArrowImage(withViewPoint: view_dashboard)
            let vc = DashboardViewController.instance()
            updateContentViewChildController(vc)
        case 2 :
            print("second")
            img_dash.image = #imageLiteral(resourceName: "dashboard_deactive")
            img_practice.image = #imageLiteral(resourceName: "practice_active")
            img_sub.image = #imageLiteral(resourceName: "subscription_deactive")
            imgToken.image = #imageLiteral(resourceName: "subscription_deactive")
            img_history.image = #imageLiteral(resourceName: "history_deactive")
            lblPractice.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
            lblDashboard.textColor = UIColor.white
            lblSubscription.textColor = UIColor.white
            lblHistory.textColor = UIColor.white
            lblToken.textColor = UIColor.white
            moveBottomArrowImage(withViewPoint: view_practice)
//            let vc = PracticeViewController.instance()
            let vc = PracticeBaseVC.instance()
            updateContentViewChildController(vc)
            
        case 3 :
            print("Third")
            lblSubscription.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
            lblDashboard.textColor = UIColor.white
            lblPractice.textColor = UIColor.white
            lblHistory.textColor = UIColor.white
            lblToken.textColor = UIColor.white
            img_dash.image = #imageLiteral(resourceName: "dashboard_deactive")
            img_practice.image = #imageLiteral(resourceName: "practice_deactive")
            img_sub.image = #imageLiteral(resourceName: "subscription_active")
            imgToken.image = #imageLiteral(resourceName: "subscription_deactive")
            img_history.image = #imageLiteral(resourceName: "history_deactive")
            moveBottomArrowImage(withViewPoint: view_subscription)
            let vc = SubscriptionViewController.instance()
            updateContentViewChildController(vc)
        case 4 :
            
            print("Fourth")
            lblHistory.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
            lblDashboard.textColor = UIColor.white
            lblPractice.textColor = UIColor.white
            lblSubscription.textColor = UIColor.white
            lblToken.textColor = UIColor.white
            img_dash.image = #imageLiteral(resourceName: "dashboard_deactive")
            img_practice.image = #imageLiteral(resourceName: "practice_deactive")
            img_sub.image = #imageLiteral(resourceName: "subscription_deactive")
            imgToken.image = #imageLiteral(resourceName: "subscription_deactive")
            img_history.image = #imageLiteral(resourceName: "history_active")
            moveBottomArrowImage(withViewPoint: view_history)
            
            // FIXME: HIMANSHU PAL 11/8/20
           // let vc = HistoryViewController.instance() as! HistoryViewController
             let vc = StudyVC.instance() as! StudyVC
            updateContentViewChildController(vc)
        case 5:
            print("Third")
            lblToken.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
            lblDashboard.textColor = UIColor.white
            lblPractice.textColor = UIColor.white
            lblHistory.textColor = UIColor.white
            //lblToken.textColor = UIColor.white
            img_dash.image = #imageLiteral(resourceName: "dashboard_deactive")
            img_practice.image = #imageLiteral(resourceName: "practice_deactive")
            imgToken.image = #imageLiteral(resourceName: "subscription_active")
            img_history.image = #imageLiteral(resourceName: "history_deactive")
            moveBottomArrowImage(withViewPoint: view_subscription)
            let vc = PTEMTokenViewController.instance()
            updateContentViewChildController(vc)
            //viewColorChange(color: colorProduct)
        default:
            break
        }

    }

}

