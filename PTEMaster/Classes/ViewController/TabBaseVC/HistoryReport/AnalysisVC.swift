//
//  AnalysisVC.swift
//  PTEMaster
//
//  Created by mac on 26/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import WebKit

class AnalysisVC: UIViewController {
    
    @IBOutlet weak var displayView: UIView!
    var webView: WKWebView!
    var testId = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.initialSetup()
        }
    }
    
    //MARK:- webview method
    func initialSetup() {
        URLCache.shared.removeAllCachedResponses()
        
        var urlString = ""
        
        urlString = "https://www.masterpte.com.au/api/historyDetailswebview/69/65"
        print(urlString)
       //  urlString = "https://www.masterpte.com.au/api/historyDetailswebview/\(AppDataManager.shared.loginData.id!)/\(testId)"
       // print(urlString)
        if urlString.count > 0, let url = URL(string: urlString) {
            webView = WKWebView(frame: displayView.bounds)
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.isUserInteractionEnabled = true
            webView.navigationDelegate = self
            self.displayView.addSubview(self.webView)
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension AnalysisVC: WKUIDelegate , WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        appDelegate.hideHUD()
        if let currentURL = webView.url {
            let absoluteString = currentURL.absoluteString
            print("<<< absoluteString >>> \(absoluteString)")
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        appDelegate.hideHUD()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
}
