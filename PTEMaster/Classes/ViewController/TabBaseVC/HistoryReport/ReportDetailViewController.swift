//
//  ReportDetailViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 24/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit
import PageMenu

class ReportDetailViewController: UIViewController {
    
    var pageMenu : CAPSPageMenu?

    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var view_Contain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var communicativeVC: CommunativeViewController!
    var enablingVC: EnablingSkillViewController!
     var testId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_Contain.isHidden = true
        var controllerArray: [UIViewController] = []
        
        //CompletedTestListViewController
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        
        //StarterPackViewController
        communicativeVC = mainStoryboard.instantiateViewController(withIdentifier: "CommunativeViewController") as? CommunativeViewController
        communicativeVC.title = "Communative Skills"
        controllerArray.append(communicativeVC)
    
        enablingVC = mainStoryboard.instantiateViewController(withIdentifier: "EnablingSkillViewController") as? EnablingSkillViewController
        enablingVC.title = "Enabling Skill"
        controllerArray.append(enablingVC)

        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)),
            //.bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "GLACIALINDIFFERENCE-BOLD", size: 15.0)!),
            .menuHeight(40.0),
            .menuItemWidth(150),
            .selectedMenuItemLabelColor(UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: view_header.frame.height + 10 , width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
        // Do any additional setup after loading the view.
        
        reportHistoryApi()
        
    }

    
    func reportHistoryApi() {
        if appDelegate.isNetworkAvailable {
            appDelegate.showHUD()
            
            let param : [String: Any] = ["user_id" : "\(AppDataManager.shared.loginData.id!)" , "test_id": "\(testId)"]
           // let param : [String: Any] = ["user_id" : "69" , "test_id": "65"]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.reportHistory.rawValue , method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                
                                print("swiftyJsonResponseDict >>>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                    
                                    print("dashboradDataDict  >>>>>> \(dashboradDataDict)")
                                    
                                   // self.arrReportHistory.append(dashboradDataDict)
                                    let reportHistoryInfo = ReportHistoryModel(dashboradDataDict)
                                    self.parseReponse(reportHistoryInfo)
                                    
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    
    func parseReponse(_ reportHistory: ReportHistoryModel) {
        communicativeVC.setData(reportHistory)
        enablingVC.setData(reportHistory)
        communicativeVC.configureBarChartView(reportHistory)
        enablingVC.reportHistory = reportHistory
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doclickonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
