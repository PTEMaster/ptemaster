//
//  CommunativeViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 24/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

class CommunativeViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewChart: UIView!
    
    var scoreDetails = [ScoreDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadDataInMain()
    }
    
    func setData(_ reportHistory: ReportHistoryModel) {
        scoreDetails.append(ScoreDetail(name: "Listening", score: reportHistory.scoreListeningSection ?? 0.0, scoreColor: AppColor.clr1))
        scoreDetails.append(ScoreDetail(name: "Reading", score: reportHistory.scoreReadingSection ?? 0.0, scoreColor: AppColor.clr2))
        scoreDetails.append(ScoreDetail(name: "Speaking", score: reportHistory.scoreSpeakinigSection ?? 0.0, scoreColor: AppColor.clr3))
        scoreDetails.append(ScoreDetail(name: "Writing", score: reportHistory.scoreWritingSection ?? 0.0, scoreColor: AppColor.clr4))
        if collectionView != nil {
            collectionView.reloadDataInMain()
        }
        
    }
    
    func configureBarChartView(_ reportHistory: ReportHistoryModel) {
        
        let barChart = PNBarChart(frame: CGRect(x: 0, y: 0, width: self.viewChart.frame.size.width , height: self.viewChart.frame.size.height))
        
        barChart.xLabels = ["Listening","Reading","Speaking","Writing"]
        
        let yValuesFloat  = [CGFloat(reportHistory.scoreListeningSection!), CGFloat(reportHistory.scoreReadingSection!) , CGFloat(reportHistory.scoreSpeakinigSection!) , CGFloat(reportHistory.scoreWritingSection!)]
        
        barChart.yValues = yValuesFloat
        barChart.yMaxValue = 90
        barChart.yMinValue = 0
        barChart.barWidth = 20.0
        barChart.showChartBorder = true
        barChart.barBackgroundColor = .clear
        barChart.strokeColors = [AppColor.clr1 ,AppColor.clr2 , AppColor.clr1 , AppColor.clr2]
        barChart.yLabelSum = 9
        barChart.strokeChart()
        barChart.backgroundColor = UIColor.white
        self.viewChart.addSubview(barChart)
        
    }
}


//MARK:Collection View
extension CommunativeViewController:  UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return scoreDetails.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SkillCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillCell", for: indexPath) as! SkillCollectionViewCell
        
        cell.view_bg.layer.cornerRadius = cell.view_bg.frame.size.height * 0.5
        cell.view_bg.layer.masksToBounds = true
        cell.lblNumberCount.text = "\(Int(scoreDetails[indexPath.row].score))"
        cell.view_bg.backgroundColor = scoreDetails[indexPath.row].scoreColor
        cell.lblTitle.text = scoreDetails[indexPath.row].name
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        // self.collectionView.frame.height - 5
        let sizeWidth = 120 - 5
        return CGSize(width: sizeWidth, height: 150)
    }
    
}
