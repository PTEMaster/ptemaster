//
//  AnalysisPageVC.swift
//  PTEMaster
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

protocol AnalysisPageVCProtocol: NSObjectProtocol {
    func updateSelectedCell(pageIndex: Int)
}


class AnalysisPageVC: UIPageViewController , UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var pages = [UIViewController]()
    var currentPageIndex: Int?
    private var pendingIndex: Int?
    
    weak var delegateProperty: AnalysisPageVCProtocol?
    var speakingVC: AnalysisSpeakingVC?
    var writingVC: AnalysisWritingVC?
    var readingVC: AnalysisReadingVC?
    var listeningVC: AnalysisListningVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setScreenLayout()
       // updateDataInScreens()
    }
    
    func setScreenLayout() {
        currentPageIndex = 0
        
        speakingVC = AnalysisSpeakingVC.instance() as? AnalysisSpeakingVC
        pages.append(speakingVC!)
        
        writingVC = AnalysisWritingVC.instance() as? AnalysisWritingVC
        pages.append(writingVC!)
        
        readingVC = AnalysisReadingVC.instance() as? AnalysisReadingVC
        pages.append(readingVC!)
        
        listeningVC = AnalysisListningVC.instance() as? AnalysisListningVC
        pages.append(listeningVC!)
        
        dataSource = self
        delegate = self
        setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
    }
    
    
    /*func updateDataInScreens(_ response: [String: JSON]) {
        if let arrCategory = response["category"]?.array {
            for dict in arrCategory {
                let practiceInfo = PracticeCategoryModel(json: dict)
                if let category = practiceInfo.category {
                    if category == "Speaking Section" {
                        speakingVC?.setCategoryDataAndUpdate(practiceInfo)
                    } else if category == "Writing Section" {
                        //writingVC?.setCategoryDataAndUpdate(practiceInfo)
                    } else if category == "Reading Section" {
                        //readingVC?.setCategoryDataAndUpdate(practiceInfo)
                    } else if category == "Listening Section" {
                        //listeningVC?.setCategoryDataAndUpdate(practiceInfo)
                    }
                }
            }
        }
    }*/
    
   
    
    
    func setCurrentIndex(index: Int) {
        if index < pages.count {
            if index < currentPageIndex! {
                setViewControllers([pages[index]], direction: .reverse, animated: true, completion: nil)
            } else {
                setViewControllers([pages[index]], direction: .forward, animated: true, completion: nil)
            }
            currentPageIndex = index
        }
    }
    
    
    // MARK:- UIPageViewControllerDelegate, UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == pages.count-1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == 0 {
            return nil
        }
        let previousIndex = abs((currentIndex - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.index(of: pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentPageIndex = pendingIndex
            if let index = currentPageIndex {
                delegateProperty?.updateSelectedCell(pageIndex: index)
            }
        }
    }
    

}
