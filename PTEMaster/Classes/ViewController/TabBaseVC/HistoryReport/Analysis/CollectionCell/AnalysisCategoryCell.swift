//
//  AnalysisCategoryCell.swift
//  PTEMaster
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class AnalysisCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
