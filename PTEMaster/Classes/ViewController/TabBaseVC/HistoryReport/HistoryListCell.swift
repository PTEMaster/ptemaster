//
//  HistoryListCell.swift
//  PTEMaster
//
//  Created by mac on 16/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class HistoryListCell: UITableViewCell {

    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblPurchaseDate: UILabel!
    @IBOutlet weak var lblTotalQuestion: UILabel!
    @IBOutlet weak var lblTestTime: UILabel!
    @IBOutlet weak var imgCompletedStatus: UIImageView!
    @IBOutlet weak var lblDuration: UILabel!
    
    var reportClick : (() -> Void)? = nil
    var analysisClick : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(info: HistoryListModel , reportClickTapped: @escaping() -> Void , anylysisClickTapped: @escaping() -> Void ) {
        
       
        lblTestTime.text = info.testTime != "" ? info.testTime.validate : "0"
        
        if info.completedStatut.validate == "1" {
            imgCompletedStatus.image = UIImage(named: "check_icon")
        }else {
            imgCompletedStatus.image = UIImage(named: "check_deactive")
        }
        
        lblTotalQuestion.text = info.testQuestionNumber != "" ? info.testQuestionNumber.validate : "0"
        lblPurchaseDate.text = info.buyDate != "" ? "Purchased On " + info.buyDate.validate : "0"
        
    
        if let detailInfo  = info.details {
            lblCategoryTitle.text = detailInfo.name != "" ? detailInfo.name.validate : "N/A"
            
            lblDuration.text = detailInfo.durationInFormate != "" ? detailInfo.durationInFormate.validate + " hr" : "0"
        }
        
        self.reportClick = reportClickTapped
        self.analysisClick = anylysisClickTapped
    }
    
    
    @IBAction func btnReportClickAction(_ sender: Any) {
        self.reportClick?()
    }
    
    
    @IBAction func btnAnylysisAction(_ sender: Any) {
        self.analysisClick?()
    }

}
