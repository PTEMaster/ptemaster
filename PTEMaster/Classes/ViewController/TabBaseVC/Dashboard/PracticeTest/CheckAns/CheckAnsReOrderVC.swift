//
//  CheckAnsReOrderVC.swift
//  PTEMaster
//
//  Created by mac on 25/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class CheckAnsReOrderVC: UIViewController {

    @IBOutlet weak var tblAns: UITableView!
    @IBOutlet weak var consTblHeight: NSLayoutConstraint!

    var arrAns = [OptionsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func updateViewConstraints() {
        // tblAns.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            DispatchQueue.main.async {
                self.consTblHeight.constant = self.tblAns.contentSize.height
            }
        }
        super.updateViewConstraints()
    }
}

extension CheckAnsReOrderVC: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReOrderCheckAnsCell", for: indexPath) as! ReOrderCheckAnsCell
        cell.lblAns.text = arrAns[indexPath.row].option.validate
        return cell
    }
}
