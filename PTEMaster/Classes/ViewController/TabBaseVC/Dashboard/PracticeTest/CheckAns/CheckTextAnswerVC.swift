//
//  CheckTextAnswerVC.swift
//  PTEMaster
//
//  Created by mac on 11/03/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class CheckTextAnswerVC: UIViewController {
    
    //@IBOutlet weak var tvAnswer: UITextView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var tvAns: UITextView!
    @IBOutlet weak var consTextViewHeight: NSLayoutConstraint!
    //@IBOutlet weak var consYourAnsHeight: NSLayoutConstraint!
    
    var ansType = ""
    var strAns = ""
    var strYourAns = ""
    var dismissBlock: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
       // setUpViewLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTextView()
    }
    
   /* func setUpViewLayout() {
        
        tvAns.text = strAns.removeHtmlFromString()
        let maxHeight = (ScreenSize.height * 0.5) - 150
        DispatchQueue.main.async {
            let size = self.tvAnswer.sizeThatFits(CGSize(width: self.tvAnswer.frame.width, height: CGFloat.greatestFiniteMagnitude))
            self.consTextViewHeight.constant = size.height > maxHeight ? maxHeight : size.height
            self.tvAnswer.isScrollEnabled = size.height > maxHeight
            self.tvAnswer.showsVerticalScrollIndicator = size.height > maxHeight
        }
        
        tvAns.text = strYourAns.removeHtmlFromString()
        DispatchQueue.main.async {
            let size = self.tvYourAns.sizeThatFits(CGSize(width: self.tvAns.frame.width, height: CGFloat.greatestFiniteMagnitude))
            self.consYourAnsHeight.constant = size.height > maxHeight ? maxHeight : size.height
            self.tvYourAns.isScrollEnabled = size.height > maxHeight
            self.tvYourAns.showsVerticalScrollIndicator = size.height > maxHeight
        }
    
        
    }*/
    
    func updateTextView() {
        let yourAns = self.strYourAns.removeHtmlFromString()
        let correctAns = self.strAns.removeHtmlFromString()
        let yourAnsText = "Your Answer"
        let correctAnsText = "Correct Answer"
        
        let displaytext = yourAnsText + "\n\n" + yourAns + "\n\n" + correctAnsText + "\n\n" + correctAns
        let yourAnsTextRange = (displaytext as NSString).range(of: yourAnsText)
        let correctAnsTextRange = (displaytext as NSString).range(of: correctAnsText)
        
        let attributedText = NSMutableAttributedString.init(string: displaytext, attributes: [.font: UIFont.glacialIndifferenceRegular(of: 16.0)])
        let boldFont = UIFont.glacialIndifferenceBold(of: 20.0)
        attributedText.addAttribute(NSAttributedString.Key.font, value: boldFont, range: yourAnsTextRange)
        attributedText.addAttribute(NSAttributedString.Key.font, value: boldFont, range: correctAnsTextRange)
        
        tvAns.attributedText = attributedText
        let maxHeight = ScreenSize.height - Helper.safeAreaTopMargine - 150
        
        DispatchQueue.main.async {
            let size = self.tvAns.sizeThatFits(CGSize(width: self.tvAns.frame.width, height: CGFloat.greatestFiniteMagnitude))
            self.consTextViewHeight.constant = size.height > maxHeight ? maxHeight : size.height
            self.tvAns.isScrollEnabled = size.height > maxHeight
            self.tvAns.showsVerticalScrollIndicator = size.height > maxHeight
        }
    }
    
    static func open(in viewController: UIViewController, answer: String, userAnswer: String, dismissBlock: @escaping () -> Void) {
        let vc = CheckTextAnswerVC.instance() as! CheckTextAnswerVC
        vc.strAns = answer
        vc.strYourAns = userAnswer
        vc.dismissBlock = dismissBlock
        vc.modalPresentationStyle = .overCurrentContext
        viewController.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btnCancelAction(_ sender: Any) {
        dismissBlock?()
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
