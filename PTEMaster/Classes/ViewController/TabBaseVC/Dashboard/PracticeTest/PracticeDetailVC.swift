//
//  PracticeDetailVC.swift
//  PTEMaster
//
//  Created by mac on 16/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit


protocol PracticeDetailsDelegate: NSObject {
    func resultStringData(transcript:String)
}



class PracticeDetailVC: UIViewController {
   
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblSubCategoryName: UILabel!
    @IBOutlet weak var lbTimerCounter: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnNumberCount: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewHeaderTime: NSLayoutConstraint!
    @IBOutlet weak var collectionviewConstraint: NSLayoutConstraint!
    @IBOutlet weak var collecionViewResult: UICollectionView!
    
    @IBOutlet weak var btnCheckAnswer: UIButton!
    @IBOutlet weak var lblCreditToken: UILabel!
    
    weak var delegate:PracticeDetailsDelegate?
    let audioPlayerManager = AudioPlayerManager()
    var practiceTestModel: PracticeTestModel!
    var arrOptionModel = [OptionsModel]()
    var currentController: UIViewController?
    var practiceQestionSelect:PracticeQestionModel?
    
    var timeLeft : TimeInterval = 160
    var totalTime : TimeInterval = 0
    var countDownTimer: Timer?
    
    var categoryId = ""
    var currentRow : Int = 0
    var updateTimer = true
    var pauseTimerCountDown = false
    var questionType:Int = 0
    var titles:String?
    var arrResult = [[String:Any]]()
    /// Back ground task identifier for timer
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var timerBg:Timer?
   
    override func viewDidDisappear(_ animated: Bool) {
         stopTimer()
    }
    func stopTimer(){
        self.countDownTimer?.invalidate()
                      self.countDownTimer = nil
    }
    
    
    override func viewDidLoad() {
      //  UIFont.availableFonts()
        super.viewDidLoad()
        collectionviewConstraint.constant = 0
        viewHeaderTime.constant = 0
       
        getTestApi(questionId: self.practiceQestionSelect!.id!)
       
    }
     //hp this function call from Summarize written  retry function
    func callRetryAgain(){
        collectionviewConstraint.constant = 0
         viewHeaderTime.constant = 0
        print(currentRow)
        print(self.practiceTestModel.mockqestion[currentRow].id!)
        self.getTestApi(questionId: self.practiceTestModel.mockqestion[currentRow].id!)
        // getTestApi(questionId: self.practiceQestionSelect!.id!)
    }
    
    
    //MARK:- set data on view on api calling of getTestApi
    func setupScreen(dict:PracticeQestionModel) { //
        prepareScreenAccordingToTest()
        lblCategoryName.text = staticCategory
        lblSubCategoryName.text = titles! + " " + "\(String(describing: dict.id ?? "" ))"
        startTimer()
        setNextPreviousTitle()
        
        //comment by hp
       /* let totalTimeRemaining = dict.totalTimeRemaining ?? 0.0
        if shouldShowHeader() {
            viewHeaderTime.constant = totalTimeRemaining == 0 ? 0.0 : 50.0
        }*/
    }
    

    
    //MARK:- button action
    @IBAction func btnBackAction(_ sender: Any) {
        let vc = PTEAlertViewController.instance() as! PTEAlertViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnCheckAnsAction(_ sender: Any) {
      //  collectionviewConstraint.constant = 120
        checkAnsStatus()
    }
    
    
    @IBAction func btnPreviousAction(_ sender: Any) {
      //   appDelegate.showHUD(appDelegate.strLoader, onView: view)
       // print("currentRow-\(currentRow)-")
        if currentRow == 0 {
            btnPrevious.isHidden = true
        }else {
            btnPrevious.isHidden = false
            currentRow -= 1
            self.getTestApi(questionId: self.practiceTestModel.mockqestion[currentRow].id!)
        }
        collectionviewConstraint.constant = 0
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
    // appDelegate.showHUD(appDelegate.strLoader, onView: view)
      //  print("currenttRow-\(currentRow)-")
        if currentRow == self.practiceTestModel.allcount + 1 {
            btnNext.isHidden = true
        }else {
            btnNext.isHidden = false
            if currentRow + 1 <= self.practiceTestModel.allcount {
                currentRow += 1
                
                self.getTestApi(questionId: self.practiceTestModel.mockqestion[currentRow].id!)
            }
        }
        collectionviewConstraint.constant = 0
    }
    
    @IBAction func btnNumberTitleAction(_ sender: Any) {
       //  print("currenttRow89-\(currentRow)-")
        let vc = PracticeCountVC.instance() as! PracticeCountVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.selectedIndex = self.currentRow
        vc.arrQuestionCount = (1...self.practiceTestModel.allcount + 1 ).map({ $0 })
        
        vc.chooseQuestionNumber = { selectedIndex in
            self.currentRow = selectedIndex
            self.getTestApi(questionId: self.practiceTestModel.mockqestion[selectedIndex].id!)
            self.collectionviewConstraint.constant = 0
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- check Ans Update View call on check answer button
    func checkAnsStatus() {
        
        if let mockTestModel = self.practiceTestModel.selectMockqestion.first?.categoryType {
            
            // speaking .....
            if mockTestModel == CategoryType.readAloud {
                
                print(" ans type >>>>> \(String(describing: self.practiceTestModel?.mockqestion.first?.answerType.validate))")
                
                if self.practiceTestModel?.mockqestion.first?.answerType == "audio" {
                    updateScreenTimer()
                }else if self.practiceTestModel.selectMockqestion.first?.answerType  == "audio" {
                    
                    if let vc = currentController as? ReadAloudVC {
                        vc.checkAns()
                    }
                }
                
                
               
                
            }else if mockTestModel == CategoryType.describeImg {
                
                if let vc = currentController as? DescribeImageVC {
                                //   stopTimer()
                            vc.checkAns()
                    }
              /*  if self.practiceTestModel.selectMockqestion.first?.answerType == "audio" {
                    let vc = CheckAnswerVC.instance() as! CheckAnswerVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.strAns = self.practiceTestModel?.mockqestion.first?.answer ?? ""
                    print("ans >>> \(vc.strAns)")
                    
                    self.present(vc, animated: true, completion: nil)
                }*/
                
            }else if mockTestModel == CategoryType.repeatSentence {
                //text
                //print(" ans type >>>>> \(self.practiceTestModel?.mockqestion.first?.answerType.validate)")
                
                if let vc = currentController as? ReapeatSentenceVC {
                    vc.checkAnswer()
                }
                //comment by hp
              /*  if self.practiceTestModel.selectMockqestion.first?.answerType  == "text" {
                    let vc = CheckTextAnswerVC.instance() as! CheckTextAnswerVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.strAns = self.practiceTestModel?.mockqestion.first?.answer ?? ""
                    print("ans >>> \(vc.strAns)")
                    self.present(vc, animated: true, completion: nil)
                }*/
                
            }else if mockTestModel == CategoryType.reTellLecture {
                if let vc = currentController as? RetellAnswereVC {
                    vc.checkAnswer()
                }
               /* if self.practiceTestModel.selectMockqestion.first?.answerType  == "text" {
                    let vc = CheckTextAnswerVC.instance() as! CheckTextAnswerVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.strAns = self.practiceTestModel?.mockqestion.first?.answer ?? ""
                    print("ans >>> \(vc.strAns)")
                    self.present(vc, animated: true, completion: nil)
                }*/
                
            }else if mockTestModel == CategoryType.ansShortQuestn {
                
                if let vc = currentController as? ShortAnswereVC {
                               //    stopTimer()
                                   vc.checkAnswer()
                               }
                /*if self.practiceTestModel.selectMockqestion.first?.answerType  == "text" {
                    let vc = CheckTextAnswerVC.instance() as! CheckTextAnswerVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.strAns = self.practiceTestModel?.mockqestion.first?.answer ?? ""
                    print("ans >>> \(vc.strAns)")
                    
                    self.present(vc, animated: true, completion: nil)
                }*/
            }
                
                // writing ...
            else if mockTestModel == CategoryType.summarizeWrittenText || mockTestModel == CategoryType.writingEasy {
                
                // shows popup
                /* let vc = CheckAnswerVC.instance() as! CheckAnswerVC
                 vc.modalPresentationStyle = .overCurrentContext
                 vc.ansType = (self.practiceTestModel?.mockqestion.first?.answerType.validate)!
                 let ans = self.practiceTestModel?.mockqestion.first?.answer ?? ""
                 vc.strAns = ans.count == 0 ? "answer not validate " : ans
                 self.present(vc, animated: true, completion: nil)*/
                
                if self.practiceTestModel.selectMockqestion.first?.answerType  == "text" {
                    
                    if let vc = currentController as? WritingEasyVC {
                        vc.checkAns()
                    }else if let vc = currentController as? SummarizeWrittenTextVC {
                        vc.checkAns()
                    }
                }
            }
                
                // reading ....
            else if mockTestModel == CategoryType.singleChoice {
                if let vc = currentController as? SingleChoiceVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.multipleAnswere {
                if let vc = currentController as? MultipleChoiceVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.reOrderParagraph {
                let vc = CheckAnsReOrderVC.instance() as! CheckAnsReOrderVC
                vc.modalPresentationStyle = .overCurrentContext
                //vc.ansType = "reorder"
                vc.arrAns = self.arrOptionModel
                self.present(vc, animated: true, completion: nil)
            }else if mockTestModel == CategoryType.readFillInBlanks {
                if let vc = currentController as? FillInTheBlanksVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.readWriteBlanks {
                if let vc = currentController as? ReadWriteBlanksVC {
                    vc.checkAns()
                }
            }
                
                // listening
            else if mockTestModel == CategoryType.listMultipleChoiceMultipleAns {
                if let vc = currentController as? ListingMultipleChoiceVC {
                    vc.checkAns()
                }
            } else if mockTestModel == CategoryType.listHighlightCorrectSummery {
                if let vc = currentController as? HighlightCorrectSummeryVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.listMultipleChoiceSingleAns {
                if let vc = currentController as? ListningSingleChoiceVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.listSelectMissingWord {
                if let vc = currentController as? SelectMissingWordVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.listFillInTheBlanks {
                if let vc = currentController as? ListningFillInBlanksVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.listHighlightInCorrectWord {
                if let vc = currentController as? HighlightIncorrectWordsVC {
                    vc.checkAns()
                }
            }else if mockTestModel == CategoryType.listWriteFromDictation {
                if let vc = currentController as? WriteDictationVC {
                    vc.checkAns()
                }
                
                /*  let vc = CheckAnswerVC.instance() as! CheckAnswerVC
                 vc.modalPresentationStyle = .overCurrentContext
                 vc.ansType = "text"
                 let ans = self.practiceTestModel?.mockqestion.first?.answer ?? ""
                 vc.strAns = ans.count == 0 ? "work in progress from back end" : ans
                 self.present(vc, animated: true, completion: nil)*/
                
//                if self.practiceTestModel.selectMockqestion.first?.answerType  == "text" {
//
//                    let vc = CheckTextAnswerVC.instance() as! CheckTextAnswerVC
//                    vc.modalPresentationStyle = .overCurrentContext
//                    let ans = self.practiceTestModel?.mockqestion.first?.answer ?? ""
//                    vc.strAns = ans.count == 0 ? "work in progress from back end" : ans
//                    self.present(vc, animated: true, completion: nil)
//
//                }
                
            }else if mockTestModel == CategoryType.listSummarySpokenText {
                 if let vc = currentController as? SummerySpokenVC {
                                   vc.checkAns()
                               }
            }
        }
    }
    
}



// MARK:- Timer
extension PracticeDetailVC {
    
    func startTimer() {
        timeLeft = 0
        invalidateTimer()
        updateTimerLabelText()
        countDownTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
           // print("!self.pauseTimerCountDown.....\(!self.pauseTimerCountDown)")
            if !self.pauseTimerCountDown {
              //  print("timer work from PracticeDetailVC-\(self.timeLeft)-")
                self.timeLeft -= 1
                self.updateTimerLabelText()
                
                if self.timeLeft <= 0 {

                    timer.invalidate()
                }
            }
        })
    }
    
    
    func invalidateTimer() {
        self.countDownTimer?.invalidate()
        self.countDownTimer = nil
        
        if let mockTestModel = self.practiceTestModel.selectMockqestion.first?.categoryType {
           print("CategoryType---\(mockTestModel)")
          
            if mockTestModel == CategoryType.summarizeWrittenText || mockTestModel == CategoryType.writingEasy || mockTestModel == CategoryType.listSummarySpokenText {
                
                self.timeLeft = (self.practiceTestModel.selectMockqestion.first?.responseTime) ?? 0.0
                print("timeLeft---\(self.timeLeft)")
                //MARK:-implemented timer by #HIMANSHU PAL
                if mockTestModel == CategoryType.summarizeWrittenText || mockTestModel == CategoryType.listSummarySpokenText {
                    self.totalTime = TimeInterval(self.practiceTestModel.difftokentime)
                    
                    
                    if appDelegate.toalCredits == 0 && appDelegate.toalCreditTime == 0 {
                        viewHeaderTime.constant = 0
                        stopTimerBg()
                    }else{
                        startTimerBg()
                        }
            }
                
            }else if mockTestModel == CategoryType.listSummarySpokenText ||
                mockTestModel == CategoryType.listFillInTheBlanks ||
                mockTestModel == CategoryType.listHighlightCorrectSummery  ||
                mockTestModel == CategoryType.listMultipleChoiceSingleAns ||
                mockTestModel == CategoryType.listSelectMissingWord ||
                mockTestModel == CategoryType.listHighlightInCorrectWord ||
                mockTestModel == CategoryType.listMultipleChoiceMultipleAns ||
                mockTestModel == CategoryType.listWriteFromDictation {
               
               /* self.totalTime = ((self.practiceTestModel.selectMockqestion.first?.totalTimeRemaining) == 0.0 ?  (self.practiceTestModel.selectMockqestion.first?.waitTime) : (self.practiceTestModel.selectMockqestion.first?.totalTimeRemaining))!
                self.timeLeft = ((self.practiceTestModel.selectMockqestion.first?.totalTimeRemaining) == 0.0 ?  (self.practiceTestModel.selectMockqestion.first?.waitTime) : (self.practiceTestModel.selectMockqestion.first?.totalTimeRemaining))!*/
                
                let waitTime = (self.practiceTestModel.selectMockqestion.first?.waitTime) ?? 0.0
                
                let responseTime = (self.practiceTestModel.selectMockqestion.first?.responseTime) ?? 0.0
                               self.timeLeft = waitTime  + responseTime + 4
                
                stopTimer()
                stopTimerBg()
                
            }else  if mockTestModel == CategoryType.readAloud  || mockTestModel == CategoryType.repeatSentence  || mockTestModel == CategoryType.reTellLecture  || mockTestModel == CategoryType.describeImg  || mockTestModel == CategoryType.ansShortQuestn   {  //
                // ADD this time by hp
                 let waitTime = (self.practiceTestModel.selectMockqestion.first?.waitTime) ?? 0.0
                let responseTime = (self.practiceTestModel.selectMockqestion.first?.responseTime) ?? 0.0
                self.timeLeft = waitTime  + responseTime + 4
       
            //MARK:-implemented timer by #HIMANSHU PAL
                self.totalTime = TimeInterval(self.practiceTestModel.difftokentime)
                if appDelegate.toalCredits == 0 && appDelegate.toalCreditTime == 0 {
                    stopTimerBg()
                    viewHeaderTime.constant = 0
                }else{
                    startTimerBg()
                    }
            }
          
        }else {
         //   self.totalTime = (self.practiceTestModel.selectMockqestion.first?.totalTimeRemaining) ?? 0.0
            self.timeLeft = (self.practiceTestModel.selectMockqestion.first?.totalTimeRemaining) ?? 0.0
           
        }
    }
    func updateTimeLeft() {
           /* let arr = (self.practiceTestModel.toalCreditTime ?? "00:00:00").components(separatedBy: ":")
          
            if arr.count > 2 {
                let hours = Int(arr[0].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
                let mins = Int(arr[1].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
                let sec = Int(arr[2].trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
             let   timeLeftSecond = Double(hours * 60 * 60) + Double(mins * 60) + Double(sec)
                print(timeLeftSecond)
                self.totalTime = TimeInterval(timeLeftSecond)
                
                if self.totalTime == 0{
                    stopTimerBg()
                }else{
                    startTimerBg()
                    }
            }*/
        }
    func updateTimerLabelText() {
       
        updateScreenTimer()
    }
    //MARK: - hp timer
    func startTimerBg() {
        if timerBg == nil
        {
            //To timer in background 10*60
            backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
            })
            timerBg = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(self.updateTimerBg), userInfo: nil, repeats: true)
        }
    }
    func stopTimerBg() {
        timerBg?.invalidate()
        timerBg = nil
    }
  
    @objc func updateTimerBg() {
     //   print("totalTime--\(totalTime)")
        totalTime -= 1
        
        let watch = StopWatch(totalSeconds: Int(totalTime))
        if self.totalTime > 0 {
           // print("self.totalTime---\(self.totalTime)")
          //  let minutes = self.totalTime / 86400
         //   let seconds = self.totalTime.truncatingRemainder(dividingBy: 60)
         //   let text = String(format: "%02i:%02i", Int(minutes), Int(seconds))
            
            if watch.days > 1{
                lbTimerCounter.text = "\(watch.days)" + " Days " + "\(watch.hours)" + ":" + "\(watch.minutes)" + ":" + "\(watch.seconds)"
            }else{
                lbTimerCounter.text = "\(watch.days)" + " Day " + "\(watch.hours)" + ":" + "\(watch.minutes)" + ":" + "\(watch.seconds)"
                }
           
        } else {
            lbTimerCounter.text = "00:00"
        }
       
        
        if totalTime <= 0 {

            stopTimerBg()
        }
    }
    
    func setNextPreviousTitle() {
        print("curentRow ---=-\(currentRow)")
        if currentRow == 0 {
            btnPrevious.isHidden = true
        } else {
            btnPrevious.isHidden = false
        }
        
        if currentRow == self.practiceTestModel.allcount  { // + 1
            btnNext.isHidden = true
        } else {
            btnNext.isHidden = false
        }
        let strStartNumber = self.practiceTestModel.selectMockqestion.first?.questionNumber
        let finalStr =  "\(currentRow  + 1)"  + "/" + String(self.practiceTestModel.allcount + 1)
        btnNumberCount.setTitle("\(finalStr)" , for: .normal)
        
    }
    
    func updateScreenTimer() {
        
        // writing ...
        if let vc = self.currentController as? SummarizeWrittenTextVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? WritingEasyVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }
            
            // speaking ...
        else if let vc = self.currentController as? ReadAloudVC {
            vc.updateTimer()
        }else if let vc = self.currentController as? ReapeatSentenceVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? DescribeImageVC {
            vc.updateTimer() // correct
        }else if let vc = self.currentController as? ShortAnswereVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? RetellAnswereVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }
            
            // reading ....
        else if let vc = self.currentController as? SingleChoiceVC {
            self.stopTimer()
           // vc.updateTimer() //hp no need to run timer in practice detail section
        }else if let vc = self.currentController as? MultipleChoiceVC {
           // vc.updateTimer()
            self.stopTimer()
        }else if let vc = self.currentController as? ReOrderVC {
           // vc.updateTimer()
             self.stopTimer()
        }else if let vc = self.currentController as? ReadWriteBlanksVC {
           // vc.updateTimer()
            self.stopTimer()
        }else if let vc = self.currentController as? FillInTheBlanksVC {
            //vc.updateTimer()
            self.stopTimer()
        }
            
            // listening ....
        else if let vc = self.currentController as? SummerySpokenVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? ListingMultipleChoiceVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? HighlightCorrectSummeryVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? ListningSingleChoiceVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? SelectMissingWordVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? WriteDictationVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? HighlightIncorrectWordsVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }else if let vc = self.currentController as? ListningFillInBlanksVC {
            vc.updateTimer(text: "\(Int(self.timeLeft))")
        }
        
    }
}



//MARK:- Child Controllers
extension PracticeDetailVC {
    
    func shouldShowHeader() -> Bool {
        //  if let mockTestModel = practiceTestModel.mockqestion[currentRow] {
        switch self.practiceTestModel.selectMockqestion.first?.categoryType! {
        // speaking .....
        case .readAloud, .describeImg, .repeatSentence, .reTellLecture, .ansShortQuestn:
            return true
        // writing ...
        case .summarizeWrittenText, .writingEasy:
            return true
        // reading ....
        case .singleChoice, .multipleAnswere, .reOrderParagraph, .readFillInBlanks, .readWriteBlanks:
            return false
        // listening....
        case .listSummarySpokenText:
            return false
        case .listMultipleChoiceMultipleAns, .listWriteFromDictation:
            return self.totalTime > 0
        case .listFillInTheBlanks, .listHighlightCorrectSummery, .listMultipleChoiceSingleAns, .listSelectMissingWord, .listHighlightInCorrectWord:
            return true
        case .none:
            break
        }
        // }
        return true
    }
    

    
    func prepareScreenAccordingToTest() {
        if let mockTestModel = self.practiceTestModel.selectMockqestion.first?.categoryType {
            viewHeaderTime.constant = 0
            // speaking .....
            if mockTestModel == CategoryType.readAloud {
               
                let vc = ReadAloudVC.instance(.question) as! ReadAloudVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
               // vc.delegate = self
                viewHeaderTime.constant = 80
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.describeImg {
                let vc = DescribeImageVC.instance(.question) as! DescribeImageVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                viewHeaderTime.constant = 80
                updateContentViewChildController(vc)
            }
                
            else if mockTestModel == CategoryType.repeatSentence {
                let vc = ReapeatSentenceVC.instance(.question) as! ReapeatSentenceVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                viewHeaderTime.constant = 80
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.reTellLecture {
                let vc = RetellAnswereVC.instance(.question) as! RetellAnswereVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                viewHeaderTime.constant = 80
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.ansShortQuestn {
                let vc = ShortAnswereVC.instance(.question) as! ShortAnswereVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                viewHeaderTime.constant = 0
                updateContentViewChildController(vc)
            }
                
                // writing ...
            else if mockTestModel == CategoryType.summarizeWrittenText {
                 viewHeaderTime.constant = 80 // hide time header  // hp
                let vc = SummarizeWrittenTextVC.instance(.question) as! SummarizeWrittenTextVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.writingEasy {
               //  viewHeaderTime.constant = 0 // hide time header  // hp
                let vc = WritingEasyVC.instance(.question) as! WritingEasyVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                updateContentViewChildController(vc)
            }
                
                // reading ....
            else if mockTestModel == CategoryType.singleChoice {
                viewHeaderTime.constant = 80
                let vc = SingleChoiceVC.instance(.question) as! SingleChoiceVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.multipleAnswere {
                let vc = MultipleChoiceVC.instance(.question) as! MultipleChoiceVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.reOrderParagraph {
                let vc = ReOrderVC.instance(.question) as! ReOrderVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.readFillInBlanks {
                let vc = FillInTheBlanksVC.instance(.question) as! FillInTheBlanksVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.readWriteBlanks {
                let vc = ReadWriteBlanksVC.instance(.question) as! ReadWriteBlanksVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }
                
                // listening
            else if mockTestModel == CategoryType.listSummarySpokenText {
                let vc = SummerySpokenVC.instance(.question) as! SummerySpokenVC
                viewHeaderTime.constant = 80
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.listMultipleChoiceMultipleAns {
                viewHeaderTime.constant = self.totalTime > 0 ? 80 : 0
                let vc = ListingMultipleChoiceVC.instance(.question) as! ListingMultipleChoiceVC
                vc.practiceDetailVC = self
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.listFillInTheBlanks {
                let vc = ListningFillInBlanksVC.instance(.question) as! ListningFillInBlanksVC
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            } else if mockTestModel == CategoryType.listHighlightCorrectSummery {
                let vc = HighlightCorrectSummeryVC.instance(.question) as! HighlightCorrectSummeryVC
                vc.practiceDetailVC = self
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.listMultipleChoiceSingleAns {
                let vc = ListningSingleChoiceVC.instance(.question) as! ListningSingleChoiceVC
                vc.practiceDetailVC = self
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.listSelectMissingWord {
                let vc = SelectMissingWordVC.instance(.question) as! SelectMissingWordVC
                vc.practiceDetailVC = self
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.listHighlightInCorrectWord {
                let vc = HighlightIncorrectWordsVC.instance(.question) as! HighlightIncorrectWordsVC
                vc.practiceDetailVC = self
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }else if mockTestModel == CategoryType.listWriteFromDictation {
                viewHeaderTime.constant =   0 //self.totalTime > 0 ? 80 : 0
                let vc = WriteDictationVC.instance(.question) as! WriteDictationVC
              //  vc.practiceDetailVC = self
                vc.practiceTestModel = self.practiceTestModel.selectMockqestion.first
                vc.arrOptionModel = self.arrOptionModel
                updateContentViewChildController(vc)
            }
        }
    }
    
    //MARK:- update view controller add view controller on partent  view on view
    func updateContentViewChildController(_ vc: UIViewController) {
        DispatchQueue.main.async {
            if let preVC = self.currentController {
                preVC.willMove(toParent: nil)
                preVC.view.removeFromSuperview()
                preVC.removeFromParent()
                self.currentController = nil
            }
            self.currentController = vc
            vc.willMove(toParent: self)
            vc.view.frame = self.viewContainer.bounds
            vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin];
            self.viewContainer.addSubview(vc.view)
            self.addChild(vc)
            vc.didMove(toParent: self)
        }
    }
    
}


 // MARK:- Call API for answer and get test api
extension PracticeDetailVC {
    
    func getTestApi(questionId:String) {
        if appDelegate.isNetworkAvailable {
            // postParams["frecuency"] = questionType
            var postParams = [String: Any]()
            postParams["userid"] = AppDataManager.shared.loginData.id
            postParams["cat_id"] = categoryId
            postParams["qtype"] = "0"
            postParams["qquality"] = "0"
            postParams["myprac"] = "0"
            postParams["labelselect"] = "0"
            postParams["frecuency"] = questionType
            postParams["startfrom"] = currentRow
            postParams["qustionId"] = questionId
            postParams["qtyp"] = "0"
            appDelegate.showHUD()
            
              print("Api:-\(BaseUrl + RequestPath.PracticeQuestionNew.rawValue)")
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.PracticeQuestionNew.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            DispatchQueue.main.async {
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                
                               print("swiftyJsonResponseDict >>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                    
                                    SpeakingConstant.pv = dashboradDataDict["pv"]?.string ?? ""
                                    SpeakingConstant.alt_pv = dashboradDataDict[""]?.string ?? ""
                                    if let fetchedImages = dashboradDataDict["alt_pv_json"]?.arrayObject as? [String] {
                                        SpeakingConstant.alt_pv_json = fetchedImages
                                    }
                                 
                                    
                               print(">>> dashboradDataDict >>> \(dashboradDataDict)")
                                    self.practiceTestModel = PracticeTestModel(json: swiftyJsonResponseDict["data"])
                                    self.lblCreditToken.text = "\(self.practiceTestModel.toalCredits ?? 0)"
                                    
                                    appDelegate.toalCredits = self.practiceTestModel.toalCredits
                                    appDelegate.toalCreditTime = self.practiceTestModel.toalCreditTime
                                    
                                    self.arrOptionModel.removeAll()
                                    
                                    if let optionList = dashboradDataDict["option"]?.array, optionList.count > 0 {
                                        for dict in optionList {
                                            let optionModel = OptionsModel(json: dict)
                                            self.arrOptionModel.append(optionModel)
                                        }
                                     //  print(self.arrOptionModel.count)
                                    }
                                  self.setupScreen(dict: self.practiceTestModel.selectMockqestion.first!)
                                   // print(self.practiceTestModel.allcount)
                                    appDelegate.hideHUD()
                                }
                                else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
       
    }
    
    func deduct_creditUser(){
        if appDelegate.isNetworkAvailable {
            var postParams = [String: Any]()
            postParams["user_id"] = AppDataManager.shared.loginData.id
            postParams["category"] = categoryId
            appDelegate.showHUD()
              print("Api:-http://3.25.96.167/api/deduct_creditUser")
            print("Post Parameter is:\(postParams)")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: "http://3.25.96.167/api/deduct_creditUser", method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            DispatchQueue.main.async {
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                             print("done")
                                let total  = self.practiceTestModel.toalCredits! - 1
                                print(total)
                                self.practiceTestModel.toalCredits! = total
                                self.lblCreditToken.text = "\(total)"
                                appDelegate.hideHUD()
                            }
                            }
                        }
                    }
                    appDelegate.hideHUD()
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
       
    }

    
    func deduct_creditUser_withoutAPI(){
        let total  = self.practiceTestModel.toalCredits! - 1
        print(total)
        self.practiceTestModel.toalCredits! = total
        self.lblCreditToken.text = "\(total)"
    }
  
}

extension PracticeDetailVC: AlertDelegate {
    func dismissViewController(_ viewController: PTEAlertViewController) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PracticeDetailVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
        
         cell.viewbg.layer.borderWidth = 2
        let dict  = arrResult[indexPath.row]
        let strResult = dict["finalResult"] as? Double ?? 0.0
        let strFinalResult = String(format: "%.1f", strResult)
        let fullArr = strFinalResult.components(separatedBy: ".")
        let first =  fullArr[0]
        let last =  fullArr[1]
      /*  if last != "0"{
        cell.lblResult.text = strFinalResult + "/" + "90"
        }else{
            cell.lblResult.text = first + "/" + "90"
        }*/
        
        cell.lblResult.text = first + "/" + "90"
        let colorName = dict["color"] as? String
        cell.viewbg.layer.borderColor = UIColor(named: colorName ?? "darkLightColor")?.cgColor
        if indexPath.row == 0 {
            cell.lblTitle.text = "Fluency"
            cell.viewbg.layer.borderColor = UIColor.blue.cgColor
        } else if indexPath.row == 1 {
            cell.lblTitle.text = "Pronounciation"
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
        } else if indexPath.row == 2 {
            cell.lblTitle.text = "Content"
            cell.viewbg.layer.borderColor = UIColor.brown.cgColor
        } else if indexPath.row == 3 {
            cell.lblTitle.text = "Final Result"
            cell.viewbg.layer.borderColor = UIColor.blue.cgColor
        }
        return cell
    }
    
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 106, height: 106)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    
    
}

class ResultCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var lblResult: UILabel!
    
    override func awakeFromNib() {
         super.awakeFromNib()
          viewbg.layer.cornerRadius = viewbg.frame.size.width/2

        
        
        viewbg.dropShadowww(Float(viewbg.frame.size.width/2))
      }
    
}



extension UIView {

    func dropShadowww(_ Width : Float) {

        var shadowLayer: CAShapeLayer!
        let cornerRadius: CGFloat = CGFloat(Width)
        let fillColor: UIColor = .white

        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()

            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor

            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: -2.0, height: 2.0)
            shadowLayer.shadowOpacity = 0.8
            shadowLayer.shadowRadius = 2

            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
}




struct StopWatch {

    var totalSeconds: Int

    var years: Int {
        return totalSeconds / 31536000
    }

    var days: Int {
        return (totalSeconds % 31536000) / 86400
    }

    var hours: Int {
        return (totalSeconds % 86400) / 3600
    }

    var minutes: Int {
        return (totalSeconds % 3600) / 60
    }

    var seconds: Int {
        return totalSeconds % 60
    }

    //simplified to what OP wanted
    var hoursMinutesAndSeconds: (hours: Int, minutes: Int, seconds: Int) {
        return (hours, minutes, seconds)
    }
}
