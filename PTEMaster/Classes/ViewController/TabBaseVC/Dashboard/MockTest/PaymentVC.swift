//
//  PaymentVC.swift
//  PTEMaster
//
//  Created by mac on 23/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

import WebKit

protocol PaymentSuccessDelegate : NSObjectProtocol {
    
}


class PaymentVC: UIViewController {
    
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    var webView: WKWebView!
    weak var delegate: PaymentSuccessDelegate?
    var strFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.initialSetup()
        }
    }
    

    //MARK:- webview method
    func initialSetup() {
        URLCache.shared.removeAllCachedResponses()
        var urlString = ""
        urlString = "\(strFrom)"

        if urlString.count > 0, let url = URL(string: urlString) {
            webView = WKWebView(frame: displayView.bounds)
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.isUserInteractionEnabled = true
            webView.navigationDelegate = self
            self.displayView.addSubview(self.webView)
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PaymentVC: WKUIDelegate , WKNavigationDelegate, UIWebViewDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let currentURL = webView.url {
            
            let absoluteString = currentURL.absoluteString
            
            if absoluteString.contains("cancel") || !absoluteString.contains("<html>") {
                print("I found: \(absoluteString)")
                webView.isHidden = true
                
                self.navigationController?.popNumberOfViewControllers(3)

                
            } else if absoluteString.contains("payment_success") {
                print("I found: \(absoluteString)")
                
                print(absoluteString)
                
                /*  DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                 
                 if self.detailInfo != nil {
                 self.delegate?.successPaymentWithCategory(self.detailInfo!)
                 } else if self.subcriptionListModel != nil {
                 self.delegate?.successPaymentWithSubcription(self.subcriptionListModel!)
                 }
                 
                 self.navigationController?.popNumberOfViewControllers(3)
                 }*/
                
                self.navigationController?.popNumberOfViewControllers(3)

                
            }else if absoluteString.contains("fail")  {
                
                
                print(absoluteString)
                
                self.navigationController?.popNumberOfViewControllers(3)

            }
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    
    }
    
}


public extension UINavigationController {
    func popNumberOfViewControllers(_ nb: Int) {
        let viewControllers: [UIViewController] = self.viewControllers
        guard viewControllers.count < nb else {
            self.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
            return
        }
    }
}
