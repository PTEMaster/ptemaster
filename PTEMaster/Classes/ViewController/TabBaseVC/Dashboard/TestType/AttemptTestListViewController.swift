//
//  AttemptTestListViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 15/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class AttemptTestListViewController: UIViewController {
    
   
    @IBOutlet weak var tableView_testList: UITableView!
    var arrAttemptTest = [HistoryListModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        attemptTestApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func attemptTestApi() {
        if appDelegate.isNetworkAvailable {
           // arrHistoryTest.removeAll()
            appDelegate.showHUD()
           
            
            let param : [String: Any] = ["user_id" : "\(AppDataManager.shared.loginData.id!)"]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.attemptTest.rawValue , method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                
                                print("swiftyJsonResponseDict >>>>>> \(swiftyJsonResponseDict)")
                                
                                if swiftyJsonResponseDict["status"].intValue == 1, let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                    
                                    print("dashboradDataDict  >>>>>> \(dashboradDataDict)")
                                    
                                    if let arrTest = dashboradDataDict["test"]?.array {
                                        
                                        print(arrTest.count)
                                        
                                        
                                        if arrTest.count == 0 {
                                            self.tableView_testList.displayBackgroundText(text: "No Completed Test Yet!")
                                        }else {
                                            self.tableView_testList.backgroundView = nil
                                            for dict in arrTest {
                                                let historyListModel = HistoryListModel(json: dict)
                                                
                                                //if historyListModel.completedStatut == "1" {
                                                    self.arrAttemptTest.append(historyListModel)
                                                //}
                                            }
                                            self.tableView_testList.reloadDataInMain()
                                        }
                                        
                                    }
                                    
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    

    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension AttemptTestListViewController : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAttemptTest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView_testList.dequeueReusableCell(withIdentifier: "AttemptListCell") as! AttemptListTableCell
        
        cell.setData(info: arrAttemptTest[indexPath.row], continueClickTapped: {
            
            print(">>> index path \(indexPath.row)")
            Util.showAlertWithCallback("Confirmation Alert !", message: "\(self.arrAttemptTest[indexPath.row].details?.name.validate ?? "not found")?", isWithCancel: true) {
                
                print(self.arrAttemptTest[indexPath.row])
                
                let mockDetailVC = MockTestDetailVC.instance() as! MockTestDetailVC
                mockDetailVC.testId = "\(self.arrAttemptTest[indexPath.row].testId!)"
        
                mockDetailVC.currentQuestionId = (self.arrAttemptTest[indexPath.row].details?.questionNumber.validate)!
                
                self.navigationController?.pushViewController(mockDetailVC, animated: true)
                
            }
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableView.automaticDimension
    }
}
