//
//  PTEMTokenViewController.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit

class PTEMTokenViewController: UIViewController {
    @IBOutlet weak var segmentToken: UISegmentedControl!
    @IBOutlet weak var tableToken: UITableView!
    var arrTokenPacks = [TokenModal]()
     var arrMyToken = [MyToken]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentToken.selectedSegmentIndex = 0
        
        self.isCallTokenPacksApiCall(apiActions: RequestPath.account_details.rawValue)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionSegmentTokens(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            
            self.isCallTokenPacksApiCall(apiActions: RequestPath.account_details.rawValue)
            tableToken.reloadData()
        } else if sender.selectedSegmentIndex == 1 {
            self.isCallTokenPacksApiCall(apiActions: RequestPath.token_packs.rawValue)
        }
}
    
    // MARK:-TokenPacks
    func isCallTokenPacksApiCall(apiActions:String) {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: Any] = [ "userid" : "\(AppDataManager.shared.loginData.id!)"]
            appDelegate.showHUD()
            print("Api-\(BaseUrl + apiActions)")
            print("Post Parameter is:\(postParams)")
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + apiActions, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    
                                    if let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                        print(">>>> \(String(describing: dashboradDataDict["user"]))")
                                        if apiActions == RequestPath.token_packs.rawValue {
                                            if let arrTestList = dashboradDataDict["pakage"]?.array {
                                                self.arrTokenPacks.removeAll()
                                                for dict in arrTestList {
                                                    let packs = TokenModal(parameters: dict)
                                                    self.arrTokenPacks.append(packs)
                                                  /*  let indexPath = IndexPath(item: self.arrTokenPacks.count - 1 , section: 0)
                                                    self.tableToken.insertRows(at: [indexPath], with: .automatic)*/
                                                }
                                                 self.tableToken.reloadData()
                                            }
                                        } else {
                                            if   dashboradDataDict["my_pakage"] == false{
                                              //  Util.showAlertWithCallback("", message: "No token available", isWithCancel: false)
                                                if self.arrMyToken.count == 0{
                                                            // self.tableView_MockList.displayBackgroundText(text: "", fontStyle: "helvetica", fontSize: 15)
                                                self.tableToken.displayBackgroundImageWithText(text:  "No token available" , fontStyle: "SansaGLACIALINDIFFERENCE-BOLD", fontSize: 15, imgName: "cloud")
                                                              self.tableToken.reloadData()
                                                          }
                                                return
                                                
                                            }else{
                                                if let arrTestList = dashboradDataDict["my_pakage"]?.array {
                                                    self.arrMyToken.removeAll()
                                                    for dict in arrTestList {
                                                        let packs = MyToken(parameters: dict)
                                                        self.arrMyToken.append(packs)
                                                       /* let indexPath = IndexPath(item: self.arrMyToken.count - 1 , section: 0)
                                                        self.tableToken.insertRows(at: [indexPath], with: .automatic)*/
                                                    }
                                                    
                                                    if self.arrMyToken.count > 0{
                                                                // self.tableView_MockList.displayBackgroundText(text: "", fontStyle: "helvetica", fontSize: 15)
                                                    self.tableToken.displayBackgroundImageWithText(text: "" , fontStyle: "SansaGLACIALINDIFFERENCE-BOLD", fontSize: 15, imgName: "")
                                                                  self.tableToken.reloadData()
                                                              }
                                                }
                                            }
                                            

                                        }
                                    }
                                } else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                   if self.arrMyToken.count == 0{
                                                // self.tableView_MockList.displayBackgroundText(text: "", fontStyle: "helvetica", fontSize: 15)
                                    self.tableToken.displayBackgroundImageWithText(text: swiftyJsonResponseDict ["msg"].string ?? "" , fontStyle: "SansaGLACIALINDIFFERENCE-BOLD", fontSize: 15, imgName: "cloud")
                                                  self.tableToken.reloadData()
                                              }
                                   // Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
}

extension PTEMTokenViewController: UITableViewDelegate , UITableViewDataSource , PTEMTokenDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return arrTokenPacks.count
        if segmentToken.selectedSegmentIndex == 0 {
            return arrMyToken.count
        } else if segmentToken.selectedSegmentIndex == 1 {
            return arrTokenPacks.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PTEMTokenTableViewCell", for: indexPath) as! PTEMTokenTableViewCell
        cell.delegate = self
        if segmentToken.selectedSegmentIndex == 0 {
            cell.btnBuyNow.isHidden = true
             cell.isSetData(dict: arrMyToken[indexPath.row])
        }else{
            cell.btnBuyNow.isHidden = false
            cell.isSetData(dict: arrTokenPacks[indexPath.row])
        }
       
        return cell
    }
    
    func actionBuyNow(cell: PTEMTokenTableViewCell) {
     /*   Util.showAlertWithCallback(AppConstant.appName, message: "Please visit masterpte.com.au to complete your purchase", isWithCancel: false)
        return
        */
   guard let indexPath = tableToken.indexPath(for: cell) else {
            return
        }
        
        let dict  = arrTokenPacks[indexPath.row]
        let vc = PTEMBuyTokensViewController.instance() as! PTEMBuyTokensViewController
        vc.tokenModal = dict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if segmentToken.selectedSegmentIndex == 1 {
        let dict  = arrTokenPacks[indexPath.row]
        let vc = PTEMBuyTokensViewController.instance() as! PTEMBuyTokensViewController
        vc.tokenModal = dict
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}


extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(
            NSAttributedString.Key.strikethroughStyle,
               value: NSUnderlineStyle.single.rawValue,
                   range:NSMakeRange(0,attributeString.length))
        return attributeString
    }
}




