//
//  PTEMBuyTokensViewController.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit
import  SafariServices

class PTEMBuyTokensViewController: UIViewController {
    
    @IBOutlet weak var lblTokenName: UILabel!
    @IBOutlet weak var lblTokenAmount: UILabel!
    @IBOutlet weak var lblCreditToken: UILabel!
    @IBOutlet weak var lblAmountDiscount: UILabel!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var lblDiscunt: UILabel!
    @IBOutlet weak var lblDiscountsPrice: UILabel!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var lblTotalAmount: UILabel!
    var tokenModal:TokenModal?
    var strToken = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isSetdata(dict: tokenModal!)
        // Do any additional setup after loading the view.
    }
    
    func isSetdata(dict:TokenModal) {
        let price = Double(dict.tk_pkg_price!)!
        let discount = Double(dict.tk_pkg_discount!)!
        let discountAmount =   discount / 100 * price
        let totalAmount = price - discountAmount
        let strAmount = String(format: "%.2f", totalAmount)
        lblTokenName.text = dict.tk_pkg_name
        lblTokenAmount.text = "$" + (dict.tk_pkg_price ?? "0") + "AUD"
        lblAmountDiscount.text = "$" + strAmount + "AUD"
        lblCredits.text = dict.tk_pkg_credits
        lblDiscunt.text = (dict.tk_pkg_discount ?? "0")  + "%"
        lblDiscountsPrice.text = "$" + strAmount + "AUD"
        lblTotalAmount.text =  "$" + strAmount  + "AUD"
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ActionsBuyToken(_ sender: Any) {
        self.isCallBuyTokenApiCall()
    }
    @IBAction func actionApplyToken(_ sender: Any) {
        self.view.endEditing(true)
            if txtPromoCode.text?.trimWhitespaces == ""{
                strToken = "0"
                 Util.showAlertWithCallback(AppConstant.appName, message: "Please enter promo code", isWithCancel: false)
                return
            }else{
                let str = txtPromoCode.text
                print("Original: \(str ?? "")")
                let utf8str = str?.data(using: .utf8)
                if let base64Encoded = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)) {
                    print("Encoded: \(base64Encoded)")
                  strToken = base64Encoded
                }
        }
        
    isVerifyTokenApiCall()
        
    }
    
    func isVerifyTokenApiCall() {
        if appDelegate.isNetworkAvailable {
            var postParams =  [String: Any]()

            postParams["user_id"] = AppDataManager.shared.loginData.id
            postParams["test_id"] = tokenModal!.tk_pkg_id!
            postParams["promo"] = txtPromoCode.text
            
            appDelegate.showHUD()
            print("Api-\(BaseUrl +  RequestPath.check_promoCode_token.rawValue)-")
            print("Post Parameter is:\(postParams)")
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl +  RequestPath.check_promoCode_token.rawValue, method: .post, headers: nil) { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                print(swiftyJsonResponseDict)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                   /* if let msg = swiftyJsonResponseDict["msg"].string {
                                        if let url = URL(string: msg) {
                                            UIApplication.shared.open(url)
                                        }
                                    }*/
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    self.strToken = "0"
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
    
    
    func isCallBuyTokenApiCall() {
        if appDelegate.isNetworkAvailable {
            var postParams =  [String: Any]()
            

            postParams["user_id"] = AppDataManager.shared.loginData.id
            postParams["token_id"] = tokenModal!.tk_pkg_id!
            postParams["code"] = strToken
            
            appDelegate.showHUD()
            print("Api-\(BaseUrl +  RequestPath.token_buynow.rawValue)-")
            print("Post Parameter is:\(postParams)")
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl +  RequestPath.token_buynow.rawValue, method: .post, headers: nil) { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                print(swiftyJsonResponseDict)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    if let msg = swiftyJsonResponseDict["msg"].string {
                                        let vc = SFSafariViewController(url: URL(string: msg)!)
                                        vc.delegate = self
                                        self.present(vc, animated: true, completion: nil)
                                        /*
                                        if let url = URL(string: msg) {
                                            UIApplication.shared.open(url)
                                        }*/
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
}



extension PTEMBuyTokensViewController: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("safari did finish")
    }
    
    func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        print("url\(URL.absoluteString)")
        let urlString = "https://www.google.co.in/"
        
        if URL.absoluteString == urlString {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                // do stuff 42 seconds later
                self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name("popAndPush"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }
    }
}
