//
//  SubscriptionViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 20/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit
import PageMenu

class SubscriptionViewController: UIViewController
{

     var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad()
      {
        
        super.viewDidLoad()
        var controllerArray: [UIViewController] = []
        
        //CompletedTestListViewController
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        //StarterPackViewController
        let controller1 = mainStoryboard.instantiateViewController(withIdentifier: "StarterPackViewController")
        controller1.title = "PTE Starter Pack"
        controllerArray.append(controller1)
        
        
        let controller2 = mainStoryboard.instantiateViewController(withIdentifier: "PremiumViewController")
        controller2.title = "PTE Premium Pack"
        controllerArray.append(controller2)
        
        let controller3 = mainStoryboard.instantiateViewController(withIdentifier: "IntensiveViewController")
        controller3.title = "PTE Intensive Pack"
        controllerArray.append(controller3)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)),
            //.bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "GLACIALINDIFFERENCE-BOLD", size: 15.0)!),
            .menuHeight(40.0),
            .menuItemWidth(150),
            .selectedMenuItemLabelColor(UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParent: self)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
