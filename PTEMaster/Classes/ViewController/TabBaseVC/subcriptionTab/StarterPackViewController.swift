//
//  StarterPackViewController.swift
//  PTEMaster
//
//  Created by CTIMac on 20/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class StarterPackViewController: UIViewController
{
    var categoryName: [String] = ["$15", "$30", "$45" , "$60"]
    var statusint : Int = Int()

    @IBOutlet weak var collectionView_sub: UICollectionView!
    //@IBOutlet weak var view_bg: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension StarterPackViewController :  UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    //MARK:Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return categoryName.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:StarterPackCollectionViewCell = collectionView_sub.dequeueReusableCell(withReuseIdentifier: "StarterPackCell", for: indexPath) as! StarterPackCollectionViewCell
        
        if indexPath.row == statusint
        {
            cell.imgChek.isHidden = false
            cell.view_bg.borderWidth = 1.0
            cell.view_bg.borderColor = UIColor(red: 64/255.0, green: 192/255.0, blue: 2/255.0, alpha: 1)
            cell.imgChek?.image = #imageLiteral(resourceName: "check_icon")
            cell.lblAmount.textColor = UIColor(red: 64/255.0, green: 192/255.0, blue: 2/255.0, alpha: 1)
            
            print("\(statusint)")
            
        }
        else
        {
            cell.view_bg.borderColor = UIColor.white
            cell.imgChek.isHidden = true
            cell.lblAmount.textColor = UIColor(red: 32/255.0, green: 126/255.0, blue: 255/255.0, alpha: 1)
            // print("\(statusint)")
        }

        cell.lblAmount.text = categoryName[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        statusint = indexPath.row
        self.collectionView_sub.reloadData()
        //self.collectionView_Amount.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:100,height:110)
    }
    
}
