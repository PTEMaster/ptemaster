//
//  StudyVC.swift
//  PTEMaster
//
//  Created by mac on 11/08/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import UIKit
import QuickLook

class StudyVC: UIViewController {

    @IBOutlet weak var collectionViewStudy: UICollectionView!
    @IBOutlet weak var tblViewPdf: UITableView!
     var sections = [StudyModel]()
    var selectedIndex: Int = 0
    var arrStorageModel = [storageModel]()
    lazy var previewItem = NSURL()
    
    var toalCreditTime = ""
    var is_student = ""
    var full_access = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
      setScreenLayout()
        storage_subscriptionApi("1")
    }
    
    
    func setScreenLayout() {
        sections.append(StudyModel(title: "79+ bands templates", index: "1"))
         sections.append(StudyModel(title: "65+ bands templates", index: "2"))
         sections.append(StudyModel(title: "50+ bands templates", index: "3"))
         sections.append(StudyModel(title: "Useful tools", index: "4"))
       //  sections.append(StudyModel(title: "Top scorer report", index: "5"))
         collectionViewStudy.reloadData()
         tblViewPdf.reloadData()
    }
}


extension StudyVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewStudyCell", for: indexPath) as! collectionViewStudyCell
        cell.viewIndicator.isHidden = selectedIndex != indexPath.item
        cell.lblTitle.text = sections[indexPath.row].title
        cell.lblTitle.textColor = selectedIndex == indexPath.item ? cell.viewIndicator.backgroundColor : UIColor.darkGray
       
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: collectionView.frame.size.height)
        if sections.indices.contains(indexPath.row) {
            size.width = sections[indexPath.row].titleWidth + 12 + 12
            // left padding + right padding + distance between image and label + image width
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        storage_subscriptionApi("\(sections[indexPath.row].index)")
        collectionViewStudy.reloadData()
        collectionViewStudy.scrollToItem(at: IndexPath(row: indexPath.item, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
}


class collectionViewStudyCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewIndicator: UIView!
    
}

extension StudyVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStorageModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tblViewPdfCell", for: indexPath) as! tblViewPdfCell
        cell.lblAns.text = arrStorageModel[indexPath.row].storage_description
        let pdfPath =  arrStorageModel[indexPath.row].storage_data!
        let itemUrl = URL(string: pdfPath)
        
        let docExtension = itemUrl!.pathExtension
        print("docExtension-\(docExtension)-")
        if docExtension == "pdf"{
            cell.imgDoc.image = UIImage(named: "pdf")
        }else{
            cell.imgDoc.image = UIImage(named: "doc")
        }
        //unlock download
 /*     print("storage_subscription",arrStorageModel[indexPath.row].storage_subscription)
        print("is_student",self.is_student)
        print("toalCreditTime",self.toalCreditTime)
        print("full_access",self.full_access)*/
        print((arrStorageModel[indexPath.row].storage_subscription == "1" ||  self.is_student == "1" || self.toalCreditTime == "1" || self.full_access == "1"))
        
        if (arrStorageModel[indexPath.row].storage_subscription == "1" ||  self.is_student == "1" || self.toalCreditTime == "1" || self.full_access == "1") {
            cell.imgLock.isHidden = true // hide lock
        }else{
            //lock popup
            cell.imgLock.isHidden = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let aa = arrStorageModel[indexPath.row]
        
        if (arrStorageModel[indexPath.row].storage_subscription == "1" ||  self.is_student == "1" || self.toalCreditTime == "1" || self.full_access == "1") {
            //BaseUrlShowPDF
            let pdfPath = BaseUrlShowPDF +  arrStorageModel[indexPath.row].storage_data!
            let fileName =  arrStorageModel[indexPath.row].storage_data ?? ""
             appDelegate.showHUD()
            // Download file
                self.downloadfile(pdfPath, fileName, completion: {(success, fileLocationURL) in
                if success {
                    // Set the preview item to display======
                    self.previewItem = fileLocationURL! as NSURL
                    DispatchQueue.main.async {
                    // Display file
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                // your code hear
                          appDelegate.hideHUD()
                        })
                      
                        let previewController = QLPreviewController()
                        previewController.dataSource = self
                        previewController.navigationItem.rightBarButtonItem = UIBarButtonItem()
                        self.present(previewController, animated: true, completion: { })
                    }
                }else{
                    debugPrint("File can't be downloaded")
                }
                
            })
            
            }else{
                Util.showAlertWithCallback(AppConstant.appName, message: "You have to purchase Vip token", isWithCancel: false)
        }
        
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 45.0
//    }
    func downloadfile(_ path : String, _ fileName : String , completion: @escaping (_ success: Bool,_ fileLocation: URL?) -> Void){
        print("path-\(path)-")
        let itemUrl = URL(string: path)
        print("itemUrl-\(itemUrl)-")
        let docExtension = itemUrl!.pathExtension
       
        
        // then lets create your document folder url
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
       
        // lets create your destination file url
    //  let destinationUrl = documentsDirectoryURL.appendingPathComponent("\(fileName).\(docExtension)")
        let destinationUrl = documentsDirectoryURL.appendingPathComponent("\(fileName)")
        print("destinationUrl\(destinationUrl)")
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            debugPrint("The file already exists at path")
            completion(true, destinationUrl)
            // if the file doesn't exist
        } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: itemUrl!, completionHandler: { (location, response, error) -> Void in
                guard let tempLocation = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    print("File moved to documents folder")
                    completion(true, destinationUrl)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completion(false, nil)
                }
            }).resume()
        }
    }
    
}

class tblViewPdfCell: UITableViewCell {
    
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
     @IBOutlet weak var imgDoc: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension StudyVC{
    func storage_subscriptionApi (_ categoryId :String) {
        if appDelegate.isNetworkAvailable {
            let postParams: [String: Any] = [ "userid" : "\(AppDataManager.shared.loginData.id!)", "cat_id": categoryId, "parant": "\(0)"]
            appDelegate.showHUD()
            print("Api-\(BaseUrl + RequestPath.lastminutestudy.rawValue)-")
            print("Post Parameter is:\(postParams)")
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in postParams {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: BaseUrl + RequestPath.lastminutestudy.rawValue, method: .post, headers: nil) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        do
                        {
                            //  Loader.hideLoader()
                            appDelegate.hideHUD()
                            if response.result.value == nil {
                                Util.showAlertWithCallback(AppConstant.appName, message: AppConstant.couldNotConnect, isWithCancel: false)
                            }else {
                                let swiftyJsonResponseDict = JSON(response.result.value!)
                                if swiftyJsonResponseDict ["status"].intValue == 1 {
                                    
                                    
                                    
                                    print(" swifty json print \(swiftyJsonResponseDict)")
                                    if let dashboradDataDict = swiftyJsonResponseDict["data"].dictionary {
                                        let user = dashboradDataDict["user"]?.dictionary
                                        self.full_access = user?["full_access"]?.string ?? "0"
                                        self.is_student = user?["is_student"]?.string ?? "0"
                                        self.toalCreditTime = dashboradDataDict["toalCreditTime"]?.string ?? "0"
                                        
                                        if let arrTestList = dashboradDataDict["storage"]?.array , arrTestList.count > 0{
                                            self.arrStorageModel.removeAll()
                                            for dict in arrTestList {
                                                let obj = storageModel(json: dict)
                                                self.arrStorageModel.append(obj)
                                            }
                                        }
                                        self.tblViewPdf.reloadDataInMain()
                                    }
                                }else if swiftyJsonResponseDict ["status"].intValue != 1 {
                                    Util.showAlertWithCallback(AppConstant.appName, message: swiftyJsonResponseDict ["msg"].string, isWithCancel: false)
                                }
                            }
                        }
                    }
                case .failure(let error):
                    appDelegate.hideHUD()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        } else {
            Util.showAlertWithMessage(AppConstant.networkMessage, title: "")
        }
    }
}

//--model

struct StudyModel {
    var title: String {
        didSet {
            self.titleWidth = title.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        }
    }
    var index: String
    var titleWidth: CGFloat
}


extension StudyModel {
    
    init(title: String, index: String) {
        self.title = title
        self.index = index
        self.titleWidth = title.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
    }
    
}
/*
 {
 
   "storage_subscription" : "1",
   "parent_folder" : "0",
   "storage_status" : "0",
   "storage_description" : "Writing template",
   "storage_name" : "Writing template",
   "created_at" : "2020-02-16",
   "storage_category" : "1",
   "storage_data" : "5e49200c813a1.pdf",
   "storage_id" : "71",
   "storage_type" : "2",
   "storage_short_desc" : "Writing template"
 }
 */

struct storageModel {
    
    var storage_name: String?
    var storage_category: String?
    var storage_data: String?
    var created_at: String?
    var storage_id: String?
    var storage_short_desc: String?
    var storage_subscription: String?
    var storage_description: String?
    var storage_type: String?
    var parent_folder: String?
    var storage_status: String?
    
    init(json: JSON) {
        storage_name = json["storage_name"].string
        storage_category = json["storage_category"].string
        storage_data = json["storage_data"].string
        created_at = json["created_at"].string
         storage_id = json["storage_id"].string
         storage_short_desc = json["storage_short_desc"].string
         storage_subscription = json["storage_subscription"].string
         storage_description = json["storage_description"].string
         storage_type = json["storage_type"].string
         parent_folder = json["parent_folder"].string
         storage_status = json["storage_status"].string
    }

}


//MARK:- QLPreviewController Datasource

extension StudyVC: QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        
        return self.previewItem as QLPreviewItem
    }
}





class QLSPreviewController : QLPreviewController {

        override func viewDidLoad() {
            super.viewDidLoad()



        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true )
            //This hides the share item
            let add =  self.children.first as! UINavigationController
            let layoutContainerView  = add.view.subviews[1] as! UINavigationBar
             layoutContainerView.subviews[2].subviews[1].isHidden = true

        }
    }




