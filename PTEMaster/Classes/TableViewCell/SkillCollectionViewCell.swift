//
//  SkillCollectionViewCell.swift
//  PTEMaster
//
//  Created by CTIMac on 24/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class SkillCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var view_bg: Shadow!
    @IBOutlet weak var lblNumberCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
}
