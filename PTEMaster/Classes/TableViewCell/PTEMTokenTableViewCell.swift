
//
//  PTEMTokenTableViewCell.swift
//  PTEMaster
//
//  Created by mac on 30/05/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation

protocol PTEMTokenDelegate:NSObject {
    func actionBuyNow(cell:PTEMTokenTableViewCell)
}

class PTEMTokenTableViewCell: UITableViewCell {
    @IBOutlet weak var lblstudentName: UILabel!
    @IBOutlet weak var lblCreditToken: UILabel!
    @IBOutlet weak var lblAUD: UILabel!
    @IBOutlet weak var lblCut: UILabel!
    @IBOutlet weak var lbTimeToken: UILabel!
    
    @IBOutlet weak var imgIcons: UIImageView!
    @IBOutlet weak var btnBuyNow: GradientButton!
    weak var delegate:PTEMTokenDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func actionsBuy(_ sender: Any) {
        delegate?.actionBuyNow(cell: self)
    }
    
    
    
    func isSetData(dict:TokenModal) {
        lblstudentName.text = dict.tk_pkg_name
        if  dict.tk_pkg_active_time != "0" {
            let tk_pkg_active_time = dict.tk_pkg_active_time
            lbTimeToken.text = "Time Token : " + tk_pkg_active_time! + " hours"
            lblCut.isHidden = true
            lblCreditToken.text = ""
        } else {
            lblCut.isHidden = false
            let price = Double(dict.tk_pkg_price!)!
            let discount = Double(dict.tk_pkg_discount!)!
            let discountAmount =   discount / 100 * price
            let totalAmount = price - discountAmount
            let strAmount = String(format: "%.2f", totalAmount)
            
            lblCreditToken.text = "AUD " + strAmount
        }
        lblAUD.text = "AUD " + dict.tk_pkg_price!
       // imgIcons.image =  dict.
    }
    
    
    
    
    
    
    func isSetData(dict:MyToken) {
          lblstudentName.text = dict.tokenpackname
          lblCut.isHidden = true
          if  dict.tokentype == "1" {
            let tk_pkg_active_time = (Int(dict.timecount!) ?? 0 )/3600
              lblCreditToken.text = "Time Token"
            lblAUD.text = "\(tk_pkg_active_time)" + " Hours"
          } else {
              let tk_pkg_active_time = dict.tokencount
              lblCreditToken.text = "Credit Token"  //+ dict.tk_pkg_credits!+
              lblAUD.text = "\(tk_pkg_active_time ?? "")" + " Tokens"
          }
       //   lblAUD.text = "AUD " + dict.tk_pkg_price!
         // imgIcons.image =  dict.
      }
    
   /* {
        "endtime" : "2019-12-09 21:57:02",
        "affiliated_by" : "0",
        "promo" : "0",
        "timecount" : "14400",
        "starttime" : "2019-12-09 17:57:02",
        "id" : "867",
        "purchase_token" : "",
        "finalprice" : "",
        "tokenpackid" : "20",
        "buy_date" : "2019-12-05",
        "tokentype" : "1",
        "activestatus" : "1",
        "tokencount" : "0",
        "purchase_status" : "1",
        "tokenpackname" : "In-House Tokens ( Do not buy)",
        "userid" : "1321"
      }*/
    
    
}
