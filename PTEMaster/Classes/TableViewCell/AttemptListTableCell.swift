//
//  AttemptListTableCell.swift
//  PTEMaster
//
//  Created by CTIMac on 15/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class AttemptListTableCell: UITableViewCell {

    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblPurchaseDate: UILabel!
    @IBOutlet weak var lblTotalQuestion: UILabel!
    @IBOutlet weak var lblTestTime: UILabel!
    @IBOutlet weak var imgCompletedStatus: UIImageView!
    @IBOutlet weak var lblDuration: UILabel!
    
    var continueTapped: (()-> Void)? = nil
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(info: HistoryListModel, continueClickTapped: @escaping() -> Void ) {
        self.continueTapped = continueClickTapped
        lblTestTime.text = info.testTime != "" ? info.testTime.validate : "0"
        
        if info.completedStatut.validate == "1" {
            imgCompletedStatus.image = UIImage(named: "check_icon")
        }else {
            imgCompletedStatus.image = UIImage(named: "cross")
        }
        
        lblTotalQuestion.text = info.testQuestionNumber != "" ? info.testQuestionNumber.validate : "0"
        lblPurchaseDate.text = info.buyDate != "" ? "Purchased On " + info.buyDate.validate : "0"
        
        
        if let detailInfo  = info.details {
            lblCategoryTitle.text = detailInfo.name != "" ? detailInfo.name.validate : "N/A"
            
            lblDuration.text = detailInfo.durationInFormate != "" ? detailInfo.durationInFormate.validate + " hr" : "0"
        }
        
    }
    
    
    @IBAction func btnContinueAction(_ sender: Any) {
        self.continueTapped?()
    }
    
    
}
