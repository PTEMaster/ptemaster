//
//  CheckAnswerFillInTheBlanksCell.swift
//  PTEMaster
//
//  Created by Honey Maheshwari on 22/04/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class CheckAnswerFillInTheBlanksCell: UITableViewCell {

    @IBOutlet weak var corectAnswerLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupData(_ model: CheckAnswerFTBModel) {
        corectAnswerLabel.text = model.correctAnswer
        answerLabel.text = model.userAnswer
    }
    
}
