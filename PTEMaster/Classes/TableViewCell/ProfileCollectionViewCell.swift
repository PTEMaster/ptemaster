//
//  ProfileCollectionViewCell.swift
//  PTEMaster
//
//  Created by mac on 13/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lblNumberTest: UILabel!
    @IBOutlet weak var lblTaskName: UILabel!
    
    @IBOutlet weak var lblFooter: UILabel!
    
}
