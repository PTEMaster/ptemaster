//
//  CloudView.swift
//  CloudView
//
//  Created by Honey Maheshwari on 18/02/18.
//  Copyright © 2018 Honey Maheshwari. All rights reserved.
//

import UIKit

protocol CloudViewDelegate {
    func didRemoveCloud(at index: Int, value: String)
}

class CloudView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: CloudViewDelegate?
    var arrTags = [String]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    func initialSetup() {
        _ = Bundle.main.loadNibNamed("CloudView", owner: self, options: nil)
        self.addSubview(self.contentView)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        self.collectionView.register(UINib(nibName: "CloudViewCell", bundle: nil), forCellWithReuseIdentifier: "CloudViewCell")
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 25)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CloudViewCell", for: indexPath) as! CloudViewCell
        
        cell.setupData(text: self.arrTags[indexPath.row])
         //  cell.setupData(text: self.arrTags[indexPath.row]) {
            // self.removedButtonTapped(at: indexPath)
         // }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CloudViewCell", for: indexPath) as! CloudViewCell
        cell.backgroundColor = UIColor.red
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CloudViewCell", for: indexPath) as! CloudViewCell
        
        cell.backgroundColor = UIColor.green
        
    }

    func removedButtonTapped(at indexPath: IndexPath) {
        let str = self.arrTags[indexPath.row]
        self.arrTags.remove(at: indexPath.row)
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.scrollTo(indexPath: indexPath)
        }
        if let delegate = self.delegate {
            delegate.didRemoveCloud(at: indexPath.row, value: str)
        }
    }
    
    func add(_ text: String) {
        self.arrTags.append(text)
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.scrollTo(indexPath: IndexPath(item: self.arrTags.count - 1, section: 0))
        }
    }
    
    func add(array: [String]) {
        self.arrTags = array
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func scrollTo(indexPath: IndexPath) {
        if self.indexPathIsValid(indexPath) {
            DispatchQueue.main.async {
                self.collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.right, animated: false)
            }
        }
    }
    
    func indexPathIsValid(_ indexPath: IndexPath) -> Bool {
        return indexPath.section < self.collectionView.numberOfSections && indexPath.row < self.collectionView.numberOfItems(inSection: 0)
    }
    
}
