//
//  CloudViewCell.swift
//  CloudView
//
//  Created by Honey Maheshwari on 18/02/18.
//  Copyright © 2018 Honey Maheshwari. All rights reserved.
//

import UIKit

class CloudViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
  //  @IBOutlet weak var removeButton: UIButton!
    
  //  var removeBlock: (() -> Void)!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //self.contentView.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    func setupData(text: String ) {
        self.label.text = text
       // self.removeBlock = removeBlock
        
       // removeBlock: @escaping (() -> Void)
    }
    
  /*  @IBAction func removeButtonTapped(_ sender: Any) {
        self.removeBlock()
    }*/

}
