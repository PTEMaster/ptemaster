//
//  MultipleChoiceVC.swift
//  PTEMaster
//
//  Created by mac on 28/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class MultipleChoiceVC: UIViewController {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var tblAnswere: UITableView!
    @IBOutlet weak var consTblHeight: NSLayoutConstraint!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    var arrOptionModel = [OptionsModel]()
    var timeOnScreen: TimeInterval = 0
    var displayCorrectAns = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblAnswere.delegate = self
        tblAnswere.dataSource = self
        setupData()
    }
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblDescription.text = mockQestionModel.description?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.question?.removeHtmlFromString()
        }else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblDescription.text = mockQestionModel.description?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.question?.removeHtmlFromString()
        }
        
        print(arrOptionModel.count)
        consTblHeight.constant = CGFloat(arrOptionModel.count * 45)
    }
    
    func updateTimer() {
        timeOnScreen += 1
    }
    
    //MARK:- ans api call
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC {
            if let mockQestionModel = self.mockQestionModel {
                
                let selectAnsId = arrOptionModel.filter(({$0.isSelected})).map(({$0.id.validate})).joined(separator: ",")
                
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                print(sectionTime)
                
                let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" ,"user_id": "\(AppDataManager.shared.loginData.id!)", "reading_section_timer" : "\(Int(sectionTime))" , "optionid": selectAnsId]
                
                testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.readingResult.rawValue)
            }
        }
    }
    
    func checkAns() {
        var isChecked : Bool = false
             for dict in arrOptionModel {
                 if dict.isSelected == true{
                     isChecked = true
                 }
             }
             if !isChecked{return}
        displayCorrectAns = true
        self.tblAnswere.reloadDataInMain()
    }

}

extension MultipleChoiceVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptionModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAnswere.dequeueReusableCell(withIdentifier: "ReadingAnswereCell", for: indexPath) as! ReadingAnswereCell
        cell.lblAns.text = arrOptionModel[indexPath.row].option ?? "N/A"
        
        cell.btnCheck.accessibilityLabel = "\(indexPath.row),\(indexPath.section)"
        cell.btnCheck.addTarget(self, action: #selector(btnCheckUncheck(_:)), for: .touchUpInside)
        cell.btnCheck.isSelected = arrOptionModel[indexPath.row].isSelected
        
        if  cell.btnCheck.isSelected{
                           cell.btnCheck.tintColor = UIColor.systemBlue
                     }else{
                       cell.btnCheck.tintColor = UIColor.white
                      }
        
        
        if displayCorrectAns, arrOptionModel[indexPath.row].correct == "1" {
          //  cell.backgroundColor =  AppColor.greenClr
            cell.backgroundColor = AppColor.greenClr
        }else {
            cell.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //for multiple selection
        print(indexPath.row)
        arrOptionModel[indexPath.row].isSelected = !arrOptionModel[indexPath.row].isSelected
        tblAnswere.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 0.4) {
//            cell.transform = CGAffineTransform.identity
//        }
//    }
    
    @objc private func btnCheckUncheck(_ sender: UIButton) {
           let index : IndexPath = getCurrent(element: sender)
         print(index.row)
          arrOptionModel[index.row].isSelected = !arrOptionModel[index.row].isSelected
        tblAnswere.reloadData()
       }
    
}
