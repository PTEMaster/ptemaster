//
//  ReadingAnswereCell.swift
//  PTEMaster
//
//  Created by mac on 28/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class ReadingAnswereCell: UITableViewCell {
    
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
