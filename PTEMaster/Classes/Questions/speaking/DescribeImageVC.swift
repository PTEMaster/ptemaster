//
//  DescribeImageVC.swift
//  PTEMaster
//
//  Created by mac on 25/01/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech
import SDWebImage


class DescribeImageVC: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var imgQuestion: UIImageView!
    @IBOutlet weak var consHeightImg: NSLayoutConstraint!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    //HP
    var arrResult = [[String:Any]]()
    @IBOutlet weak var viewCollectionResult: UIView!
    @IBOutlet weak var collectionViewResult: UICollectionView!
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var lblToStop: UILabel!
    @IBOutlet weak var viewRecording: UIView!
    @IBOutlet weak var contHeightCollection: NSLayoutConstraint!
    
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    var waitTime: TimeInterval = 0
    var responseTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var screenStatus = ScreenStatus.waiting
    var recordTime: TimeInterval = 0
    var timesPause = Int64()
    var prev_length:Int = 0
    var t1 = Int64()
    var t0 = Int64()
    var pausetime:Int64 = 0
    var abbs = Double()
    var pausefinaltime:Int64 = 0
    var isStopRecording : Bool = false
    @IBOutlet weak var viewTextView: UIView!
    
    var strMySpeakingText = ""
    
    enum ScreenStatus {
        case waiting
        case recording
        case ideal
    }
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        textView.delegate = self
        setupData()
        textView.text = ""
        viewTextView.isHidden = true
        strMySpeakingText  = ""
        speechRecognizer.delegate = self
        //   viewRecording.isHidden = true
        checkForSpeechRecognizer()
        setupSaveButtonTitle(false)
        
        //hp
        self.viewCollectionResult.isHidden = true
        lblToStop.isHidden = true
        btnSkipOrSave.isSelected = false
        
        if let layout = collectionViewResult?.collectionViewLayout as? UICollectionViewFlowLayout{
            // layout.minimumLineSpacing = 1
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            let size = CGSize(width:(collectionViewResult!.bounds.width )/4, height: (collectionViewResult!.bounds.width )/4)
            print(size)
            layout.itemSize = size
        }
        
    }
    
    
    /*  override func viewDidAppear(_ animated: Bool) {
     super.viewDidAppear(animated)
     self.pauseAndStartTimer(false)
     if let testDetailVC = self.parent as? PracticeDetailVC {
     testDetailVC.startTimer()
     }
     }
     override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
     self.stopRecording()
     }*/
    
    
    func checkForSpeechRecognizer() {
        switch SFSpeechRecognizer.authorizationStatus() {
        case .authorized:
            break
        case .denied:
            print("User denied access to speech recognition")
        case .restricted:
            print("Speech recognition restricted on this device")
        case .notDetermined:
            askForSpeechRecognizer()
            print("Speech recognition not yet authorized")
        }
    }
    
    func askForSpeechRecognizer() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            self.checkForSpeechRecognizer()
        }
    }
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            if let url = URL(string: BaseUrlTestQuestion  + mockQestionModel.testId.validate + "/" + mockQestionModel.image.validate) {
                imgQuestion.sd_imageIndicator?.startAnimatingIndicator()
                imgQuestion.sd_setImage(with: url, placeholderImage: UIImage(named: "test_icon"))
                
            }
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            
        }else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            if let url = URL(string: BaseUrlTestQuestion  + mockQestionModel.testId.validate + "/" + mockQestionModel.image.validate) {
                
                imgQuestion.sd_setImage(with: url, placeholderImage: UIImage(named: "test_icon"))
            }
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
        }
    }
    
    func updateTimer() {
        timeOnScreen += 1
        print("screenStatus--\(screenStatus)=====\(audioEngine.isRunning)----------\(isStopRecording)")
        if SFSpeechRecognizer.authorizationStatus() == .authorized {
            if screenStatus == .waiting {
                waitTime -= 1
                btnSkipOrSave.isSelected = false
                screenStatus = waitTime > 0 ? .waiting : .recording
                if screenStatus == .recording, !audioEngine.isRunning {
                    startRecording()
                }
            } else if screenStatus == .recording {
                if audioEngine.isRunning {
                    recordTime += 1
                }
                textView.textColor = .black
                btnSkipOrSave.pulsate()
                btnSkipOrSave.isSelected = true
                responseTime -= 1
                if responseTime == 0 {
                    if let testDetailVC = self.parent as? PracticeDetailVC{
                        testDetailVC.stopTimer()
                    }
                    isStopRecording = true
                    stopRecordingAndCallAnsAPI()
                }
            }
            
            
            if screenStatus == .waiting {
                //  lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
                lblTimeLeft.text = "\(Int(waitTime))"
                
            } else if responseTime > 0, screenStatus == .recording {
                // print(" responce time ...... \(Int(responseTime)) ")
                // pauseSeconds += 1
                // lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Time left ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
                lblTimeLeft.text = "\(Int(responseTime))"
            } else {
                lblTimeLeft.text = ""
                if practiceTestModel != nil {
                    // constViewBottomHeight.constant = 0
                }
            }
            setupSaveButtonTitle(audioEngine.isRunning)
        }
    }
    
    func setupSaveButtonTitle(_ setSkip: Bool) {
        if setSkip{
            btnSkipOrSave.isSelected = true
            lblToStop.isHidden = false
        }else{
            btnSkipOrSave.isSelected = false
            lblToStop.isHidden = true
        }
    }
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        //  textViewHeightConstraint.constant = newSize.height
        scrlView.scrollToBottom(animated: true)
        view.layoutSubviews()
    }
    
    
    @IBAction func btnSkipOrSave(_ sender: Any) {
        //   print(screenStatus)
        lblToStop.isHidden = true
        if screenStatus == .waiting {
            screenStatus = .recording
            //    lblToStop.isHidden = true
            btnSkipOrSave.isSelected = true
            self.viewCollectionResult.isHidden = true
            //  stopRecording()
            
            startRecording()
            
        } else {
            
            responseTime = 0
            viewRecording.isHidden = true
            if let testDetailVC = self.parent as? PracticeDetailVC{
                testDetailVC.stopTimer()
            }
            stopRecordingAndCallAnsAPI()
        }
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        lblTimeLeft.text = ""
        print ("pause***************************************\(pause)")
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    //MARK:- btn action retry
    @IBAction func actionReset(_ sender: Any) {
        stopRecording()
        isStopRecording = false
        recordTime = 0
        abbs = 0.0
        setupData()
        strMySpeakingText = ""
        viewTextView.isHidden = true
        if let testDetailVC = self.parent as? PracticeDetailVC {
            testDetailVC.startTimer()
            testDetailVC.setNextPreviousTitle()
        }
        self.viewCollectionResult.isHidden = true
        self.viewRecording.isHidden = false
        screenStatus = .waiting
        textView.text = ""
        textView.textColor = .black
        viewTextView.isHidden = true
        strMySpeakingText  = ""
        btnSkipOrSave.isSelected = false
    }
    
}

extension DescribeImageVC: SFSpeechRecognizerDelegate {
    
    func startRecording() {
        if practiceTestModel != nil {
            /// constViewBottomHeight.constant = 0
        }
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2..
        // let speechRecognizer = SFSpeechRecognizer()
        
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .default)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        
        let inputNode = audioEngine.inputNode  //4
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        speechRecognizer.delegate = self
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            if result != nil {
                
                let currentTimeTS = Date.currentTimeStamp
                // print("\(tsLong)")
                
                let length_of_data = result?.bestTranscription.formattedString.count
                //  print("length_of_data-\(length_of_data)-")
                if  self.isStopRecording == false{
                    //  self.textView.text = result?.bestTranscription.formattedString  //9
                    self.strMySpeakingText = result?.bestTranscription.formattedString as! String
                    print("My text:---\(self.strMySpeakingText)")
                }
                
                if length_of_data ?? 0 < 1500{
                    self.isCalulationPulseTime(length_of_data ?? 0, currentTimeTS)
                }
                // print(abss)
                
                //      print(result!.bestTranscription.formattedString as Any)
                // print(" one word and two word distance:-", result?.bestTranscription.segments.distance(from: 1, to: 2) as Any)
                isFinal = (result?.isFinal)!
            }
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    func stopRecording() {
        if audioEngine.isRunning {
            audioEngine.pause()
            audioEngine.inputNode.removeTap(onBus: 0)
            audioEngine.stop()
            recognitionRequest?.endAudio()
            self.audioEngine.stop()
            self.recognitionRequest = nil
            self.recognitionTask = nil
            //
            audioEngine.reset()
        }
    }
    
    func pauseRecording() {
        if audioEngine.isRunning {
            audioEngine.pause()
            
        }
    }
    
    func reStartRecording() {
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            //microphoneButton.isEnabled = true
        } else {
            //microphoneButton.isEnabled = false
        }
    }
    
    func isCalulationPulseTime(_ length_of_data:Int ,_ timeCurrents:Int64) -> Double {
        //print("length_of_text-\(length_of_data)-")
       // print("CurrentTime----\(timeCurrents)-")
      // print("t0--\(t0)-")
       //  print("t1--\(t1)-")
        if length_of_data > 0 {
            if self.t0 != 0 {
                prev_length = length_of_data;
                t1 = Date.currentTimeStamp
                // print("fluency_t1", t1)
                pausetime = (t1 - t0);
              //  print("pausetime==\(pausetime)")
             if pausetime > 650{
               print("fluency_pausetime========---", pausetime)
             }
                t0 = t1
     
                if (pausetime >= 650 && pausetime <= 750) {
                    abbs = abbs + 0.1
                    //  print("abs_live_value", abbs)
                } else if (pausetime > 751 && pausetime <= 790) {
                    abbs = abbs + 0.2
                    // print("abs_live_value", abbs)
                } else if (pausetime >= 791 && pausetime <= 850) {
                    abbs = abbs + 0.3
                    // print("abs_live_value", abbs)
                } else if (pausetime >= 851 && pausetime <= 890) {
                    abbs = abbs + 0.4
                    // print("abs_live_value",  abbs)
                } else if (pausetime >= 891 && pausetime <= 950) {
                    abbs = abbs + 0.5
                    //  print("abs_live_value", abbs)
                } else if (pausetime >= 951) {
                    abbs = abbs + 1
                    //  print("abs_live_value", abbs)
                }
            } else {
                t0 = Date.currentTimeStamp
                t1 = t0
            }
        }
        return abbs
    }
    
    
    
    func checkAns() {
        if !viewCollectionResult.isHidden  || screenStatus == .waiting {
            return
        }
        viewRecording.isHidden = true
        self.stopRecordingAndCallAnsAPI()
    }
    
    //MARK:- ans api call
    func stopRecordingAndCallAnsAPI() {
        // screenStatus = .ideal
        DispatchQueue.main.async {
            self.stopRecording()
        }
        if  appDelegate.toalCreditTime > 0 || appDelegate.toalCredits > 0 {
            if let testDetailVC = self.parent as? MockTestDetailVC, let mockTestInfo = self.mockQestionModel {
                
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                 print(sectionTime)
                
                let recordDuration = mockTestInfo.responseTime - recordTime
                print(recordDuration)
                
                let param : [String: Any] = ["question_id": "\(mockTestInfo.id!)", "new_name": "pte","user_id": "\(AppDataManager.shared.loginData.id!)", "recordduration": "\(Int(recordDuration))", "usetxt": "\(textView.text!)", "confidance": "1", "speaking_section_timer": "\(sectionTime)" ]
                print(param)
                
                testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.sepakingResult.rawValue)
                
            } else if let practice = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
              //  let sectionTime = practice.totalTime - timeOnScreen
                //    print(sectionTime)
                
                let recordDuration = questionModal.responseTime - recordTime
                 print("recordDuration->",recordDuration)
                
                let param : [String: Any] = ["question_id": "\(questionModal.id!)"  , "user_id" : "\(AppDataManager.shared.loginData.id!)", "usetxt" : strMySpeakingText  , "abbss":abbs , "mocqdesc":questionModal.description! as Any,"question_cat":questionModal.categoryId! as Any, "pv" : SpeakingConstant.pv , "alt_pv" : SpeakingConstant.alt_pv , "alt_pv_json" : SpeakingConstant.alt_pv_json ,"finalrecorderdduration" : recordDuration]
                IPServiceHelper.shared.callAnswerAPIWithParameters(param, ansApi:BaseUrl2 +  RequestPath.practice_speaking_mobile.rawValue) { (result, responseCode, error) in
                    ///Success Case
                    if responseCode ==  200
                    {
                        DispatchQueue.main.async(execute: {
                            //  self.isStopRecording = true
                            //   appDelegate.hideHUD()
                            self.arrResult.removeAll()
                            if let dict = result{
                                var arrList = [[String:Any]]()
                                arrList.append(["finalResult": (dict["myfluency"] as Any), "color":(dict["prgressclass3"] as Any)])
                                arrList.append(["finalResult": (dict["final_pronoun"] as Any), "color":(dict["prgressclass2"] as Any)])
                                arrList.append(["finalResult": (dict["final_count"] as Any), "color":(dict["prgressclass1"] as Any)])
                                arrList.append(["finalResult": (dict["finalall"] as Any),"color":(dict["prgressclass4"] as Any)])
                                self.arrResult = arrList
                                let text  =  dict["transcript"] as? String ?? ""
                                self.setTranscriptFromPracticeDetail(text)
                            }
                            
                            self.viewCollectionResult.isHidden = false
                            self.collectionViewResult.reloadData()
                            self.viewRecording.isHidden = true
                            self.screenStatus = .waiting
                           // self.stopRecording()
                           // self.audioEngine.stop()
                        //    self.recognitionRequest?.endAudio()
                            if let testDetailVC = self.parent as? PracticeDetailVC{
                                testDetailVC.stopTimer()
                              //  testDetailVC.deduct_creditUser()
                            }
                        })
                    }else{
                        //  appDelegate.hideHUD()
                    }
                }
                
            } else {
                
            }
                   
        }else{
           // Util.showAlertWithMessage("Please purchase more tokens under \"Buy Token\" section to enable automated scoring.", title: "")
            
            Util.showAlertWithMessage("Please visit masterpte.com.au to purchase token to enable scoring.", title: "")
        }
        
        
        
    
        
        
    }
    func setTranscriptFromPracticeDetail (_ transcript : String){
        let decodedData = Data(base64Encoded: transcript)!
        let decodedString = String(data: decodedData, encoding: .utf8)!
        setHTMLFromString(htmlText: decodedString)
    }
    
    func setHTMLFromString(htmlText: String) { //GlacialIndifference-Regular GillSans
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'GlacialIndifference'; font-size: \(15)\">%@</span>", htmlText)
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        if textView.text != ""{
            viewTextView.isHidden = false
            textView.attributedText  = attrStr
        }
    }
    
    
}



extension DescribeImageVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
        //   cell.viewbg.layer.cornerRadius = cell.viewbg.frame.size.height/2
        cell.viewbg.layer.borderWidth = 2
        let dict  = arrResult[indexPath.row]
        let strResult = dict["finalResult"] as? Double ?? 0.0
        let strFinalResult = String(format: "%.1f", strResult)
        let fullArr = strFinalResult.components(separatedBy: ".")
        let first =  fullArr[0]
       // let last =  fullArr[1]
       /* if last != "0"{
            cell.lblResult.text = strFinalResult + "/" + "90"
        }else{
            cell.lblResult.text = first + "/" + "90"
        }*/
        cell.lblResult.text = first + "/" + "90"
        
        
        let colorName = dict["color"] as? String
        cell.viewbg.layer.borderColor = UIColor(named: colorName ?? "darkLightColor")?.cgColor
        if indexPath.row == 0 {
            cell.lblTitle.text = "Fluency"
            cell.viewbg.layer.borderColor = UIColor.blue.cgColor
        } else if indexPath.row == 1 {
            cell.lblTitle.text = "Pronounciation"
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
        } else if indexPath.row == 2 {
            cell.lblTitle.text = "Content"
            cell.viewbg.layer.borderColor = UIColor.brown.cgColor
        } else if indexPath.row == 3 {
            cell.lblTitle.text = "Final Result"
            cell.viewbg.layer.borderColor = UIColor.blue.cgColor
        }
        return cell
    }
}

