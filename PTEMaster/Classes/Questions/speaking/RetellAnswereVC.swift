//
//  RetellAnswereVC.swift
//  PTEMaster
//
//  Created by mac on 07/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class RetellAnswereVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var btnReplay: UIButton!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constProgressViewTraling: NSLayoutConstraint!
    @IBOutlet weak var viewRecording: UIView!
    @IBOutlet weak var btnStartStopRecording: UIButton!
    @IBOutlet weak var lblRecordingHint: UILabel!
    @IBOutlet weak var lblPracticeTimer: UILabel!
    
   //HP
        var isOn : Bool = false
        var arrResult = [[String:Any]]()
        @IBOutlet weak var viewCollectionResult: UIView!
        @IBOutlet weak var collectionViewResult: UICollectionView!
        @IBOutlet weak var scrlView: UIScrollView!
        
        var mockQestionModel: MockQestionModel?
        var practiceTestModel: PracticeQestionModel?
        var waitTime: TimeInterval = 0
        var responseTime: TimeInterval = 0
        var screenStatus = ScreenStatus.preparingToPlay
        var timeOnScreen: TimeInterval = 0
        var recordTime: TimeInterval = 0
        var timesPause = Int64()
        var prev_length:Int = 0
        var t1 = Int64()
        var t0 = Int64()
        var pausetime:Int64 = 0
        var abbs = Double()
        var pausefinaltime:Int64 = 0
        var isPracticeTest: Bool {
            return practiceTestModel != nil
        }
        
        let speechRecognizer = SpeechRecognizerManager()
        let audioPlayerManager = AudioPlayerManager()
        
        // pre audio
        enum ScreenStatus {
            case preparingToPlay
            case waiting
            case playingAudio
            case recognizingText
            case ideal
        }
    @IBOutlet weak var viewTextView: UIView!
        var strMySpeakingText = ""
    
        
        override func viewDidLoad() {
            super.viewDidLoad()
          btnReplay.isEnabled = false
            textView.delegate = self
            constBottomViewHeight.constant = 0
            viewRecording.isHidden = true
            viewCollectionResult.isHidden = true
            setupData()
            checkForSpeechRecognizer()
            setupSaveButtonTitle(true)//
            
            viewTextView.isHidden = true
            strMySpeakingText  = ""
            
            /*  if isPracticeTest {    // comment ny himanshu pal  this function call after 5 sec after audio finishd and stop speechRecognizer
             speechRecognizer.stopRecognizingAfter5Sec = true
             speechRecognizer.recognizingStopAfter5SecBlock = {
             print("Done 5 sec")
             self.pauseAndStartTimer(true) //stop
             self.btnStartStopRecording.isSelected = false
             }
             }*/
            
            if let layout = collectionViewResult?.collectionViewLayout as? UICollectionViewFlowLayout{
                   // layout.minimumLineSpacing = 1
                    layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    let size = CGSize(width:(collectionViewResult!.bounds.width )/4, height: (collectionViewResult!.bounds.width )/4)
                print(size)
                    layout.itemSize = size
            }
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        }
        //stop audio while disappear
        override func viewDidDisappear(_ animated: Bool) {
            stopPlaying(true)
            self.stopRecording()
        }
        
        func checkForSpeechRecognizer() {
            speechRecognizer.checkForSpeechRecognizer()
        }
        
        func setupData() {
            btnReplay.isHidden = !isPracticeTest
            DispatchQueue.main.async {
                self.constProgressViewTraling.constant = self.isPracticeTest ? 65 : 20
            }
            audioProgress.layer.cornerRadius = 8
            audioProgress.clipsToBounds = true
            audioProgress.layer.sublayers![1].cornerRadius = 8
            audioProgress.subviews[1].clipsToBounds = true
            
            if let mockQestionModel = self.mockQestionModel {
                // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
                // audioProgress.clipsToBounds = true
                lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
                waitTime = mockQestionModel.waitTime
                responseTime = mockQestionModel.responseTime
                // Pre-audio
                prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
                // Pre-audio
            }else if let mockQestionModel = practiceTestModel {
                //audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
                //audioProgress.clipsToBounds = true
                lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
                waitTime = mockQestionModel.waitTime
                responseTime = mockQestionModel.responseTime
                // Pre-audio
               
                prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            }
        }
        
        func updateTimer(text: String) {
            timeOnScreen += 1
            if speechRecognizer.isAuthorized {
               // print("screenStatus///////---\(screenStatus)")
                if screenStatus == .waiting {  //1
                    constBottomViewHeight.constant = isPracticeTest ? 0 : 50
                    
                  //  lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
                     lblPracticeTimer.text = "\(Int(waitTime))"
                    waitTime -= 1
                    print("waitTime////---\(waitTime)")
                    if waitTime == 0 {
                     //   appDelegate.showHUD("Preparing audio", onView: self.view)
                         btnReplay.isEnabled = true
                        lblPracticeTimer.text = ""
                        screenStatus = .playingAudio     ///////////////////////////playingAudio///////////////////////////
                        playAudio()
                    }
                    
                    self.setupSaveButtonTitle(true)
                }
                    
                else if screenStatus == .playingAudio {  //2
                     btnReplay.isEnabled = true
                    constBottomViewHeight.constant = 0
          
                  //  lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "1", thirdStr: " seconds")
                    self.setupSaveButtonTitle(true)
                }
                    
                else if screenStatus == .recognizingText {  //3
                    if speechRecognizer.isRunning {
                        recordTime += 1
                    }
                    constBottomViewHeight.constant = isPracticeTest ? 0 : 50
                   // lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Time left ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
                    lblPracticeTimer.text = "\(Int(responseTime))"
                    responseTime -= 1
                    print("responseTime////---\(responseTime)")
                    if responseTime == 0 { // 0
                        if let testDetailVC = self.parent as? PracticeDetailVC{
                        testDetailVC.stopTimer()
                        }
                       stopRecordingAndCallAnsAPI()
                    }
                    self.setupSaveButtonTitle(!speechRecognizer.isRunning)
                }
                
                else {
                    lblTimeLeft.text = ""
                    if practiceTestModel != nil {
                        constBottomViewHeight.constant = 0
                    }
                }
                updatePracticeBottomView()
            }
        }
        
        func updatePracticeBottomView() {
            if isPracticeTest {
                lblRecordingHint.isHidden = false
                if screenStatus == .waiting {
                   
                    lblPracticeTimer.textColor = UIColor.black
                    lblRecordingHint.isHidden = true
                    viewRecording.isHidden = false
                } else if screenStatus == .playingAudio {
                    viewRecording.isHidden = true
                    viewCollectionResult.isHidden = true
                } else if screenStatus == .recognizingText {
                    btnStartStopRecording.pulsate()
                    viewRecording.isHidden = false
                    viewCollectionResult.isHidden = true
                    lblPracticeTimer.textColor = UIColor.black
                    textView.textColor = .clear
                    if responseTime == 1 {
                        viewRecording.isHidden = true
                    }
                }else{
                    viewCollectionResult.isHidden = true
                    viewRecording.isHidden = true
                    lblRecordingHint.isHidden = true
                }
            }
        }
        
       
        
        // MARK:- button action
        @IBAction func btnSkipOrSave(_ sender: Any) {
             btnReplay.isEnabled = true
            if screenStatus == .waiting {
              //  appDelegate.showHUD("Preparing audio", onView: self.view)
                playAudio()
            } else if screenStatus == .recognizingText {
                stopRecordingAndCallAnsAPI()
            }
        }
        
        @IBAction func btnRepeatAudioAction(_ sender: Any) {
            replay()
        }
        
        @IBAction func btnStartStopRecording(_ sender: Any) {
            btnReplay.isEnabled = true
            if speechRecognizer.state == .recognizing {
                pauseAndStartTimer(true)
                //speechRecognizer.pauseRecording()
                btnStartStopRecording.isSelected = false
                responseTime = 0
                viewRecording.isHidden = true
                if let testDetailVC = self.parent as? PracticeDetailVC{
                    testDetailVC.stopTimer()
                }
                stopRecordingAndCallAnsAPI()
            }
           /* else if speechRecognizer.state == .paused {
                pauseAndStartTimer(false)
                btnStartStopRecording.isSelected = true
                speechRecognizer.reStartRecording()
            }*/
          else if speechRecognizer.state == .ideal || speechRecognizer.state == .stoped {
                    if   self.screenStatus == .recognizingText {
              pauseAndStartTimer(false)
              screenStatus = .recognizingText
              btnStartStopRecording.isSelected = true
              startRecording()
                }else if   self.screenStatus == .waiting {
                        // appDelegate.showHUD("Preparing audio", onView: self.view)
                     screenStatus = .playingAudio
                     playAudio()
                }
          }
        }
        
        func pauseAndStartTimer(_ pause: Bool) {
            lblTimeLeft.text = ""
            print ("pause***************************************\(pause)")
            if let practiceDetailVC = self.parent as? PracticeDetailVC {
                practiceDetailVC.pauseTimerCountDown = pause
            }
        }
    }


    extension RetellAnswereVC {
        // MARK:- Pre-audio
        func prepareAudio(from urlString: String) {
            if let url = URL(string: urlString) {
                pauseAndStartTimer(true) //timer not running on true
               appDelegate.showHUD()
               // DotLoader.shared.showLoader()
                audioPlayerManager.prepareAudio(from: url) {
                    self.pauseAndStartTimer(false)
                    self.screenStatus = .waiting  ///////////////////////////waiting///////////////////////////
                  appDelegate.hideHUD()
                }
            }
        }
        
        // MARK:- Pre-audio
        func playAudio() {
            if self.screenStatus == .playingAudio {
                pauseAndStartTimer(true)
                constBottomViewHeight.constant = 0
                playAudioAndUpdateTimer()
                viewRecording.isHidden = true
            }
        }
        
        func playAudioAndUpdateTimer() {
            audioPlayerManager.playAudio { (progress) in
                self.updateUIAccordingToAudio(progress)
            }
        }
        
        func updateUIAccordingToAudio(_ progress: Float) {
              DispatchQueue.main.async {
               
           print("progress...............................................\(progress)")
            UIView.animate(withDuration: 1.0) {
                self.audioProgress.setProgress(progress, animated: true)
            }
             self.viewRecording.isHidden = true
            
            if progress >= 1 {
                if  !self.isOn{
                    self.isOn = true
                self.pauseAndStartTimer(false)
                self.constBottomViewHeight.constant = self.isPracticeTest ? 0 : 50
                
                self.viewRecording.isHidden = false
                self.stopPlaying(!self.isPracticeTest)
              //   btnReplay.isUserInteractionEnabled = true
                self.screenStatus = .recognizingText ///////////////////////////recognizingText///////////////////////////
                //stopRecording()
                self.startRecording()
            }
        }
            }
        }
        // MARK:- Pre-audio
        func stopPlaying(_ destroyPlayer: Bool = false) {
            audioPlayerManager.stopPlaying(destroyPlayer)
        }
        
       
           
         func checkAnswer()  {
            if !viewCollectionResult.isHidden || audioPlayerManager.isPlaying || screenStatus == .waiting {
                return
            }
//             if audioPlayerManager.isPlaying {
//    //            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
//    //            })
//                audioPlayerManager.stopPlaying()
//            }
            
             stopPlaying(!isPracticeTest)
           stopRecordingAndCallAnsAPI()
            
           }
        
        func replay() {
          isOn = false
          // if !audioPlayerManager.isPlaying {
           // btnReplay.isUserInteractionEnabled = false
                    self.lblPracticeTimer.text = ""
                    self.textView.text = ""
                 viewTextView.isHidden = true
               strMySpeakingText  = ""
                    viewTextView.isHidden = true
                    strMySpeakingText  = ""
                    self.viewCollectionResult.isHidden = true
                    self.viewRecording.isHidden = true
                    self.audioProgress.progress = 0
                    self.pauseAndStartTimer(true)
                    self.stopRecording()
                    self.btnStartStopRecording.isSelected = false
                    if let testDetailVC = self.parent as? PracticeDetailVC {
                        testDetailVC.startTimer()
                    }
                    if let mockQestionModel = self.practiceTestModel {
                        self.responseTime = mockQestionModel.responseTime
                    }
                    self.screenStatus = .playingAudio
                    self.audioPlayerManager.replay { (progress) in
                        self.updateUIAccordingToAudio(progress)
                    }
         //  }
        }
    }

    extension RetellAnswereVC {
        
        func startRecording() {
            btnStartStopRecording.isSelected = true
            abbs = 0.0
            speechRecognizer.startRecording { (text) in
                let tsLong = Date.currentTimeStamp
                //  print("\(tsLong)")
                let length_of_data = text.count
                self.isCalulationPulseTime(length_of_data , tsLong)
            //    self.textView.text = text
           //     self.strMySpeakingText  = text
                self.strMySpeakingText  = text
                self.scrlView.scrollToBottom(animated: true)
                self.textView.isScrollEnabled = false
                self.view.layoutSubviews()
                
//                if self.viewCollectionResult.frame.origin.y <= self.textView.frame.origin.y +  self.textView.frame.size.height {
//                  //  self.textView.isScrollEnabled = true
//                  //  self.scrlView.isScrollEnabled = true
//                } else {
//                    self.scrlView.scrollToBottom(animated: true)
//                    self.textView.isScrollEnabled = false
//                    self.view.layoutSubviews()
//                }
            }
        }
        
        func stopRecording() {
            speechRecognizer.stopRecording()
        }
        
        func setupSaveButtonTitle(_ setSkip: Bool) {
            self.btnSkipOrSave.setTitle(setSkip ? "    SKIP    " : "    STOP & SAVE    ", for: .normal)
        }
        
        func isCalulationPulseTime(_ length_of_data:Int ,_ timeCurrents:Int64) -> Double {
            //print("length_of_text-\(length_of_data)-")
           // print("CurrentTime----\(timeCurrents)-")
          // print("t0--\(t0)-")
           //  print("t1--\(t1)-")
            if length_of_data > 0 {
                if self.t0 != 0 {
                    prev_length = length_of_data;
                    t1 = Date.currentTimeStamp
                    // print("fluency_t1", t1)
                    pausetime = (t1 - t0);
                  //  print("pausetime==\(pausetime)")
                 if pausetime > 650{
                   print("fluency_pausetime========---", pausetime)
                 }
                    t0 = t1
                  /*  if (pausetime >= 450 && pausetime <= 550) {
                        abbs = abbs + 0.1
                        //  print("abs_live_value", abbs)
                    } else if (pausetime > 551 && pausetime <= 590) {
                        abbs = abbs + 0.2
                        // print("abs_live_value", abbs)
                    } else if (pausetime >= 591 && pausetime <= 650) {
                        abbs = abbs + 0.3
                        // print("abs_live_value", abbs)
                    } else if (pausetime >= 651 && pausetime <= 690) {
                        abbs = abbs + 0.4
                        // print("abs_live_value",  abbs)
                    } else if (pausetime >= 691 && pausetime <= 750) {
                        abbs = abbs + 0.5
                        //  print("abs_live_value", abbs)
                    } else if (pausetime >= 751) {
                        abbs = abbs + 1
                        //  print("abs_live_value", abbs)
                    }*/
                    if (pausetime >= 650 && pausetime <= 750) {
                        abbs = abbs + 0.1
                        //  print("abs_live_value", abbs)
                    } else if (pausetime > 751 && pausetime <= 790) {
                        abbs = abbs + 0.2
                        // print("abs_live_value", abbs)
                    } else if (pausetime >= 791 && pausetime <= 850) {
                        abbs = abbs + 0.3
                        // print("abs_live_value", abbs)
                    } else if (pausetime >= 851 && pausetime <= 890) {
                        abbs = abbs + 0.4
                        // print("abs_live_value",  abbs)
                    } else if (pausetime >= 891 && pausetime <= 950) {
                        abbs = abbs + 0.5
                        //  print("abs_live_value", abbs)
                    } else if (pausetime >= 951) {
                        abbs = abbs + 1
                        //  print("abs_live_value", abbs)
                    }
                } else {
                    t0 = Date.currentTimeStamp
                    t1 = t0
                }
            }
            return abbs
        }
        
        
        //MARK:- ans api call
        func stopRecordingAndCallAnsAPI() {
            DispatchQueue.main.async {
                self.stopRecording()
            }
           
            
            if  appDelegate.toalCreditTime > 0 || appDelegate.toalCredits > 0 {
                if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
                    let sectionTime = testDetailVC.totalTime - timeOnScreen
                  //  print(sectionTime)
                    let recordDuration = mockQestionModel.responseTime - recordTime
                 //   print(recordDuration)
                    let param : [String: Any] = ["question_id": "\(mockQestionModel.id!)" , "new_name" : "test" , "user_id" : "\(AppDataManager.shared.loginData.id!)" , "recordduration" : "\(Int(recordDuration))" , "usetxt" : "\(textView.text!)"  , "confidance" : "1" , "speaking_section_timer" : "\(sectionTime)"]
                    //  testDetailVC.callAnswerAPIWithParameters(param, ansApi: RequestPath.sepakingResult.rawValue)
                    
                } else if let practice = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
                    let sectionTime = practice.totalTime - timeOnScreen
                  //  print(sectionTime)
                    
                    let recordDuration = questionModal.responseTime - recordTime
                  //  print(recordDuration)
                    
                    let param : [String: Any] = ["question_id": "\(questionModal.id!)"  , "user_id" : "\(AppDataManager.shared.loginData.id!)", "usetxt" : strMySpeakingText  , "abbss":abbs , "mocqdesc":questionModal.description ?? "" as Any,"question_cat":questionModal.categoryId! as Any
                        , "pv" : SpeakingConstant.pv , "alt_pv" : SpeakingConstant.alt_pv , "alt_pv_json" : SpeakingConstant.alt_pv_json ,"finalrecorderdduration" : recordDuration]
                    

                    IPServiceHelper.shared.callAnswerAPIWithParameters(param, ansApi:BaseUrl2 +  RequestPath.practice_speaking_mobile.rawValue) { (result, responseCode, error) in
                        ///Success Case
                        if responseCode ==  200
                        { DispatchQueue.main.async(execute: {
                            if  let dict = result{
                                self.dictResponseApi(dict)
                            }
                        })
                        }
                    }
                    
                }  else {
                    screenStatus = .ideal    ///////////////////////////ideal///////////////////////////
                    if practiceTestModel != nil {
                        constBottomViewHeight.constant = 0
                    }
                }
                             
            }else{
               // Util.showAlertWithMessage("Please purchase more tokens under \"Buy Token\" section to enable automated scoring. title", title: "")
                
                Util.showAlertWithMessage("Please visit masterpte.com.au to purchase token to enable scoring.", title: "")
            }
            
            
            
        }
        func dictResponseApi(_ dict: [String:Any]){
            self.arrResult.removeAll()
            var arrList = [[String:Any]]()
            
            arrList.append(["finalResult": (dict["final_pronoun"] as Any), "color":(dict["prgressclass2"] as Any)])
             arrList.append(["finalResult": (dict["myfluency"] as Any), "color":(dict["prgressclass1"] as Any)])
            arrList.append(["finalResult": (dict["final_count"] as Any), "color":(dict["prgressclass3"] as Any)])
            arrList.append(["finalResult": (dict["finalall"] as Any),"color":(dict["prgressclass4"] as Any)])
            
            self.arrResult = arrList
            let text  =  dict["transcript"] as? String ?? ""
            self.setTranscriptFromPracticeDetail(text)
            
            self.collectionViewResult.reloadData()
            self.viewCollectionResult.isHidden = false
            self.viewRecording.isHidden = true
            if let testDetailVC = self.parent as? PracticeDetailVC{
            testDetailVC.stopTimer()
                testDetailVC.deduct_creditUser()
            }
            
        }
        func setTranscriptFromPracticeDetail (_ transcript : String){
            let decodedData = Data(base64Encoded: transcript)!
            let decodedString = String(data: decodedData, encoding: .utf8)!
            setHTMLFromString(htmlText: decodedString)
        }
        
        func setHTMLFromString(htmlText: String) { //GlacialIndifference-Regular GillSans
            let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'GlacialIndifference'; font-size: \(15)\">%@</span>", htmlText)
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
                options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            print("attrStr----\(attrStr)")
         //  if textView.text != ""{
            viewTextView.isHidden = false
                  textView.attributedText  = attrStr
         //   }
            
        }
        
//        //MARK: - UITextViewDelegate
//
//
//        func textViewDidChange(_ textView: UITextView) {
//            let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
//            textViewHeightConstraint.constant = newSize.height
//            if viewCollectionResult.frame.origin.y <= textView.frame.origin.y +  newSize.height {
//              textView.isScrollEnabled = true
//                scrlView.isScrollEnabled = false
//            } else {
//                scrlView.scrollToBottom(animated: true)
//                textView.isScrollEnabled = false
//                view.layoutSubviews()
//            }
//
//        }
        
        
    }

    extension RetellAnswereVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrResult.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
            
            cell.viewbg.layer.borderWidth = 2
            let dict  = arrResult[indexPath.row]
            let strResult = dict["finalResult"] as? Double ?? 0.0
            let strFinalResult = String(format: "%.1f", strResult)
            let fullArr = strFinalResult.components(separatedBy: ".")
            let first =  fullArr[0]
           // let last =  fullArr[1]
            
            
            let colorName = dict["color"] as? String
            cell.viewbg.layer.borderColor = UIColor(named: colorName ?? "darkLightColor")?.cgColor
            cell.lblResult.text = first + "/" + "90"
             if indexPath.row == 0 {
                cell.lblTitle.text = "Pronounciation"
                cell.viewbg.layer.borderColor = UIColor.green.cgColor
              /*  if last != "0"{
                cell.lblResult.text = strFinalResult + "/" + "90"
                }else{
                cell.lblResult.text = first + "/" + "90"
                }*/
             } else  if indexPath.row == 1 {
                cell.lblTitle.text = "Fluency"
                cell.viewbg.layer.borderColor = UIColor.green.cgColor
               /* if last != "0"{
                    cell.lblResult.text = strFinalResult + "/" + "90"
                }else{
                    cell.lblResult.text = first + "/" + "90"
                }*/
             }else if indexPath.row == 2 {
                cell.lblTitle.text = "Content"
                cell.viewbg.layer.borderColor = UIColor.brown.cgColor
               /* if last != "0"{
                cell.lblResult.text = strFinalResult + "/" + "90"
                }else{
                cell.lblResult.text = first + "/" + "90"
                }*/
               
            } else if indexPath.row == 3 {
                cell.lblTitle.text = "Final"
                cell.viewbg.layer.borderColor = UIColor.red.cgColor
             /*   if last != "0"{
                cell.lblResult.text = strFinalResult
                }else{
            cell.lblResult.text = first
        }*/
            }
            return cell
        }
        
     /*   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               let width = (collectionView.frame.size.width/4.0)
               return CGSize(width: width, height: width)
             }*/
        
       /* private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: 106, height: 106)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 1;
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 1;
        }*/
        
        
    }
