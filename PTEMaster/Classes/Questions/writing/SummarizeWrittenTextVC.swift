//
//  SummarizeWrittenTextVC.swift
//  PTEMaster
//
//  Created by mac on 24/01/19..
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit

class SummarizeWrittenTextVC: UIViewController, UITextViewDelegate , UIScrollViewDelegate {
    @IBOutlet weak var viewCollectionResult: UIView!
    @IBOutlet weak var ContHeightViewCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionViewResult: UICollectionView!
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimerSecond: UILabel!
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    
    @IBOutlet weak var lblWords: UILabel!
    var timeLeft: TimeInterval = 60
    var timeOnScreen: TimeInterval = 0
    var arrResult = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textView.delegate = self
        ContHeightViewCollection.constant = 0
        viewCollectionResult.isHidden = true
        setupData()
       /* if let layout = collectionViewResult?.collectionViewLayout as? UICollectionViewFlowLayout{
               // layout.minimumLineSpacing = 1
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                let size = CGSize(width:(collectionViewResult!.bounds.width )/4, height: (collectionViewResult!.bounds.width )/4)
            print(size)
                layout.itemSize = size
        }*/
    }
    
    
    
    func setupData() {
        if let mockQestionModel = self.mockQestionModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.question?.removeHtmlFromString()
        }else if let mockQestionModel = practiceTestModel {
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = mockQestionModel.question?.removeHtmlFromString()
        }
    }
   


    func updateTimer(text: String) {
       
        timeOnScreen += 1
        lblTimerSecond.attributedText =  Util.multipleClrStr(firstStr: "Time Left", secondStr: " \(text)", thirdStr: " seconds")
        if text == "0"{
            self.callAnsAPI()
        }
        
    }
    
    func checkAns() {
        if textView.text == ""{
            return
        }
        self.callAnsAPI()
    }
        //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        let components = textView.text.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter { !$0.isEmpty }
       if words.count == 0{
            lblWords.text = "Word: \(words.count)"
        }else if words.count == 1{
            lblWords.text = "Word: \(words.count)"
        }else{
            lblWords.text = "Words: \(words.count)"
        }
        
        textViewHeightConstraint.constant = newSize.height
      //
        scrlView.scrollToBottom(animated: true)
        view.layoutIfNeeded()
    }
    
    @IBAction func actionRetry(_ sender: Any) {
        ContHeightViewCollection.constant = 0
        viewCollectionResult.isHidden = true
        textView.delegate = self
         if let testDetailVC = self.parent as? PracticeDetailVC {
        testDetailVC.callRetryAgain()
        }
    }
    
    //MARK:- ans api
    func callAnsAPI(){
       
        if  appDelegate.toalCreditTime > 0 || appDelegate.toalCredits > 0 {
                
            appDelegate.showHUD()
            if let _ = self.parent as? PracticeDetailVC, let questionModal = self.practiceTestModel {
                let ansWordCount = textView.text.count //textView.text.removeHtmlFromString().components(separatedBy: " ")

                
                let parameter: [String: Any] = ["question_cat":questionModal.categoryId! as Any, "word_count": "\(ansWordCount)" ,"question_id": "\(questionModal.id.validate)","userid": "\(AppDataManager.shared.loginData.id!)" , "user_string": "\(textView.text!)"  , "mocqdesc": questionModal.answer! ]
                print("params --\(parameter)-")
           IPServiceHelper.shared.callAnswerAPIWithParameters(parameter, ansApi:BaseUrl +  RequestPath.practice_result_writing.rawValue) { (result, responseCode, error) in
                ///Success Case
                if responseCode ==  200
                {
                    DispatchQueue.main.async(execute: {
                        appDelegate.hideHUD()
                        self.arrResult.removeAll()
                        if let dict = result{
                             var arrList = [[String:Any]]()
                             arrList.append(["finalResult": (dict["grammar"] as Any)])
                             arrList.append(["finalResult": (dict["context"] as Any)])
                            arrList.append(["finalResult": (dict["form"] as Any)])
                            arrList.append(["finalResult": (dict["context"] as Any)])
                            arrList.append(["finalResult": (dict["marks"] as Any)])
                            self.arrResult = arrList
                        }
                        
                        self.ContHeightViewCollection.constant = 270
                        self.viewCollectionResult.isHidden = false
                        self.collectionViewResult.reloadData()
                        if let testDetailVC = self.parent as? PracticeDetailVC{
                           testDetailVC.stopTimer()
                            testDetailVC.deduct_creditUser_withoutAPI()
                           // let total  = testDetailVC.toalCredits! - 1
                           
                           // self.practiceTestModel.toalCredits! = total
                         }
                    })
                }else{
                    appDelegate.hideHUD()
            }
            }
        }
        }else{
          //  Util.showAlertWithMessage("Please purchase more tokens under \"Buy Token\" section to enable automated scoring.", title: "" )
            
            Util.showAlertWithMessage("Please visit masterpte.com.au to purchase token to enable scoring.", title: "")
        }

    }
}

extension SummarizeWrittenTextVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
        
         cell.viewbg.layer.borderWidth = 2
        let dict  = arrResult[indexPath.row]
        let strResult = dict["finalResult"] as? Double ?? 0.0
        let strFinalResult = String(format: "%.1f", strResult)
       // let colorName = dict["color"] as? String
      //  cell.viewbg.layer.borderColor = UIColor(named: colorName ?? "darkLightColor")?.cgColor
       // let fullArr = strFinalResult.components(separatedBy: ".")
     //   let first =  fullArr[0]
       // let last =  fullArr[1]
        if indexPath.row == 0 {
            cell.lblTitle.text = "Grammer"
            cell.viewbg.layer.borderColor = UIColor.blue.cgColor
            cell.lblResult.text = strFinalResult + "/" + "2"
    }
         else if indexPath.row == 1 {
            cell.lblTitle.text = "Content"
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
            cell.lblResult.text = strFinalResult + "/" + "2"
        } else if indexPath.row == 2 {
            cell.lblTitle.text = "Form"
            cell.viewbg.layer.borderColor = UIColor.brown.cgColor
            cell.lblResult.text = strFinalResult + "/" + "1"
        } else if indexPath.row == 3 {
            cell.lblTitle.text = "Vocabulary"
            cell.viewbg.layer.borderColor = UIColor.yellow.cgColor
            cell.lblResult.text = strFinalResult + "/" + "2"
        }else if indexPath.row == 4 {
            cell.lblTitle.text = "Final"
            let strFinalResult = String(format: "%.1f", strResult)
            let fullArr = strFinalResult.components(separatedBy: ".")
            let first =  fullArr[0]
            let last =  fullArr[1]
            if last != "0"{
            cell.lblResult.text = strFinalResult + "/" + "90"
            }else{
                cell.lblResult.text = first + "/" + "90"
            }
            cell.viewbg.layer.borderColor = UIColor.red.cgColor
          //  cell.lblResult.text = strFinalResult + "/" + "90"
        }
        return cell
    }
    
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 106, height: 106)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    
    
}





