//
//  WriteDictationVC.swift
//  PTEMaster
//
//  Created by mac on 05/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
// WriteDictationVC

import UIKit
import AVFoundation

import MediaPlayer

class WriteDictationVC: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
     @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var viewCollectionResult: UIView!
       @IBOutlet weak var collectionViewResult: UICollectionView!
       @IBOutlet weak var contViewCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblAnswer: UILabel!
    var responseTime: TimeInterval = 0
    // pre audio
    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    var screenStatus = ScreenStatus.preparingToPlay
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    var arrOptionModel = [OptionsModel]()
    var waitTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    let audioPlayerManager = AudioPlayerManager()
    var arrResult = [[String:Any]]()
   
    
    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case Writing
        case playingAudio
        case ideal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCollectionResult.isHidden = true
        
        // Do any additional setup after loading the view.
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        textView.delegate = self
        contViewCollectionHeight.constant = 0
        setupData()
        
        //
         btnRetry.isEnabled  = false
        //   handleAudioPlayerStateChange(data: controller.player.playerState)
        //  controller.player?.togglePlaying()
        textView.text = ""
    }
    
    //stop audio
    override func viewWillDisappear(_ animated: Bool) {
        stopPlaying(true)
    }
    
 
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = false
        }
    }
    
    //MARK:- set data on summery view
    func setupData() {
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
            
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
          
            // Pre-audio
            // prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }else if let mockQestionModel = practiceTestModel {
            // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
            // audioProgress.clipsToBounds = true
            // self.audioProgress.layer.cornerRadius = 15.0
            
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            
         
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
            
        }
    }
    
    
    func filterAnswer(){
        if let mockQestionModel = practiceTestModel {
            lblAnswer.text =  mockQestionModel.description
            
            var answer = mockQestionModel.answer!.removeHtmlFromString()
            answer = answer.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
            let strSeprated = answer.components(separatedBy: " ")
            let myText = textView.text.removeHtmlFromString()
            var strmyTextSeprated = myText.components(separatedBy: " ")
          var  count : Int = 0
            for str in strSeprated{
                 for index in 0..<strmyTextSeprated.count {
                    let str2 = strmyTextSeprated[index]
                    if str == str2 {
                        strmyTextSeprated.remove(at: index)
                        count += 1
                        break
                    }
                }
            }
            let result = (count * 90)/strSeprated.count
            self.arrResult.removeAll()
            var dict = [String : Any]()
            dict["score"] = (result)
            self.arrResult.append(dict)
            self.collectionViewResult.reloadData()
        }
    }
    
    
    func updateTimer(text: String) {
        print(" screenStatus \(screenStatus)")
        timeOnScreen += 1
        if screenStatus == .preparingToPlay {
            waitTime -= 1
            self.constBottomViewHeight.constant = 50
            self.viewSkipOrSave.isHidden = false
            if waitTime == 0 {
                pauseAndStartTimer(true)
                screenStatus = .playingAudio
                 btnRetry.isEnabled  = true
                                playAudio()
                self.constBottomViewHeight.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
            btnSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
        }else if screenStatus == .playingAudio {
            self.constBottomViewHeight.constant = 0
            self.viewSkipOrSave.isHidden = true
           // pauseAndStartTimer(true)
        } else if screenStatus == .Writing {
         //   self.constBottomViewHeight.constant = 50
            btnSkipOrSave.isHidden = false
            self.viewSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("   STOP   ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "End in ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
            responseTime -= 1
            if responseTime == 0 {
                pauseAndStartTimer(true)
                self.constBottomViewHeight.constant = 0
                self.viewSkipOrSave.isHidden = true
                print("call api")
                self.btnRetry.isEnabled = true
            }
        }
    }
    
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        self.scrlView.scrollToBottom(animated: true)
        self.textView.isScrollEnabled = false
        self.view.layoutSubviews()
        view.layoutSubviews()
    }
    
    //MARK:- btn skip action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        if screenStatus == .preparingToPlay {
            DispatchQueue.main.async {
                self.pauseAndStartTimer(true)
                self.screenStatus = .playingAudio
                self.btnRetry.isEnabled  = true
                self.playAudio()
                self.constBottomViewHeight.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
        }else if screenStatus == .Writing {
            pauseAndStartTimer(true)
            self.constBottomViewHeight.constant = 0
            self.viewSkipOrSave.isHidden = true
            btnRetry.isEnabled = true
            print("call api")
            // contViewCollectionHeight.constant = 150
        }
    }
    
    func  checkAns(){
        pauseAndStartTimer(true)
        self.viewSkipOrSave.isHidden = true
        btnRetry.isEnabled = true
        viewCollectionResult.isHidden = false
        filterAnswer()
        contViewCollectionHeight.constant = 180
    }
    
    @IBAction func actionRetry(_ sender: Any) {
        lblAnswer.text = ""
        self.constBottomViewHeight.constant = 0
        viewCollectionResult.isHidden = true
       self.audioProgress.progress = 0
         self.pauseAndStartTimer(true)
         self.screenStatus = .playingAudio
         self.audioPlayerManager.replay { (progress) in
             self.updateUIAccordingToAudio(progress)
         }
      
         textView.text = ""
         screenStatus = .playingAudio
         contViewCollectionHeight.constant = 0
        if let mockQestionModel = practiceTestModel {
              responseTime = mockQestionModel.responseTime
        }
       
         if let testDetailVC = self.parent as? PracticeDetailVC {
             testDetailVC.startTimer()
         }
       
     }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
}


//MARK:- Audio
extension WriteDictationVC {
 
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD()
            audioPlayerManager.prepareAudio(from: url) {
                self.pauseAndStartTimer(false)
                self.screenStatus = .playingAudio
                appDelegate.hideHUD()
            }
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            playAudioAndUpdateTimer()
            

        }
    }
    
    func playAudioAndUpdateTimer() {
        audioPlayerManager.playAudio { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    func updateUIAccordingToAudio(_ progress: Float) {
         DispatchQueue.main.async {
       print("progress...............................................\(progress)")
        UIView.animate(withDuration: 1.0) {
            self.audioProgress.setProgress(progress, animated: true)
        }
        
        if progress >= 1 {
            self.pauseAndStartTimer(false)
            self.stopPlaying(false)
        }
    }
    }
    

    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
   
    
    func replay() {
        screenStatus = .playingAudio
        audioPlayerManager.replay { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
}
extension WriteDictationVC{
    //MARK:- ans api
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
            let ansWordCount = textView.text.removeHtmlFromString().components(separatedBy: " ")
            print(" >>> \(ansWordCount)")
            print(ansWordCount)
            // total remaning time me se jitna ho gya h utna - krke jo bcha h wo dena h
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(sectionTime)" , "usetxt": "\(textView.text!)" , "word_count": "\(ansWordCount.count)" , "optionid": ""]
            testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
        } else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                constBottomViewHeight.constant = 0
            }
        }
    }
}

extension WriteDictationVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
        
        cell.viewbg.layer.borderWidth = 2
        let dict  = arrResult[indexPath.row]
        let strResult = dict["score"] as? Int ?? 0
        if strResult < 35{
             cell.viewbg.layer.borderColor = UIColor.red.cgColor
        }else if strResult > 35 &&  strResult < 60 {
             cell.viewbg.layer.borderColor = UIColor.yellow.cgColor
        }
        else if strResult > 60{
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
        }else{
             cell.viewbg.layer.borderColor = UIColor.green.cgColor
        }
         if indexPath.row == 0 {
            cell.lblTitle.text = "Score"
             cell.lblResult.text = "\(strResult)" + "/" + "90"
         }
        return cell
    }
    
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 106, height: 106)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    
}
