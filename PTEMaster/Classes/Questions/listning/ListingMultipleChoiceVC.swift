//
//  ListingMultipleChoiceVC.swift
//  PTEMaster
//
//  Created by mac on 06/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

import MediaPlayer


class ListingMultipleChoiceVC: UIViewController {
    
    @IBOutlet weak var tblOption: UITableView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var viewSkipOrSaveHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var condTblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var constProgressViewTraling: NSLayoutConstraint!
   //  private let controller = AudioController.shared
    var mockQestionModel: MockQestionModel?
    var arrOptionModel = [OptionsModel]()
    var practiceTestModel: PracticeQestionModel?
    
    weak var practiceDetailVC: PracticeDetailVC?

    var screenStatus = ScreenStatus.preparingToPlay
    var waitTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var displayCorrectAns = false


    // pre audio
    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case playingAudio
        case ideal
    }
    
    var isPracticeTest: Bool {
        return practiceTestModel != nil
    }
    
       let audioPlayerManager = AudioPlayerManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        // Do any additional setup after loading the view.
        setupData()
        btnRetry.isEnabled  = false
    }
    
  override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
         if let practiceDetailVC = self.parent as? PracticeDetailVC {
             practiceDetailVC.pauseTimerCountDown = false
         }
     }
     //stop audio
     override func viewWillDisappear(_ animated: Bool) {
          stopPlaying(true)
     }
    
    
    func setupData() {
        
        DispatchQueue.main.async {
            self.constProgressViewTraling.constant = self.isPracticeTest ? 65 : 20
        }
        
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = (mockQestionModel.question?.removeHtmlFromString())! + " " + "?"
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
          //  prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            condTblHeight.constant = CGFloat(arrOptionModel.count * 45)
            btnSkipOrSave.isHidden = true

        }else if let mockQestionModel = practiceTestModel {
            //audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
            //audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            lblQuestion.text = (mockQestionModel.question?.removeHtmlFromString())! + " " + "?"
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            condTblHeight.constant = CGFloat(arrOptionModel.count * 45)
            btnSkipOrSave.isHidden = true

        }
        print(arrOptionModel.count)
    }
    
    func updateTimer(text: String) {
       print(" screenStatus \(screenStatus)")
        timeOnScreen += 1
       if screenStatus == .preparingToPlay {
            viewSkipOrSaveHeightConstraint.constant = 50
            waitTime -= 1
        
     
            if waitTime == 0 {
                 
                pauseAndStartTimer(true)
                screenStatus = .playingAudio
                btnRetry.isEnabled  = true
                 playAudio()
                self.viewSkipOrSaveHeightConstraint.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
            
            btnSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
        } else if screenStatus == .playingAudio {
            viewSkipOrSaveHeightConstraint.constant = 0
            viewSkipOrSave.isHidden = true
        }
    }
    
    
    //MARK:- ans api
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC , let mockQestionModel = self.mockQestionModel {
            
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                print(sectionTime)
                let selectAnsId = arrOptionModel.filter(({$0.isSelected})).map(({$0.id.validate})).joined(separator: ",")
                print(selectAnsId)
                let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(sectionTime)" , "usetxt": "" , "word_count": "" , "optionid": selectAnsId]
            
                testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
            
        }else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                viewSkipOrSaveHeightConstraint.constant = 0
            }
        }
    }
    
    func checkAns() {
        var isChecked : Bool = false
        for dict in arrOptionModel {
            if dict.isSelected == true{
                isChecked = true
            }
        }
        if !isChecked{return}
        
        displayCorrectAns = true
         btnRetry.isEnabled = true
        self.tblOption.reloadDataInMain()
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        } else if let practiceDetailVC = self.practiceDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    
    
    @IBAction func btnSkipOrSave(_ sender: Any) {
        
        if screenStatus == .preparingToPlay {
                   DispatchQueue.main.async {
                       self.pauseAndStartTimer(true)
                       self.screenStatus = .playingAudio
                    self.btnRetry.isEnabled  = true
                    self.playAudio()
                       self.viewSkipOrSave.isHidden = true
                    self.viewSkipOrSaveHeightConstraint.constant = 0
                   }
               }
    }
    @IBAction func actionRetry(_ sender: Any) {
       self.audioProgress.progress = 0
       self.pauseAndStartTimer(true)
       self.screenStatus = .playingAudio
       self.audioPlayerManager.replay { (progress) in
           self.updateUIAccordingToAudio(progress)
       }
        
        for index in 0..<arrOptionModel.count {
            arrOptionModel[index].isSelected = false
        }
        displayCorrectAns = false
        self.tblOption.reloadDataInMain()
    }

}


extension ListingMultipleChoiceVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptionModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReadingAnswereCell", for: indexPath) as! ReadingAnswereCell
        cell.lblAns.text = arrOptionModel[indexPath.row].option ?? "N/A"
        cell.btnCheck.isSelected = arrOptionModel[indexPath.row].isSelected
        cell.btnCheck.accessibilityLabel = "\(indexPath.row),\(indexPath.section)"
        cell.btnCheck.addTarget(self, action: #selector(btnCheckUncheck(_:)), for: .touchUpInside)
        if displayCorrectAns, arrOptionModel[indexPath.row].correct == "1" {
            cell.viewBg.backgroundColor = AppColor.clr1
        }else {
            cell.viewBg.backgroundColor = UIColor.clear
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //for multiple selection
        arrOptionModel[indexPath.row].isSelected = !arrOptionModel[indexPath.row].isSelected
        tblOption.reloadData()
    }
    
 
    @objc private func btnCheckUncheck(_ sender: UIButton) {
        let index : IndexPath = getCurrent(element: sender)
        // let obj = arrOptionModel[index.row]
        for i in 0 ..< arrOptionModel.count {
            var dict = arrOptionModel[i]
            if i == index.row {
                dict.isSelected = !dict.isSelected
            }else{
                dict.isSelected = false
            }
            arrOptionModel[i] = dict
        }
        tblOption.reloadDataInMain()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
      }
}

//MARK:- Audio
extension ListingMultipleChoiceVC {
 
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD()
            audioPlayerManager.prepareAudio(from: url) {
                self.pauseAndStartTimer(false)
                self.screenStatus = .preparingToPlay
                appDelegate.hideHUD()
            }
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            playAudioAndUpdateTimer()
            

        }
    }
    
    func playAudioAndUpdateTimer() {
        audioPlayerManager.playAudio { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    func updateUIAccordingToAudio(_ progress: Float) {
         DispatchQueue.main.async {
       print("progress...............................................\(progress)")
        UIView.animate(withDuration: 1.0) {
            self.audioProgress.setProgress(progress, animated: true)
        }
        
        if progress >= 1 {
            self.pauseAndStartTimer(false)
            self.stopPlaying(!self.isPracticeTest)
        }
    }
    }
    

    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
   
    
    func replay() {
        screenStatus = .playingAudio
        audioPlayerManager.replay { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
}
/*let url: URL! = URL(string: "http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIWebView_Class/UIWebView_Class.pdf")
webView.loadRequest(URLRequest(url: url))
*/
