//
//  HighlightIncorrectWordsVC.swift
//  PTEMaster
//
//  Created by mac on 06/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

import MediaPlayer
class HighlightIncorrectWordsVC: UIViewController  {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var collectionViewHighlight: UICollectionView!
    
    @IBOutlet weak var constProgressViewTraling: NSLayoutConstraint!
    let audioPlayerManager = AudioPlayerManager()
    @IBOutlet weak var btnRetry: UIButton!
    // pre audio
//    private var playerItemContext = 0
//    var audioPlayer: AVPlayer?
//    var avPlayerItem: AVPlayerItem?
    
    var screenStatus = ScreenStatus.preparingToPlay
    var mockQestionModel: MockQestionModel?
    var arrOptionModel = [OptionsModel]()
    var waitTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var arrHighlightWords = [HighlightWordModel]()
    var practiceTestModel: PracticeQestionModel?
    weak var practiceDetailVC: PracticeDetailVC?

    var isPracticeTest: Bool {
        return practiceTestModel != nil
    }
    
    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case playingAudio
        case ideal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        setupData()
       btnRetry.isEnabled  = false
    }
    
    //stop audio
    override func viewWillDisappear(_ animated: Bool) {
     
         stopPlaying(true)
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           if let practiceDetailVC = self.parent as? PracticeDetailVC {
               practiceDetailVC.pauseTimerCountDown = false
           }
       }
    
    func setupData() {
        
        DispatchQueue.main.async {
            self.constProgressViewTraling.constant = self.isPracticeTest ? 65 : 20
        }
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 8)
          //  audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
        //    prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
        }else if let mockQestionModel = practiceTestModel {
        //    audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 8)
      //      audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
         prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
        }
        
        self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
        setTextOnCloudView()
    }
    
    //MARK:- set data on cloud view
    func setTextOnCloudView() {
        
        if let mockQestionModel = self.mockQestionModel {
            
            var strSeprated = mockQestionModel.description.validate.removeHtmlFromString().components(separatedBy: " ")
            print(" >>> \(strSeprated)")
            
            for i in 0 ..< strSeprated.count {
                if strSeprated[i].trimWhitespaces.count > 0 {
                    var highlightModel = HighlightWordModel(word: strSeprated[i], isCorrect: false, displayCorrect: false, isSelected: false)
                    for j in 0 ..< arrOptionModel.count {
                        let sequence = "{\(j+1)}"
                        //  sequence ==  strSeprated[i]
                        if strSeprated[i].range(of: sequence) != nil {
                            highlightModel.word = arrOptionModel[j].option.validate
                            highlightModel.isCorrect = true
                        }
                    }
                    arrHighlightWords.append(highlightModel)
                    print(arrHighlightWords)
                }
            }
            collectionViewHighlight.reloadDataInMain()
        }else if let mockQestionModel = practiceTestModel{
            var strSeprated = mockQestionModel.description.validate.removeHtmlFromString().components(separatedBy: " ")
            print(" >>> \(strSeprated)")
            
            for i in 0 ..< strSeprated.count {
                if strSeprated[i].trimWhitespaces.count > 0 {
                    var highlightModel = HighlightWordModel(word: strSeprated[i], isCorrect: false, displayCorrect: false, isSelected: false)
                    for j in 0 ..< arrOptionModel.count {
                        let sequence = "{\(j+1)}"
                        if strSeprated[i].range(of: sequence) != nil {
                            //sequence == strSeprated[i] {
                            highlightModel.word = arrOptionModel[j].option.validate
                            highlightModel.isCorrect = true
                        }
                    }
                    arrHighlightWords.append(highlightModel)
                }
            }
            collectionViewHighlight.reloadDataInMain()
        }
    }
   
    //MARK:- show timer label and button
    func updateTimer(text: String) {
        
        print("screenStatus \(screenStatus)")
        
        timeOnScreen += 1
        if screenStatus == .preparingToPlay {
               constBottomViewHeight.constant = 50
               waitTime -= 1
           
        
               if waitTime == 0 {
                   pauseAndStartTimer(true)
                   screenStatus = .playingAudio
                   btnRetry.isEnabled  = true
                                   playAudio()
                   screenStatus = .playingAudio
                   self.constBottomViewHeight.constant = 0
                   self.viewSkipOrSave.isHidden = true
               }
               
               btnSkipOrSave.isHidden = false
               self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
               lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
           } else if screenStatus == .playingAudio {
            constBottomViewHeight.constant = 0
            viewSkipOrSave.isHidden = true
        }
    }
    
    //MARK:- ans api call
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC , let mockQestionModel = self.mockQestionModel {
                
                // **********
                // option id me jo selected string wo  aese "one, new, i, thought"  dena hai
                // user text me jo select kiya text wo /*****     <span class='usertag'>one</span>year, the number of mobile phones on cell <span class='usertag'>networks</span> is <span class='usertag'>remembered</span> to surpass the Earth’s population. “More people now own cell phones <span class='usertag'>than</span> actually have access to working toilets.” James Meadow, a microbial ecologist at the University of Oregon.   *******/   //
                
                var options = [String]()
                var userText = ""
                for highlightWord in arrHighlightWords {
                    if highlightWord.isSelected {
                        options.append(highlightWord.word)
                        userText += "<span class='usertag'>\(highlightWord.word)</span> "
                    } else {
                        userText += "\(highlightWord.word) "
                    }
                }
                let optionId = options.joined(separator: ", ")
                print("open id >>>> \(optionId)")
            
        
                let newUserText = userText.replacingOccurrences(of: "\'usertag\'", with: "@usertag@")
            
                  print("newUserText >>>>. \(newUserText))")
            
                let sectionTime = testDetailVC.totalTime - timeOnScreen
                print(sectionTime)
            
                 // **********
                let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(Int(sectionTime))" , "usetxt": "\(newUserText)" , "word_count": "" , "optionid": "\(optionId)"]
                
                print("parameter >>>> \(parameter)")

                testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
        }else {
            screenStatus = .ideal
            if practiceTestModel != nil {
                constBottomViewHeight.constant = 0
            }
        }
    }

    func checkAns() {
        var isSelected = false
              for i in 0 ..< self.arrHighlightWords.count {
                 if self.arrHighlightWords[i].isSelected {
                  isSelected = true
                  break
                  }
              }
              if !isSelected{
                  return
              }
        for i in 0 ..< self.arrHighlightWords.count {
            if self.arrHighlightWords[i].isCorrect {
                self.arrHighlightWords[i].displayCorrect = true
            }
        }
       
        self.collectionViewHighlight.reloadDataInMain()
    }
    
    //MARK:- btn skip action
    
    @IBAction func btnSkipOrSave(_ sender: Any) {
       if screenStatus == .preparingToPlay {
           DispatchQueue.main.async {
               self.pauseAndStartTimer(true)
               self.screenStatus = .playingAudio
            self.btnRetry.isEnabled  = true
            self.playAudio()
               self.viewSkipOrSave.isHidden = true
            self.constBottomViewHeight.constant = 0
           }
       }
    }
    @IBAction func actionRetry(_ sender: Any) {
        self.audioProgress.progress = 0
              self.pauseAndStartTimer(true)
              self.screenStatus = .playingAudio
              self.audioPlayerManager.replay { (progress) in
                  self.updateUIAccordingToAudio(progress)
              }
        for i in 0 ..< self.arrHighlightWords.count {
            if self.arrHighlightWords[i].isSelected {
                self.arrHighlightWords[i].isSelected = false
            }
            if self.arrHighlightWords[i].isCorrect {
                self.arrHighlightWords[i].displayCorrect = false
            }
        }
        self.collectionViewHighlight.reloadDataInMain()
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }else if let practiceDetailVC = self.practiceDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    
}



//MARK:- Audio
extension HighlightIncorrectWordsVC {
 
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD()
            audioPlayerManager.prepareAudio(from: url) {
                self.pauseAndStartTimer(false)
                self.screenStatus = .preparingToPlay
                appDelegate.hideHUD()
            }
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            playAudioAndUpdateTimer()
            

        }
    }
    
    func playAudioAndUpdateTimer() {
        audioPlayerManager.playAudio { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    func updateUIAccordingToAudio(_ progress: Float) {
         DispatchQueue.main.async {
       print("progress...............................................\(progress)")
        UIView.animate(withDuration: 1.0) {
            self.audioProgress.setProgress(progress, animated: true)
        }
        
        if progress >= 1 {
            self.pauseAndStartTimer(false)
            self.stopPlaying(!self.isPracticeTest)
        }
    }
    }
    

    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
   
    
    func replay() {
        screenStatus = .playingAudio
        audioPlayerManager.replay { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
}


extension HighlightIncorrectWordsVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateLeftAlignedLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrHighlightWords.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightCollectionCell", for: indexPath) as! HighlightCollectionCell
        cell.setupData(text: self.arrHighlightWords[indexPath.row].word)
        if self.arrHighlightWords[indexPath.row].displayCorrect, self.arrHighlightWords[indexPath.row].isCorrect {
            cell.label.backgroundColor = AppColor.clr2
        } else if self.arrHighlightWords[indexPath.row].isSelected {
            cell.label.backgroundColor = UIColor.yellow
        } else {
            cell.label.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        arrHighlightWords[indexPath.row].isSelected = !arrHighlightWords[indexPath.row].isSelected
        collectionViewHighlight.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: 22.0)
        if arrHighlightWords.indices.contains(indexPath.row) {
            size.width = arrHighlightWords[indexPath.row].wordWidth + 8
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}


//MARK:- view class for correct/incorrect
class CorrectInCorrectView: UIView {
    
    var word: String!
    var isCorrect: Bool!
    var displayCorrect: Bool!
    
    enum WordStatus {
        case selected
        case correct
        case incorrect
    }
    
    override private init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    convenience init(frame: CGRect, isCorrect: Bool, word: String, displayCorrect: Bool) {
        self.init(frame: frame)
        self.alpha = 0.5
        self.isCorrect = isCorrect
        self.word = word
        self.displayCorrect = displayCorrect
        updateViewBackground()
    }
    
    func updateViewBackground() {
        if displayCorrect, isCorrect {
            self.backgroundColor = UIColor.green
        } else if !displayCorrect, isCorrect {
            self.backgroundColor = UIColor.clear
        } else {
            self.backgroundColor = UIColor.yellow
        }
    }
    
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func convertToAttributedString() -> NSAttributedString? {
        let modifiedFontString = "<span style=\"font-family: Lato-Regular; font-size: 14; color: rgb(60, 60, 60)\">" + self + "</span>"
        return modifiedFontString.htmlToAttributedString
    }
}
