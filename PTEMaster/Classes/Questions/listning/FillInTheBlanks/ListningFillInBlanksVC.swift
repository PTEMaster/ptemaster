//
//  ListningFillInBlanksVC.swift
//  PTEMaster
//
//  Created by mac on 06/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

import MediaPlayer


class ListningFillInBlanksVC: UIViewController {
    
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var viewSkipOrSaveHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var collectionViewFillInBlanks: UICollectionView!
      @IBOutlet weak var btnRetry: UIButton!
    

    
    var screenStatus = ScreenStatus.preparingToPlay
    var waitTime: TimeInterval = 0

    var mockQestionModel: MockQestionModel?
    var arrOptionModel = [OptionsModel]()
    var arrFillInBlanks = [FillInBlanksModel]()
    var timeOnScreen: TimeInterval = 0
    var practiceTestModel: PracticeQestionModel?
    weak var practiceDetailVC: PracticeDetailVC?

    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case playingAudio
        case ideal
    }
     let audioPlayerManager = AudioPlayerManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        setupData()
         btnRetry.isEnabled  = false
    }
    
   //stop audio
        override func viewWillDisappear(_ animated: Bool) {
             stopPlaying(true)
        }
       
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = false
        }
    }
    
    func setupData() {
        
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime

            // Pre-audio
           // prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }else if let mockQestionModel = self.practiceTestModel {
           // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
           // audioProgress.clipsToBounds = true
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
             prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
             btnSkipOrSave.isHidden = true
        }
       
        setupCollectionView()
    }
    
    func checkAns() {
        for i in 0 ..< arrFillInBlanks.count {
            var fillInBlanks = arrFillInBlanks[i]
            if fillInBlanks.sequence != nil {
                fillInBlanks.word = fillInBlanks.correctAns
                arrFillInBlanks[i] = fillInBlanks
            }
        }
        collectionViewFillInBlanks.reloadDataInMain()
    }
    
    //MARK:- show timer label and button
    func updateTimer(text: String) {
        
        print("screenStatus \(screenStatus)")
        
        timeOnScreen += 1
        if screenStatus == .preparingToPlay {
               viewSkipOrSaveHeightConstraint.constant = 50
               waitTime -= 1
           
        
               if waitTime == 0 {
                 btnRetry.isEnabled  = true
                   pauseAndStartTimer(true)
                   screenStatus = .playingAudio
                    playAudio()
                   screenStatus = .playingAudio
                   self.viewSkipOrSaveHeightConstraint.constant = 0
                   self.viewSkipOrSave.isHidden = true
               }
               
               btnSkipOrSave.isHidden = false
               self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
               lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
           } else if screenStatus == .playingAudio {
            viewSkipOrSaveHeightConstraint.constant = 0
            viewSkipOrSave.isHidden = true
        }
    }
    
    //MARK:- set data on view
    func setupCollectionView() {
        if let mockQestionModel = self.mockQestionModel {
            let strSeprated = mockQestionModel.description.validate.removeHtmlFromString().components(separatedBy: " ")
            print(strSeprated)
            for i in 0 ..< strSeprated.count {
                if strSeprated[i].trimWhitespaces.count > 0 {
                    var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "" )
                    // var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "", id: strSeprated[i])
                    for j in 0 ..< arrOptionModel.count {
                        let sequence = "{\(j+1)}"
                        if sequence == strSeprated[i] {
                           
                            print(strSeprated[i])
                            fillInTheBlanks.word = ""
                            fillInTheBlanks.isEditable = true
                            fillInTheBlanks.sequence = j+1
                            fillInTheBlanks.placeholder = "  _  \(j+1)  _  "
                            fillInTheBlanks.correctAns = arrOptionModel[j].option.validate /*strSeprated[i]*/

                        }
                    }
                    arrFillInBlanks.append(fillInTheBlanks)
                }
            }
            collectionViewFillInBlanks.reloadDataInMain()
        } else if let mockQestionModel = self.practiceTestModel {
            let strSeprated = mockQestionModel.description.validate.removeHtmlFromString().components(separatedBy: " ")
            print(strSeprated)
            for i in 0 ..< strSeprated.count {
                if strSeprated[i].trimWhitespaces.count > 0 {
                    var fillInTheBlanks = FillInBlanksModel(word: strSeprated[i], isEditable: false, sequence: nil, placeholder: "", correctAns: "")
                    for j in 0 ..< arrOptionModel.count {
                        let sequence = "{\(j+1)}"
                        if sequence == strSeprated[i] {
                            
                            print(strSeprated[i])
                            fillInTheBlanks.word = ""
                            fillInTheBlanks.isEditable = true
                            fillInTheBlanks.sequence = j+1
                            fillInTheBlanks.placeholder = "  _  \(j+1)  _  "
                            fillInTheBlanks.correctAns = arrOptionModel[j].option.validate /*strSeprated[i]*/
                            
                        }
                    }
                    arrFillInBlanks.append(fillInTheBlanks)
                }
            }
            collectionViewFillInBlanks.reloadDataInMain()
        }
    }
    
    
    //MARK:- ans api call
    func callAnsAPI() {
        if let testDetailVC = self.parent as? MockTestDetailVC, let mockQestionModel = self.mockQestionModel {
        
            var userText = ""
            for fillInBlanks in arrFillInBlanks {
                if let sequence = fillInBlanks.sequence {
                    userText += (fillInBlanks.word.count > 0 ? fillInBlanks.word : "{\(sequence)}") + " "
                } else {
                    userText += fillInBlanks.word + " "
                }
            }
            print("userText >>>>> \(userText)")
            let filteredArray = arrFillInBlanks.filter({ $0.sequence != nil }).sorted { (obj1, obj2) -> Bool in
                return obj1.sequence! < obj2.sequence!
            }
        
            let optionid = filteredArray.map({ $0.word.count > 0 ? $0.word : "null" }).joined(separator: ", ")
            print("optionid >>>>> \(optionid)")
        
            let sectionTime = testDetailVC.totalTime - timeOnScreen
            print(sectionTime)
            
            let parameter: [String: Any] = ["question_id": "\(mockQestionModel.id.validate)","user_id": "\(AppDataManager.shared.loginData.id!)" , "listening_section_timer": "\(Int(sectionTime))" , "usetxt": "\(userText)" , "word_count": "" , "optionid": "\(optionid)"]
            print("parameter >>>> \(parameter)")
            
            testDetailVC.callAnswerAPIWithParameters(parameter, ansApi: RequestPath.listningResult.rawValue)
            
            
        }
    }
    
    //MARK:- btn skip action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        
        if screenStatus == .preparingToPlay {
                   DispatchQueue.main.async {
                       self.pauseAndStartTimer(true)
                       self.screenStatus = .playingAudio
                    self.btnRetry.isEnabled  = true
                    self.playAudio()
                       self.viewSkipOrSave.isHidden = true
                    self.viewSkipOrSaveHeightConstraint.constant = 0
                   }
               }
    }
    @IBAction func actionRetry(_ sender: Any) {
         self.audioProgress.progress = 0
             self.pauseAndStartTimer(true)
             self.screenStatus = .playingAudio
             self.audioPlayerManager.replay { (progress) in
                 self.updateUIAccordingToAudio(progress)
             }
        
        for i in 0 ..< arrFillInBlanks.count {
                 var fillInBlanks = arrFillInBlanks[i]
                 if fillInBlanks.sequence != nil {
                     fillInBlanks.word = ""
                     arrFillInBlanks[i] = fillInBlanks
                 }
             }
             collectionViewFillInBlanks.reloadDataInMain()
      
    }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }else if let practiceDetailVC = self.practiceDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
    
}


extension ListningFillInBlanksVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateLeftAlignedLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFillInBlanks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let fillInTheBlanksModel = arrFillInBlanks[indexPath.row]
        if fillInTheBlanksModel.isEditable {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FillInBlanksCollectionCell", for: indexPath) as! FillInBlanksCollectionCell
            if let sequence = arrFillInBlanks[indexPath.row].sequence {
                cell.textField.tag = sequence
                cell.textField.delegate = self
                cell.textField.placeholder = arrFillInBlanks[indexPath.row].placeholder
                cell.textField.text = arrFillInBlanks[indexPath.row].word
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightCollectionCell", for: indexPath) as! HighlightCollectionCell
            cell.setupData(text: arrFillInBlanks[indexPath.row].word)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 0, height: 25.0)
        if arrFillInBlanks.indices.contains(indexPath.row) {
            size.width = arrFillInBlanks[indexPath.row].wordWidth
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}

extension ListningFillInBlanksVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tag = textField.tag
        if let index = arrFillInBlanks.index(where: { (fillInBlanksModel) -> Bool in
            return fillInBlanksModel.sequence == tag
        }) {
            arrFillInBlanks[index].word = textField.text.validate
        }
    }
    
}

//MARK:- Audio
extension ListningFillInBlanksVC {
 
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD()
            audioPlayerManager.prepareAudio(from: url) {
                self.pauseAndStartTimer(false)
                self.screenStatus = .preparingToPlay
                appDelegate.hideHUD()
            }
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            playAudioAndUpdateTimer()
            

        }
    }
    
    func playAudioAndUpdateTimer() {
        audioPlayerManager.playAudio { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    func updateUIAccordingToAudio(_ progress: Float) {
         DispatchQueue.main.async {
       print("progress...............................................\(progress)")
        UIView.animate(withDuration: 1.0) {
            self.audioProgress.setProgress(progress, animated: true)
        }
        
        if progress >= 1 {
            self.pauseAndStartTimer(false)
            self.stopPlaying(false)
        }
    }
    }
    

    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
   
    
    func replay() {
        screenStatus = .playingAudio
        audioPlayerManager.replay { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
}


struct FillInBlanksModel {
    var word: String {
        didSet {
            if !isEditable {
                self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
            }
        }
    }
    var isEditable: Bool {
        didSet {
            if isEditable {
                self.wordWidth = 120
            }
        }
    }
    var wordWidth: CGFloat = 0.0
    var sequence: Int?
    var placeholder: String
    var correctAns: String
    var displayCorrectAns = false
    var id: String
}

extension FillInBlanksModel {
    
    init(word: String, isEditable: Bool, sequence: Int?, placeholder: String, correctAns: String) {
        self.word = word
        self.isEditable = isEditable
        self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        // print("wordWidth---\(wordWidth)")
        self.sequence = sequence
        self.placeholder = placeholder
        self.correctAns = correctAns
        self.id = ""        
    }
    
    init(word: String, isEditable: Bool, sequence: Int?, placeholder: String, correctAns: String, id: String) {
        self.word = word
        self.isEditable = isEditable
        self.wordWidth = word.sizeOf(UIFont.systemFont(ofSize: 15)).width + 1
        self.sequence = sequence
        self.placeholder = placeholder
        self.correctAns = correctAns
        self.id = id
    }
    
}

