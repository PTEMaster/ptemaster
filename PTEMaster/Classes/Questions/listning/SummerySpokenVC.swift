//
//  SummerySpokenVC.swift
//  PTEMaster
//
//  Created by mac on 05/02/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import UIKit
import AVFoundation

import MediaPlayer

class SummerySpokenVC: UIViewController , UITextViewDelegate {
    
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var lblShortTitle: UILabel!
    @IBOutlet weak var audioProgress: UIProgressView!    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSkipOrSave: UIButton!
    @IBOutlet weak var viewSkipOrSave: UIView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var constBottomViewHeight: NSLayoutConstraint!
     @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var viewCollectionResult: UIView!
       @IBOutlet weak var collectionViewResult: UICollectionView!
       @IBOutlet weak var contViewCollectionHeight: NSLayoutConstraint!
    var responseTime: TimeInterval = 0
    // pre audio
    private var playerItemContext = 0
    var audioPlayer: AVPlayer?
    var avPlayerItem: AVPlayerItem?
    
    var screenStatus = ScreenStatus.preparingToPlay
    var mockQestionModel: MockQestionModel?
    var practiceTestModel: PracticeQestionModel?
    var arrOptionModel = [OptionsModel]()
    var waitTime: TimeInterval = 0
    var timeOnScreen: TimeInterval = 0
    var arrResult = [[String:Any]]()
   
       let audioPlayerManager = AudioPlayerManager()
    // screen update
    enum ScreenStatus {
        case preparingToPlay
        case begining
        case Writing
        case playingAudio
        case ideal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // pre audio
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
        } catch {
            print(error)
        }
        
        textView.delegate = self
        contViewCollectionHeight.constant = 0
        viewCollectionResult.isHidden  = true
        setupData()
     //    btnRetry.isEnabled  = false
        
    }
    
    //stop audio
    override func viewWillDisappear(_ animated: Bool) {
        stopPlaying(true)
    }
    
 
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = false
        }
    }
    
    //MARK:- set data on summery view
    func setupData() {
        audioProgress.layer.cornerRadius = 8
        audioProgress.clipsToBounds = true
        audioProgress.layer.sublayers![1].cornerRadius = 8
        audioProgress.subviews[1].clipsToBounds = true
        
        if let mockQestionModel = self.mockQestionModel {
            
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            
            // Pre-audio
            // prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }else if let mockQestionModel = practiceTestModel {
            // audioProgress.transform = audioProgress.transform.scaledBy(x: 1, y: 7)
            // audioProgress.clipsToBounds = true
            // self.audioProgress.layer.cornerRadius = 15.0
            
            lblShortTitle.text = mockQestionModel.shortTitle?.removeHtmlFromString()
            waitTime = mockQestionModel.waitTime
            responseTime = mockQestionModel.responseTime
            
            // Pre-audio
            prepareAudio(from: BaseUrlTestQuestion + mockQestionModel.testId.validate + "/" + mockQestionModel.audio.validate)
            btnSkipOrSave.isHidden = true
        }
    }
    
    func updateTimer(text: String) {
        print(" screenStatus \(screenStatus)")
        timeOnScreen += 1
        if screenStatus == .preparingToPlay {
            waitTime -= 1
            self.constBottomViewHeight.constant = 50
            self.viewSkipOrSave.isHidden = false
            if waitTime == 0 {
               //  btnRetry.isEnabled  = true
                pauseAndStartTimer(true)
                screenStatus = .playingAudio
                 playAudio()
                self.constBottomViewHeight.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
            btnSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("    SKIP    ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "Beginning in ", secondStr: "\(Int(waitTime))", thirdStr: " seconds")
        }else if screenStatus == .playingAudio {
            self.constBottomViewHeight.constant = 0
            self.viewSkipOrSave.isHidden = true
           // pauseAndStartTimer(true)
        } else if screenStatus == .Writing {
            self.constBottomViewHeight.constant = 50
            btnSkipOrSave.isHidden = false
            self.viewSkipOrSave.isHidden = false
            self.btnSkipOrSave.setTitle("   STOP   ", for: .normal)
            lblTimeLeft.attributedText = Util.multipleClrStr(firstStr: "End in ", secondStr: "\(Int(responseTime))", thirdStr: " seconds")
            responseTime -= 1
            if responseTime == 0 {
                pauseAndStartTimer(true)
            
                callApi()
            }
        }
    }
    
    
    
    
    //MARK: - UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        let newSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.infinity))
        textViewHeightConstraint.constant = newSize.height
        self.scrlView.scrollToBottom(animated: true)
        self.textView.isScrollEnabled = false
        self.view.layoutSubviews()
        view.layoutSubviews()
    }
    
    //MARK:- btn skip action
    @IBAction func btnSkipOrSave(_ sender: Any) {
        if screenStatus == .preparingToPlay {
            DispatchQueue.main.async {
                self.pauseAndStartTimer(true)
                self.screenStatus = .playingAudio
              //  self.btnRetry.isEnabled  = true
                self.playAudio()
                self.constBottomViewHeight.constant = 0
                self.viewSkipOrSave.isHidden = true
            }
        }else if screenStatus == .Writing {
            pauseAndStartTimer(true)
            self.constBottomViewHeight.constant = 0
            self.viewSkipOrSave.isHidden = true
          //  btnRetry.isEnabled = true
            print("call api")
            pauseAndStartTimer(true)
            callApi()
          //   contViewCollectionHeight.constant = 150
        }
    }
    func  checkAns(){
        if viewCollectionResult.isHidden  == false  {
                   return
               }
        
        if  screenStatus == .playingAudio  {
                       return
                   }
        pauseAndStartTimer(true)
        callApi()
    }
    @IBAction func actionRetry(_ sender: Any) {
      self.audioProgress.progress = 0
         constBottomViewHeight.constant = 0
         self.viewCollectionResult.isHidden = true
           self.pauseAndStartTimer(true)
           self.screenStatus = .playingAudio
           self.audioPlayerManager.replay { (progress) in
               self.updateUIAccordingToAudio(progress)
           }
         textView.text = ""
         screenStatus = .playingAudio
         contViewCollectionHeight.constant = 0
        if let mockQestionModel = practiceTestModel {
              responseTime = mockQestionModel.responseTime
        }
       
         if let testDetailVC = self.parent as? PracticeDetailVC {
             testDetailVC.startTimer()
         }
       
     }
    
    func pauseAndStartTimer(_ pause: Bool) {
        if let practiceDetailVC = self.parent as? PracticeDetailVC {
            practiceDetailVC.pauseTimerCountDown = pause
        }
    }
}


//MARK:- Audio
extension SummerySpokenVC {
 
    func prepareAudio(from urlString: String) {
        if let url = URL(string: urlString) {
            pauseAndStartTimer(true)
            appDelegate.showHUD()
            audioPlayerManager.prepareAudio(from: url) {
                self.pauseAndStartTimer(false)
                self.screenStatus = .preparingToPlay
                appDelegate.hideHUD()
            }
        }
    }
    
    func playAudio() {
        if self.screenStatus == .playingAudio {
            pauseAndStartTimer(true)
            playAudioAndUpdateTimer()
            

        }
    }
    
    func playAudioAndUpdateTimer() {
        audioPlayerManager.playAudio { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
    func updateUIAccordingToAudio(_ progress: Float) {
         DispatchQueue.main.async {
       print("progress...............................................\(progress)")
        UIView.animate(withDuration: 1.0) {
            self.audioProgress.setProgress(progress, animated: true)
        }
        
        if progress >= 1 {
            self.screenStatus = .Writing
            self.pauseAndStartTimer(false)
            self.stopPlaying(false)
        }
    }
    }
    

    func stopPlaying(_ destroyPlayer: Bool = false) {
        audioPlayerManager.stopPlaying(destroyPlayer)
    }
    
   
    
    func replay() {
        screenStatus = .playingAudio
        audioPlayerManager.replay { (progress) in
            self.updateUIAccordingToAudio(progress)
        }
    }
    
}

extension SummerySpokenVC {
    func callApi(){
        if  appDelegate.toalCreditTime > 0 || appDelegate.toalCredits > 0 {
            if  ((self.parent as? PracticeDetailVC) != nil), let questionModal = self.practiceTestModel {
                      let components = textView.text.components(separatedBy: .whitespacesAndNewlines)
                let words    = components.filter { !$0.isEmpty }
                let ansWordCount = textView.text.removeHtmlFromString().components(separatedBy: " ").count
                           print(" >>> \(ansWordCount)")
                let param : [String: Any] = ["question_id": "\(questionModal.questionId!)"  , "user_id": "\(AppDataManager.shared.loginData.id!)", "user_string": "\(textView.text!)" , "word_count":words.count, "question_cat":questionModal.categoryId!]
                print(param)
                IPServiceHelper.shared.callAnswerAPIWithParameters(param, ansApi:BaseUrl3 +  RequestPath.practice_result_listening.rawValue) { (result, responseCode, error) in
                    ///Success Case
                    if responseCode ==  200
                    {
                        DispatchQueue.main.async(execute: {
                           self.constBottomViewHeight.constant = 0
                           self.viewSkipOrSave.isHidden = true
                                 //      self.btnRetry.isEnabled = true
                            self.contViewCollectionHeight.constant = 270
                           self.arrResult.removeAll()
                             if let dict = result{
                                var arrList = [[String:Any]]()
                                arrList.append(["finalResult": (dict["context"] as Any), "color":(dict["prgressclass3"] as Any)])
                                arrList.append(["finalResult": (dict["form"] as Any), "color":(dict["prgressclass2"] as Any)])
                                arrList.append(["finalResult": (dict["grammar"] as Any), "color":(dict["prgressclass1"] as Any)])
                                arrList.append(["finalResult": (dict["marks"] as Any),"color":(dict["prgressclass4"] as Any)])
                                arrList.append(["finalResult": (dict["spelling"] as Any),"color":(dict["prgressclass4"] as Any)])
                                self.arrResult = arrList
                                print(arrList)
                              //  let text  =  dict["transcript"] as? String ?? ""
                                //self.setTranscriptFromPracticeDetail(text)
                            }
                            self.viewCollectionResult.isHidden = false
                            self.collectionViewResult.reloadData()
                            if let testDetailVC = self.parent as? PracticeDetailVC {
                                testDetailVC.deduct_creditUser_withoutAPI()
                            }
                            
                        })
                    }
                }
            }
                               
        }else{
         //   Util.showAlertWithMessage("Please purchase more tokens under \"Buy Token\" section to enable automated scoring.", title: "" )
            Util.showAlertWithMessage("Please visit masterpte.com.au to purchase token to enable scoring.", title: "")
        }

        
    }
}

extension SummerySpokenVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultCollectionViewCell", for: indexPath) as! ResultCollectionViewCell
        
        cell.viewbg.layer.borderWidth = 2
        let dict  = arrResult[indexPath.row]
        let strResult = dict["finalResult"] as? Double ?? 0.0
        let strFinalResult = String(format: "%.1f", strResult)
        let fullArr = strFinalResult.components(separatedBy: ".")
        let first =  fullArr[0]
    //    let last =  fullArr[1]
        
        cell.lblResult.text = first + "/" + "90"
        let colorName = dict["color"] as? String
        cell.viewbg.layer.borderColor = UIColor(named: colorName ?? "darkLightColor")?.cgColor
         if indexPath.row == 0 {
            cell.lblTitle.text = "Pronounciation"
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
            cell.lblResult.text = first + "/" + "90"
           /* if last != "0"{
            cell.lblResult.text = strFinalResult + "/" + "90"
            }else{
            cell.lblResult.text = first + "/" + "90"
            }*/
         } else  if indexPath.row == 1 {
            cell.lblTitle.text = "Fluency"
            cell.viewbg.layer.borderColor = UIColor.green.cgColor
/*if last != "0"{
                cell.lblResult.text = strFinalResult + "/" + "90"
            }else{
                cell.lblResult.text = first + "/" + "90"
            }*/
         }else if indexPath.row == 2 {
            cell.lblTitle.text = "Content"
            cell.viewbg.layer.borderColor = UIColor.brown.cgColor
           /* if last != "0"{
            cell.lblResult.text = strFinalResult + "/" + "90"
            }else{
            cell.lblResult.text = first + "/" + "90"
            }*/
           
        } else if indexPath.row == 3 {
            cell.lblTitle.text = "Final"
            cell.viewbg.layer.borderColor = UIColor.red.cgColor
          /*  if last != "0"{
            cell.lblResult.text = strFinalResult
            }else{
        cell.lblResult.text = first
    }*/
        }
        return cell
    }
    
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 106, height: 106)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    
}
