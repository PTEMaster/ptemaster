//
//  CustomProgress.swift
//  PTEMaster
//
//  Created by mac on 01/05/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation

@IBDesignable class CustomSwitch: UIProgressView {
    
    @IBInspectable var scale : CGFloat = 1 {
        didSet{
            setup()
        }
    }
    
    //from storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    //from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup(){
        self.transform = CGAffineTransform(scaleX: scale, y: scale)
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
        super.prepareForInterfaceBuilder()
    }
    
    
}

