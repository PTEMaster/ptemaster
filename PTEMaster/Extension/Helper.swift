//
//  Helper.swift
//  Recipe
//
//  Created by MacMini on 09/01/18.
//  Copyright © 2018 Codiant. All rights reserved.
//

import Foundation
import UIKit

 struct Helper {
    
    static var safeAreaBottomMargine: CGFloat {
        get {
            if let window = UIApplication.shared.keyWindow {
                if #available(iOS 11.0, *) {
                    return window.safeAreaInsets.bottom
                }
            }
            return 0
        }
    }

    static var safeAreaTopMargine: CGFloat {
        get {
            if let window = UIApplication.shared.keyWindow {
                if #available(iOS 11.0, *){
                    return window.safeAreaInsets.top
                }
            }
            return 0
        }
    }
  
}



//MARK:- UITableView Extension
extension UITableView {
    func reloadDataInMain() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
    
    func displayBackgroundText(text: String, fontStyle: String = "HelveticaNeue", fontSize: CGFloat = 16.0) {
        let lbl = UILabel();
        
        if let headerView = self.tableHeaderView {
            lbl.frame = CGRect(x: 0, y: headerView.bounds.size.height, width: self.bounds.size.width, height: self.bounds.size.height - headerView.bounds.size.height)
        } else {
            lbl.frame = CGRect(x: 10, y: 0, width: self.bounds.size.width - 20, height: self.bounds.size.height);
        }
        lbl.text = text;
        lbl.textColor = AppColor.clr1
        lbl.numberOfLines = 0;
        lbl.textAlignment = .center;
        lbl.font = UIFont(name: fontStyle, size:fontSize);
        let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height));
        backgroundView.addSubview(lbl);
        self.backgroundView = backgroundView;
    }
    
    func removeBackgroundText() {
        self.backgroundView = nil
    }
}

//MARK:- UICollectionView Extension
extension UICollectionView {
    func reloadDataInMain() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
  
    func displayBackgroundText(text: String, fontStyle: String = "Helvetica", fontSize: CGFloat = 16.0) {
        let lbl = UILabel();
        lbl.frame = CGRect(x: 10, y: 0, width: self.bounds.size.width - 20, height: self.bounds.size.height);
        lbl.text = text;
        lbl.textColor =  AppColor.redColor
        lbl.numberOfLines = 0;
        lbl.textAlignment = .center;
        lbl.font = UIFont(name: fontStyle, size:fontSize);
            
        let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height));
        backgroundView.addSubview(lbl);
        self.backgroundView = backgroundView;
    }
    
    
    func removeBackgroundText() {
        self.backgroundView = nil
    }
        
    
}

//MARK:- Dictionary Extension
extension Dictionary {
    func getString(forKey key: Key, pointValue val: Int = 2, defaultValue def: String = "") -> String {
        if let str = self[key] as? String {
            return str
        } else if let num = self[key] as? NSNumber {
            let doubleVal = Double(truncating: num)
            return String(format: "%0.\(val)f", doubleVal)
        }
        return def
    }
    
    func getInt(forKey key: Key, defaultValue def: Int = 0) -> Int {
        if let num = self[key] as? Int {
            return num
        } else if let str = self[key] as? String {
            if let val = Int(str) {
                return val
            }
        } else if let num = self[key] as? NSNumber {
            return Int(truncating: num)
        }
        return def
    }
    
    func getDouble(forKey key: Key, defaultValue def: Double = 00.00) -> Double {
        if let num = self[key] as? Double {
            return num
        } else if let str = self[key] as? String {
            if let val = Double(str) {
                return val
            }
        } else if let num = self[key] as? NSNumber {
            return Double(truncating: num)
        }
        return def
    }
    
    func getBool(forKey key: Key, defaultValue def: Bool = false) -> Bool {
        if let val = self[key] as? Bool {
            return val
        } else if let num = self[key] as? NSNumber {
            if num == 0 {
                return false
            } else if num == 1 {
                return true
            }
        } else if let str = self[key] as? String {
            if str.lowercased() == "true" || str.lowercased() == "yes" {
                return true
            } else if str.lowercased() == "false" || str.lowercased() == "no" {
                return false
            }
        }
        return def
    }
    
    func getDictionary(forKey key: Key, defaultValue def: [String: Any] = [:]) -> [String: Any] {
        if let dict = self[key] as? [String: Any] {
            return dict
        } else if let nsdict = self[key] as? NSDictionary, let dict = nsdict as? [String: Any] {
            return dict
        } else if let str = self[key] as? String, let dict = str.convertToDictionary() {
            return dict
        } else if let data = self[key] as? Data, let dict = data.convertToDictionary() {
            return dict
        }
        
        return def
    }
}

//MARK:- String Extension
extension String {
    
    func convertToDate(serverDateFormat: String) -> Date {
        let df = DateFormatter()
        df.dateFormat = serverDateFormat
        
        if let date = df.date(from: self) {
            return date
        }
        return Date()

    }
    
    var trimWhitespaces: String {
        get {
            return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    func sizeOf(_ font: UIFont) -> CGSize {
        return self.size(withAttributes: [NSAttributedString.Key.font: font])
    }
    
    func removeHtmlFromString() -> String{
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    //textView.attributedText = htmlText.htmlToAttributedString
    
   
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            return data.convertToDictionary()
        }
        return nil
    }
    
    func underlinedText(_ lineColor: UIColor) -> NSAttributedString {
        let textRange = NSMakeRange(0, self.count)
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: lineColor, range: textRange)
        return attributedString
    }
    
    func getTextSize(font: UIFont, boundedBySize: CGSize) -> CGSize {
        let attrString = NSAttributedString(string: self, attributes: [NSAttributedString.Key.font: font])
        let size = attrString.boundingRect(with: boundedBySize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil).size
        return size
    }
    
    subscript(i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
}

extension NSObject{
    class var nameOfClass: String{
        return String(describing: self)
    }
    
    var nameOfClass: String {
        return type(of: self).nameOfClass
    }
}

extension Data {
    func convertToDictionary() -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}


// scroll UIColor for parallex
extension UIColor {
    func add(overlay: UIColor) -> UIColor {
        var bgR: CGFloat = 0
        var bgG: CGFloat = 0
        var bgB: CGFloat = 0
        var bgA: CGFloat = 0
        
        var fgR: CGFloat = 0
        var fgG: CGFloat = 0
        var fgB: CGFloat = 0
        var fgA: CGFloat = 0
        
        self.getRed(&bgR, green: &bgG, blue: &bgB, alpha: &bgA)
        overlay.getRed(&fgR, green: &fgG, blue: &fgB, alpha: &fgA)
        
        let r = fgA * fgR + (1 - fgA) * bgR
        let g = fgA * fgG + (1 - fgA) * bgG
        let b = fgA * fgB + (1 - fgA) * bgB
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}


// change image color
extension UIImage {
    
    func coloredImage(color: UIColor) -> UIImage {
        let backgroundSize = self.size
        UIGraphicsBeginImageContextWithOptions(backgroundSize, false, UIScreen.main.scale)
        
        let ctx = UIGraphicsGetCurrentContext()!
        
        var backgroundRect = CGRect()
        backgroundRect.size = backgroundSize
        backgroundRect.origin.x = 0
        backgroundRect.origin.y = 0
        
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        ctx.setFillColor(red: r, green: g, blue: b, alpha: a)
        ctx.fill(backgroundRect)
        
        var imageRect = CGRect()
        imageRect.size = self.size
        imageRect.origin.x = (backgroundSize.width - self.size.width) / 2
        imageRect.origin.y = (backgroundSize.height - self.size.height) / 2
        
        // Unflip the image
        ctx.translateBy(x: 0, y: backgroundSize.height)
        ctx.scaleBy(x: 1.0, y: -1.0)
        
        ctx.setBlendMode(.destinationIn)
        ctx.draw(self.cgImage!, in: imageRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? self
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount: CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


extension Date {

    func convertDateToLocalString(withFormat format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier: "UTC/GMT")
       // dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
       // dateFormatter.timeZone = TimeZone.current
        dateFormatter.doesRelativeDateFormatting = false
        // en_IE  // en_US_POSIX
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
}

extension String {
    
    func convertServerDateToTimeInterval(_ dateFormat: String) -> TimeInterval {
        let df = DateFormatter()
        df.dateFormat = dateFormat
        df.locale = Locale(identifier: "en_US_POSIX")
        if let date = df.date(from: self) {
            return date.timeIntervalSince1970
        }
        return Date().timeIntervalSince1970
    }
    
    func getDate(withFormat format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
       // dateFormatter.timeZone = TimeZone(identifier: "UTC/GMT")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: self)
    }
}

