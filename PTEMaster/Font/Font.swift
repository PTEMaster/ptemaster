//
//  Font.swift
//  PTEMaster
//
//  Created by mac on 01/05/19.
//  Copyright © 2019 CTIMac. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    static func GlacialBold(_ size : CGFloat) -> UIFont{
        let font : UIFont? = UIFont(name: "SansaGLACIALINDIFFERENCE-BOLD", size: size)!
        return (font != nil) ? font! : UIFont.systemFont(ofSize : size)
    }
    
    static func GlacialRegular(_ size : CGFloat) -> UIFont{
        let font : UIFont? = UIFont(name: "GLACIALINDIFFERENCE-REGULAR", size: size)!
        return (font != nil) ? font! : UIFont.systemFont(ofSize : size)
    }
    
    /*
     Sansation-Bold
     Sansation-Regular
     */
}
