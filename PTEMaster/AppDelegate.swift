//
//  AppDelegate.swift
//  PTEMaster
//
//  Created by CTIMac on 11/12/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//  client id:- 55262368066-n1ve64gge0gd15lsr7v3lc96j4ragtaf.apps.googleusercontent.com

import UIKit
import UserNotifications
import GoogleSignIn
import FBSDKCoreKit
import RSLoadingView


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
    
    var window: UIWindow?
    fileprivate var reachability = Reachability()!

    var isNetworkAvailable: Bool = false
    var strLoader = ""
    var strDeviceToken: String = ""
    
    var toalCreditTime: Int = 0
    var toalCredits: Int = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "792744435099-avb3mvbrjjl2v33435cftq9t5mprumep.apps.googleusercontent.com"
        ApplicationDelegate.shared.application(application,didFinishLaunchingWithOptions: launchOptions)
        
        
        DotLoader.shared.appDelegate = self
        DotLoader.shared.animateDotLoader = true
        DotLoader.shared.dotColor =  UIColor(red: 0/255.0, green: 130/255.0, blue: 249/255.0, alpha: 1)
        DotLoader.shared.loaderContainerColor = UIColor.clear
        registerForPushNotifications()
        setupNetworkMonitoring()
        IQKeyboardManager.shared.enable = true;
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        application.statusBarStyle = .lightContent // .default
        // Override point for customization after application launch.
        if #available(iOS 13.0, *) {
                    // Always adopt a light interface style.
          window?.overrideUserInterfaceStyle = .light
          application.statusBarStyle = .default
            
          /*  for family: String in UIFont.familyNames
                 {
                     print("\(family)\n")
                     for names: String in UIFont.fontNames(forFamilyName: family)
                     {
                         print("== \(names)")
                     }
                 }*/
      }
        return true
    }
    
//facebook
    

    // Swift
    //
    // AppDelegate.swift

  /*  class AppDelegate: UIResponder, UIApplicationDelegate {
        
        func application(
            _ application: UIApplication,
            didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
        ) -> Bool {
              
            ApplicationDelegate.shared.application(
                application,
                didFinishLaunchingWithOptions: launchOptions
            )

            return true
        }
              
        func application(
            _ app: UIApplication,
            open url: URL,
            options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {

            ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            )

        }
        

        @available(iOS 13.0, *)
        func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
            guard let url = URLContexts.first?.url else {
                return
            }

            ApplicationDelegate.shared.application(
                UIApplication.shared,
                open: url,
                sourceApplication: nil,
                annotation: [UIApplication.OpenURLOptionsKey.annotation]
            )
        }


    }*/
    
  
 

    // MARK: UISceneSession Lifecycle

  /*  @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

   @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }*/


//--------------------------------------------------------------
     // FIXME: HIMANSHU PAL 25/8/20 googele integration
         
    func application(_ app: UIApplication,open url: URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {

    ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )

    }
     
   /*  @available(iOS 9.0, *)
     func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
       return GIDSignIn.sharedInstance().handle(url)
     }*/
     
    /* func application(_ application: UIApplication,
                      open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
       return GIDSignIn.sharedInstance().handle(url)
     }*/
     func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
         var flag: Bool = false
         if ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) {

             // Handle Facebook URL scheme
             flag = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
         } else {

             // Handle Google URL scheme
             flag = GIDSignIn.sharedInstance().handle(url)
         }
         return flag
     }
     


    


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
show(message: "11111")
        }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        show(message: "2222")
        print("[RemoteNotification] applicationState:  didReceiveRemoteNotification for iOS9: \(userInfo)")
       // self.handleNotification(userInfo: userInfo)
    }

    
    
    // MARK: -- PushNoification
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        //log.info("Device Token String:\(token)")
        
        //** Remove special character and get valid device token string
        let characterSet: CharacterSet = CharacterSet( charactersIn: "<>" )
        
        let deviceTokenString: String = (token as NSString ) .trimmingCharacters(in: characterSet) .replacingOccurrences(of: " ", with: "" ) as String
        
        strDeviceToken = deviceTokenString
        UserDefaults.standard.setValue(strDeviceToken, forKey: "device_token")
        UserDefaults.standard.setValue(strDeviceToken, forKey: "device_id")
        
        //UserDefaults.standard.set(Util.getValidString(LoggedInUser.sharedUser.deviceToken), forKey: "device_token")
        
        let isLogin = UserDefaults.standard.object(forKey: "LoggedIn") as? String
        
        if isLogin == "true" {
            // strUserId = "\(String(describing: UserDefaults.standard.object(forKey: "UserId") as! NSNumber))"
            //NotificationAPI()
        } else {
            //strUserId = ""
            //NotificationAPI()
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }

    //****************************************************
    // MARK: MBProgressHUD Methods
    //****************************************************
    
    func showHUD() {
        
    //  DotLoader.shared.showLoader()
     Loader.showLoader()
    }
    
    func hideHUD() {
       // MBProgressHUD.hide(for: fromView, animated: true)
    Loader.hideLoader()
        
        print("---------hide_hp-----------")
   //  DotLoader.shared.stopLoader()
    }
    
    //****************************************************
    // MARK: - Network Reachability Methods
    //****************************************************
    //MARK:- Reachability
    func setupNetworkMonitoring() {
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
                self.isNetworkAvailable = true
            }
        }
        reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                print("Not reachable")
                self.isNetworkAvailable = false
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            self.isNetworkAvailable = true
        } else {
            print("Network not reachable")
            self.isNetworkAvailable = false
        }
    }

}

